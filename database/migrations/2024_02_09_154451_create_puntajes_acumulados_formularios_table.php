<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuntajesAcumuladosFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
     public function up()
     {
         Schema::create('puntajes_acumulados_formularios', function (Blueprint $table) {
             $table->id();
             $table->unsignedBigInteger('usuario_id');
             $table->unsignedBigInteger('formulario_id');
             $table->float('puntaje_obtenido_formulario')->default(0);
             $table->boolean('aprobado')->default(false); 
             $table->timestamps();
             
             $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('formulario_id')->references('id')->on('formularios')->onDelete('cascade');
         });
     }
     
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntajes_acumulados');
    }
}
