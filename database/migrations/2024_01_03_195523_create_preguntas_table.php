<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('preguntas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('formulario_id');
            $table->foreign('formulario_id')->references('id')->on('formularios')->onDelete('cascade');
            $table->string('tipo_pregunta');
            $table->text('texto_pregunta');
            $table->string('respuestaCorrecta')->nullable();
            $table->integer('puntaje')->default(0);
            $table->unsignedBigInteger('contenido_id')->nullable(); 
            $table->timestamps();
        
            $table->foreign('contenido_id')->references('id')->on('contenidos')->onDelete('set null');
        });
        
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
