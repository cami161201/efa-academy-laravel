<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->string('correo')->unique();
            $table->string('academia')->nullable();
            $table->string('ocupacion');
            $table->date('fecha_nacimiento');
            $table->string('celular');
            $table->string('password');
            $table->date('start_access')->nullable(); 
            $table->date('end_access')->nullable();
            $table->enum('funcion', ['admin', 'estudiante']);
            $table->enum('tipo_curso',['Intensivo','Grupal','Personalizado','Online']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
