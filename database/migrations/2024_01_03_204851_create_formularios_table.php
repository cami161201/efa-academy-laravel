<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulariosTable extends Migration
{
    /** DROP TABLE opciones;

     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('unidad_id'); // Relación con Unidad
            $table->string('titulo');
            $table->text('descripcion');
            $table->integer('puntaje_total')->default(0);
            $table->timestamps();
        
            $table->foreign('unidad_id')
                ->references('id')
                ->on('unidades')
                ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formularios');
    }
}
