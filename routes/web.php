<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactoController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Auth\Events\Login;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ModuloController;
use App\Http\Controllers\ContenidoController;
use App\Http\Controllers\UnidadController;
use App\Http\Controllers\PreguntaController;
use App\Http\Controllers\FormularioController;
use App\Http\Controllers\CalificacionController;
use App\Http\Controllers\RespuestaUsuarioController;
use App\Http\Controllers\ContactController;
use App\Models\RespuestaUsuario;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/pru', function () {
    return view('pruebas');
});
Route::get('/pagoCurso', function () {
    return view('pago');
}); 

Route::get('/login', [LoginController::class, 'show'])->name('login.show'); // Pagina de login
Route::post('/login', [LoginController::class, 'login'])->name('login.ingresar');

Route::get('/contacto', [ContactController::class,'index'])->name('contacto');
Route::post('/contacto/enviar', [ContactController::class,'enviar'])->name('contacto.enviar');



Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [LoginController::class, 'logout']); 
    Route::get('/perfil', fn() => view('user.perfil'));
    Route::get('/bienvenido', [HomeController::class, 'show'])->name('user.bienvenida');

    Route::get('/ver-modulo/{id}', [HomeController::class, 'verModulo'])->name('ver.modulo');
    Route::get('/ver-formulario/{id}', [CalificacionController::class, 'verFormulario'])->name('ver.formulario');
    Route::post('/guardar-respuestas/{formulario}',[CalificacionController::class, 'guardarRespuestas'])->name('guardar.respuestas');
    
    Route::get('/niveles', function () {
        return view('user.nivel');
    }); 

});


Route::middleware(['auth', 'admin'])->group(function () {

    Route::get('/modulo', [ModuloController::class, 'show'])->name('modulo.show');
    Route::post('/modulo', [ModuloController::class, 'store'])->name('modulo.registrar');
    Route::delete('/modulo/{id}', [ModuloController::class, 'destroy'])->name('modulo.eliminar');
    Route::put('/modulo/{id}', [ModuloController::class, 'editar'])->name('modulo.actualizar');
    Route::get('/visor-contenido/{id}', [ModuloController::class, 'mostrarContenido'])->name('visor-contenido');

    // seccion de unidades

    Route::get('admin/modulo/{modulo}/unidades', [UnidadController::class, 'showUnidades'])->name('admin.modulo.unidades');
    Route::get('admin/modulo/{modulo}/unidad/create',  [UnidadController::class, 'create'])->name('unidad.create');
    Route::post('admin/modulo/{modulo}/unidad/store',  [UnidadController::class, 'store'])->name('unidad.store');
    Route::delete('admin/modulo/unidad/{unidad}', [UnidadController::class, 'destroy'])->name('unidad.destroy');
    Route::get('admin/modulo/{unidad}/edit', [UnidadController::class, 'edit'])->name('unidad.edit');
    Route::put('admin/modulo/{unidad}', [UnidadController::class, 'update'])->name('unidad.update');


    //Seccion de Estudiantes    
    Route::post('/registrar-estudiante', [UsuarioController::class, 'registro'])->name('registro.registrar');
    Route::get('/lista-estudiantes', [UsuarioController::class, 'mostrar'])->name('estudiante.mostrar');
    Route::delete('/eliminar-estudiante/{id}', [UsuarioController::class, 'destroy'])->name('eliminar.usuario');
    Route::put('/actualizar-estudiante/{id}', [UsuarioController::class, 'actualizar'])->name('usuario.actualizar');
    Route::post('/asignar-editar-modulos/{userId}', [UsuarioController::class, 'asignarEditarModulos'])->name('asignar.editar.modulos');


    Route::get('/contenidos', [ContenidoController::class, 'index'])->name('contenido.show');
    Route::post('/subir-archivo', [ContenidoController::class, 'store'])->name('contenido.subir-archivo');
    Route::get('/editar-contenidos/{contenido}/edit', [ContenidoController::class, 'edit'])->name('contenidos.edit');
    Route::put('/actualizar-contenidos/{contenido}', [ContenidoController::class, 'update'])->name('contenidos.update');
    Route::delete('/eliminar-contenidos/{contenido}', [ContenidoController::class, 'destroy'])->name('contenidos.destroy');

    Route::get('/formularios', [FormularioController::class, 'show'])->name('formularios.show');
    Route::post('/crear-formulario', [FormularioController::class, 'crearFormulario'])->name('formularios.store');
    Route::delete('/eliminar-formulario/{id}', [FormularioController::class, 'eliminarformulario'])->name('formulario.eliminar');
    Route::put('/editar-formulario/{id}', [FormularioController::class, 'actualizarFormulario'])->name('formulario.actualizar');

    Route::get('/preguntas/{id}', [PreguntaController::class, 'mostrarPreguntas'])->name('preguntas.show');
    Route::post('/guardar-pregunta/{formulario_id}', [PreguntaController::class, 'guardarPregunta'])->name('crear.pregunta');
    Route::delete('/eliminar-pregunta/{id}', [PreguntaController::class, 'eliminarPregunta'])->name('eliminar.pregunta');
    Route::put('/editar-pregunta/{pregunta_id}', [PreguntaController::class, 'editarPregunta'])->name('editar.pregunta');

    Route::get('/respuesta', [RespuestaUsuarioController::class, 'show'])->name('show');
    Route::post('/guardar-correccion/{respuestaId}', [RespuestaUsuarioController::class, 'guardarCorreccion'])->name('guardar_correccion');
    Route::delete('/eliminar-respuestas/{formularioId}/{usuarioId}', [CalificacionController::class, 'eliminarRespuestasUsuario'])->name('eliminar.respuestas');

    Route::get('/notas-estudiantes',[RespuestaUsuarioController::class,'mostrarPuntajesUsuarios'])->name('mostrar.notas'); 
});
