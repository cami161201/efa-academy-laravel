import firebase from 'firebase/app';
import 'firebase/storage';

// Your web app's Firebase configurationaca
const firebaseConfig = {
  apiKey: "AIzaSyAZGUH8NgXtqzqx63BK-urKpbbWAIqLqt8",
  authDomain: "efa-contenido.firebaseapp.com",
  projectId: "efa-contenido",
  storageBucket: "efa-contenido.appspot.com",
  messagingSenderId: "694437332883",
  appId: "1:694437332883:web:0e8667235aa9f9fccd4a66"
};

const app = firebase.initializeApp(firebaseConfig);
const storage = app.storage();

export default storage;

