var contadorOpcionesCrear = 0;

function agregarOpcionCrear() {
    var div = document.createElement('div');
    div.id = 'opcion-crear-' + contadorOpcionesCrear;
    var isMultipleOption = document.getElementById('tipo_pregunta_crear').value === 'Seleccion multiple';

    div.innerHTML =
        '<div class="row align-items-end mb-3">' +
        '<div class="col-md-10">' +
        'Opción: <input class="form-control campo" type="text" name="opciones[' + contadorOpcionesCrear + '][textoOpcion]">' +
        '</div>' +
        '<div class="col-md-2">' +
        '<button type="button" class="btn btn-danger" onclick="eliminarOpcionCrear(' + contadorOpcionesCrear +
        ')"><i class="bi bi-trash-fill"></i></button>' +
        '</div>' +
        '<div class="row">' +
        (isMultipleOption ? '<label><input class="form-check-input mb-3" type="checkbox" name="opciones[' +
            contadorOpcionesCrear + '][esCorrecta]"> Es Correcta</label>' : '') +
        '</div>' +
        '</div>';

    document.getElementById('opciones-container-crear').appendChild(div);
    contadorOpcionesCrear++;
}

function eliminarOpcionCrear(id) {
    var elemento = document.getElementById('opcion-crear-' + id);
    elemento.parentNode.removeChild(elemento);
}

document.getElementById('tipo_pregunta_crear').onchange = function () {
    var opcionesDiv = document.getElementById('opciones-crear');
    var respuestaCorrectaDiv = document.getElementById('respuestaCorrectaDiv-crear');
    var verdaderoFalsoDiv = document.getElementById('verdaderoFalsoDiv-crear');
    var opcionesContainer = document.getElementById('opciones-container-crear');

    // Eliminar opciones existentes antes de agregar nuevas opciones
    while (opcionesContainer.firstChild) {
        opcionesContainer.removeChild(opcionesContainer.firstChild);
    }

    contadorOpcionesCrear = 0; // Reiniciar el contador de opciones

    if (this.value === 'Seleccion multiple') {
        opcionesDiv.style.display = 'block';
        respuestaCorrectaDiv.style.display = 'none';
        verdaderoFalsoDiv.style.display = 'none';
        agregarOpcionCrear();
    } else if (this.value === 'Opcion multiple') {
        opcionesDiv.style.display = 'block';
        respuestaCorrectaDiv.style.display = 'block';
        verdaderoFalsoDiv.style.display = 'none';
        agregarOpcionCrear();
    } else if (this.value === 'Verdadero/Falso') {
        opcionesDiv.style.display = 'none';
        respuestaCorrectaDiv.style.display = 'none';
        verdaderoFalsoDiv.style.display = 'block';
    } else if (this.value === 'Larga') {
        opcionesDiv.style.display = 'none';
        respuestaCorrectaDiv.style.display = 'none';
        verdaderoFalsoDiv.style.display = 'none';
    } else {
        opcionesDiv.style.display = 'none';
        respuestaCorrectaDiv.style.display = 'block';
        verdaderoFalsoDiv.style.display = 'none';
    }
};
