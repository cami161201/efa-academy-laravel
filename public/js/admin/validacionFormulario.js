
    document.addEventListener('DOMContentLoaded', function () {
        const tituloInput = document.getElementById('titulo');
        const unidadSelect = document.getElementById('unidad');
        const btnCrearCurso = document.getElementById('btnCrearCurso');

        tituloInput.addEventListener('input', validarFormulario);
        unidadSelect.addEventListener('change', validarFormulario);

        function validarFormulario() {
            const tituloValue = tituloInput.value.trim();
            const unidadValue = unidadSelect.value;

            // Validación para el campo Título
            if (tituloValue === '') {
                mostrarAdvertencia(tituloInput, 'El título es obligatorio');
                deshabilitarBoton();
                return;
            } else {
                ocultarAdvertencia(tituloInput);
            }

            // Validación para el campo Unidad
            if (unidadValue === '') {
                mostrarAdvertencia(unidadSelect, 'Selecciona una unidad válida');
                deshabilitarBoton();
                return;
            } else {
                ocultarAdvertencia(unidadSelect);
            }

            // Si ambos campos están llenos, habilita el botón
            habilitarBoton();
        }

        function mostrarAdvertencia(elemento, mensaje) {
            elemento.classList.add('is-invalid');
            const parent = elemento.parentElement;
            const warningDiv = parent.querySelector('.invalid-feedback');
            warningDiv.innerHTML = mensaje;
            warningDiv.style.display = 'block';
        }

        function ocultarAdvertencia(elemento) {
            elemento.classList.remove('is-invalid');
            const parent = elemento.parentElement;
            const warningDiv = parent.querySelector('.invalid-feedback');
            warningDiv.innerHTML = '';
            warningDiv.style.display = 'none';
        }

        function deshabilitarBoton() {
            btnCrearCurso.disabled = true;
        }

        function habilitarBoton() {
            btnCrearCurso.disabled = false;
        }
    });

