document.addEventListener("DOMContentLoaded", function() {
    const passwordInput = document.getElementById("password");
    const togglePasswordButton = document.getElementById("toggle-password");
    const passwordError = document.getElementById('password-error');

    togglePasswordButton.addEventListener("click", function() {
        togglePasswordVisibility(passwordInput, togglePasswordButton);
    });

    passwordInput.addEventListener('input', function(e) {
        validarPassword(e);
        validarCampo(passwordInput);
    });

    const nombreInput = document.getElementById("nombre");
    const nombreError = document.getElementById("nombre-error");

    const primerApellidoInput = document.getElementById("primer_apellido");
    const primerApellidoError = document.getElementById("primer_apellido-error");

    const segundoApellidoInput = document.getElementById("segundo_apellido");
    const segundoApellidoError = document.getElementById("segundo_apellido-error");

    const academiaInput = document.getElementById("academia");
    const academiaError = document.getElementById("academia-error");

    const ocupacionInput = document.getElementById("ocupacion");
    const ocupacionError = document.getElementById("ocupacion-error");

    const celularInput = document.getElementById("celular");
    const celularError = document.getElementById("celular-error");

    const correoInput = document.getElementById("correo");
    const correoError = document.getElementById("correo-error");

    const fechaNacimientoInput = document.getElementById("fecha_nacimiento");
    const fechaNacimientoError = document.getElementById("fecha_nacimiento-error");

    const startAccessInput = document.getElementById("start_access");
    const endAccessInput = document.getElementById("end_access");

    const startAccessError = document.getElementById("start_access-error");
    const endAccessError = document.getElementById("end_access-error");

    const submitButton = document.querySelector("button[type='submit']");

    const campos = [nombreInput, primerApellidoInput, segundoApellidoInput, academiaInput, ocupacionInput,
        celularInput, correoInput, fechaNacimientoInput, passwordInput
    ];

    campos.forEach(function(campo) {
        campo.addEventListener('input', function() {
            validarCampo(campo);
        });
    });

    submitButton.disabled = true;

    function togglePasswordVisibility(input, toggleButton) {
        if (input.type === "password") {
            input.type = "text";
            toggleButton.innerHTML = '<i class="bi bi-eye"></i>';
        } else {
            input.type = "password";
            toggleButton.innerHTML = '<i class="bi bi-eye-slash"></i>';
        }
    }

    function validarPassword(e) {
        const password = passwordInput.value;
        const passwordError = document.getElementById('password-error');

        if (password === '') {
            passwordInput.classList.add('is-invalid');
            passwordError.style.display = 'block';
            passwordError.innerHTML = 'La contraseña es obligatoria.';
        } else if (password.length < 6) { // Cambiado a 6 para requerir al menos 6 caracteres
            passwordInput.classList.add('is-invalid');
            passwordError.style.display = 'block';
            passwordError.innerHTML = 'La contraseña debe tener al menos 6 caracteres.';
        } else {
            passwordInput.classList.remove('is-invalid');
            passwordError.style.display = 'none';
            passwordError.innerHTML = '';
        }
    }


    function validarCampo(input) {
        const inputError = document.getElementById(input.id + '-error');

        if (input.value.trim() === '') {
            input.classList.add('is-invalid');
            inputError.style.display = 'block';
            inputError.innerHTML = 'Este campo es obligatorio.';
        } else if (input === nombreInput || input === primerApellidoInput || input === academiaInput ||
            input === ocupacionInput) {

            const patron = /^[a-zA-ZñÑáÁéÉíÍóÓúÚ\s]+$/;
            if (!patron.test(input.value)) {
                input.classList.add('is-invalid');
                inputError.style.display = 'block';
                inputError.innerHTML = 'Este campo solo debe contener letras';
            } else {
                input.classList.remove('is-invalid');
                inputError.style.display = 'none';
                inputError.innerHTML = '';
            }
        } else if (input === segundoApellidoInput) {
            const patron =
                /^[a-zA-ZñÑáÁéÉíÍóÓúÚ\s]*$/; // Permitir letras y espacios, pero no es obligatorio
            if (!patron.test(input.value)) {
                input.classList.add('is-invalid');
                inputError.style.display = 'block';
                inputError.innerHTML = 'Este campo solo debe contener letras.';
            } else {
                input.classList.remove('is-invalid');
                inputError.style.display = 'none';
                inputError.innerHTML = '';
            }
        } else if (input === celularInput) {

            const patronNumeros = /^[0-9]+$/;
            if (!patronNumeros.test(input.value)) {
                input.classList.add('is-invalid');
                inputError.style.display = 'block';
                inputError.innerHTML = 'Este campo debe contener solo números';
            } else {
                input.classList.remove('is-invalid');
                inputError.style.display = 'none';
                inputError.innerHTML = '';
            }
        } else if (input === correoInput) {
            // Validación para el campo de correo
            const patronCorreo = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (!patronCorreo.test(input.value)) {
                input.classList.add('is-invalid');
                inputError.style.display = 'block';
                inputError.innerHTML = 'Por favor, ingrese un correo electrónico válido.';
            } else {
                input.classList.remove('is-invalid');
                inputError.style.display = 'none';
                inputError.innerHTML = '';
            }
        } else if (input === fechaNacimientoInput) {
            if (input.value.trim() === '') {
                input.classList.add('is-invalid');
                fechaNacimientoError.style.display = 'block';
                fechaNacimientoError.innerHTML = 'Este campo es obligatorio.';
            } else {
                input.classList.remove('is-invalid');
                fechaNacimientoError.style.display = 'none';
                fechaNacimientoError.innerHTML = '';
            }
        }

        // Deshabilitar o habilitar el botón de submit según si hay errores en algún campo
        submitButton.disabled = hayErrores();
    }

    // Función para comprobar si hay errores en algún campo
    function hayErrores() {
        return campos.some(function(campo) {
            return campo.value.trim() === '' || campo.classList.contains('is-invalid');
        });
    }
});
