
    // function applyFormat(format) {
    //     var editor = document.getElementById('editor');
    //     var selectionStart = editor.selectionStart;
    //     var selectionEnd = editor.selectionEnd;
    //     var selectedText = editor.value.substring(selectionStart, selectionEnd);
    //     var newText;
    //     switch (format) {
    //         case 'bold':
    //             newText = editor.value.substring(0, selectionStart) + '<strong>' + selectedText + '</strong>' + editor.value.substring(selectionEnd);
    //             break;
    //         case 'italic':
    //             newText = editor.value.substring(0, selectionStart) + '<em>' + selectedText + '</em>' + editor.value.substring(selectionEnd);
    //             break;
    //         case 'linebreak':
    //             newText = editor.value.substring(0, selectionStart) + '<br>' + editor.value.substring(selectionStart);
    //             break;
    //     }
    //     editor.value = newText;
    // }

    // function applyColor() {
    //     var editor = document.getElementById('editor');
    //     var selectionStart = editor.selectionStart;
    //     var selectionEnd = editor.selectionEnd;
    //     var selectedText = editor.value.substring(selectionStart, selectionEnd);
    //     var color = document.getElementById('colorPicker').value;
    //     var newText = editor.value.substring(0, selectionStart) + '<span style="color:' + color + '">' + selectedText + '</span>' + editor.value.substring(selectionEnd);
    //     editor.value = newText;
    // }

    function applyStyle(style) {
        var editor = document.getElementById('editor');
        var selection = window.getSelection();
        var selectedText = selection.toString();
        var range = selection.getRangeAt(0);
    
        switch (style) {
            case 'bold':
                document.execCommand('bold');
                break;
            case 'italic':
                document.execCommand('italic');
                break;
            case 'underline':
                document.execCommand('underline');
                break;
        }
    }
    
    function applyColor() {
        var editor = document.getElementById('editor');
        var color = document.getElementById('colorPicker').value;
        document.execCommand('foreColor', false, color);
    }
    // Función para actualizar el campo oculto con el contenido del editor
    function updateHiddenInput() {
        var editorContent = document.getElementById('editor').innerHTML;
        document.getElementById('texto_pregunta_hidden').value = editorContent;
    }
    
    // Agrega un event listener para actualizar el campo oculto cuando el contenido del editor cambia
    document.getElementById('editor').addEventListener('input', updateHiddenInput);
    