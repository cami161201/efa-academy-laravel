document.addEventListener('DOMContentLoaded', function() {
    const tituloInput = document.getElementById('titulo');
    const tituloError = document.getElementById('titulo-error');
    const descripcionInput = document.getElementById('descripcion');
    const descripcionError = document.getElementById('descripcion-error');
    const urlArchivoInput = document.getElementById('url_archivo');
    const urlArchivoError = document.getElementById('url_archivo-error');
    const tipoContenidoSelect = document.getElementById('tipo_contenido');
    const botonSubir = document.getElementById('submit-button');
    const urlPattern = /^https?:\/\/\S+/;

    function validarCampos() {
        const tituloValido = tituloInput.value.trim() !== '' && tituloInput.value.length >= 5;
        const descripcionValida = descripcionInput.value.trim() !== '' && descripcionInput.value.length >=
            5;
        const urlArchivoValida = urlArchivoInput.value.trim() !== '' && urlPattern.test(urlArchivoInput
            .value);
        const tipoContenidoValido = tipoContenidoSelect.value !== 'Seleccione el tipo de contenido';

        return tituloValido && descripcionValida && urlArchivoValida && tipoContenidoValido;
    }

    function actualizarBotonSubir() {
        const camposValidados = validarCampos();
        botonSubir.disabled = !camposValidados;
    }

    tituloInput.addEventListener('input', function() {
        if (tituloInput.value.trim() === '' || tituloInput.value.length < 5) {
            tituloInput.classList.add('is-invalid');
            tituloError.style.display = 'block';
        } else {
            tituloInput.classList.remove('is-invalid');
            tituloError.style.display = 'none';
        }
        actualizarBotonSubir();
    });

    descripcionInput.addEventListener('input', function() {
        if (descripcionInput.value.trim() === '' || descripcionInput.value.length < 5) {
            descripcionInput.classList.add('is-invalid');
            descripcionError.style.display = 'block';
        } else {
            descripcionInput.classList.remove('is-invalid');
            descripcionError.style.display = 'none';
        }
        actualizarBotonSubir();
    });

    urlArchivoInput.addEventListener('input', function() {
        if (urlArchivoInput.value.trim() === '' || !urlPattern.test(urlArchivoInput.value)) {
            urlArchivoInput.classList.add('is-invalid');
            urlArchivoError.style.display = 'block';
        } else {
            urlArchivoInput.classList.remove('is-invalid');
            urlArchivoError.style.display = 'none';
        }
        actualizarBotonSubir();
    });

    tipoContenidoSelect.addEventListener('change', function() {
        actualizarBotonSubir();
    });
});
