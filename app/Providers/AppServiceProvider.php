<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View::share('modulosAsignados', function () {
            if (Auth::check()) {
                $usuario = Auth::user();
                return $usuario->modulos;
            } else {
                return collect(); // Retorna una colección vacía si el usuario no está autenticado.
            }
        });
    }
    

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
