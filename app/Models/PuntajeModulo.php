<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuntajeModulo extends Model
{
    use HasFactory;
    
    protected $table = 'puntaje_acumulado_modulo';

    protected $fillable = [
        'usuario_id',
        'modulo_id',
        'puntaje_obtenido_modulo',
        'aprobado',
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'modulo_id');
    }
}
