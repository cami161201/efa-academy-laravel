<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuntajeAcumulado extends Model
{
    protected $fillable = [
        'usuario_id',
        'formulario_id',
        'puntaje_obtenido_formulario',
        'aprobado'
    ];

    protected $table = 'puntajes_acumulados_formularios';

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function formulario()
    {
        return $this->belongsTo(Formulario::class, 'formulario_id');
    }
}
