<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    use HasFactory;
    
    protected $table = 'preguntas';

    protected $fillable = [
        'texto_pregunta',
        'tipo_pregunta',
        'respuestaCorrecta',
        'puntaje',
        'contenido_id', 
    ];

    public function opciones()
    {
        return $this->hasMany(Opcion::class);
    }

    public function contenido()
    {
        return $this->belongsTo(Contenido::class);
    }

    public function formulario()
{
    return $this->belongsTo(Formulario::class, 'formulario_id');
}
public function respuestaUsuario()
    {
        return $this->hasMany(RespuestaUsuario::class);
    }
}

