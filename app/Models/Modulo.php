<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{

    use HasFactory;

    protected $fillable = ['titulo', 'descripcion', 'puntaje_total_modulo','num_modulo'];

    protected $table = 'modulos'; 
    
    public function unidades()
    {
        return $this->hasMany(Unidad::class, 'moduloID');
    }

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'module_user', 'modulo_id', 'user_id');
    }

    public function actualizarPuntajeTotalModulo()
    {
        // Actualizar el puntaje total del módulo al sumar los puntajes totales de todas las unidades asociadas
        $puntajeTotalModulo = $this->unidades()->sum('puntaje_total_unidad');
        $this->update(['puntaje_total_modulo' => $puntajeTotalModulo]);
    }
    public function puntajesModulos()
    {
        return $this->hasMany(PuntajeModulo::class, 'modulo_id');
    }
}
