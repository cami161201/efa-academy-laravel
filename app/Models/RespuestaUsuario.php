<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestaUsuario extends Model
{
    use HasFactory;

    protected $table = 'respuestas_usuario';

    protected $fillable = [
        'usuario_id',
        'pregunta_id',
        'formulario_id',
        'respuesta_usuario'
    ];


    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function pregunta()
    {
        return $this->belongsTo(Pregunta::class, 'pregunta_id');
    }

    public function formulario()
    {
        return $this->belongsTo(Formulario::class, 'formulario_id');
    }

   
}
