<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    use HasFactory;

    protected $table = 'unidades'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'id'; // Nombre de la clave primaria

    protected $fillable = [
        'moduloID',
        'nombreUnidad',
        'descripcionUnidad',
        'puntaje_total_unidad',
        'orden'
    ];

    // Relación con la tabla de Módulos
    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'moduloID');
    }

    public function formularios()
    {
        return $this->hasMany(Formulario::class, 'unidad_id');
    }

    public function actualizarPuntajeTotalUnidad()
    {
        // Actualizar el puntaje total de la unidad al sumar los puntajes totales de los formularios asociados
        $puntajeTotalUnidad = $this->formularios->sum('puntaje_total');
        $this->update(['puntaje_total_unidad' => $puntajeTotalUnidad]);
    }

    public function puntajesUnidades()
    {
        return $this->hasMany(PuntajeUnidad::class, 'unidad_id');
    }
   // app\Models\Unidad.php


    // app\Models\Unidad.php

    // public function estaHabilitadaParaUsuario($usuario)
    // {
    //     // Si es la primera unidad, siempre está habilitada
    //     if ($this->orden == 1) {
    //         return true;
    //     }

    //     // Verifica si el usuario ha aprobado la unidad anterior
    //     $unidadAnterior = Unidad::where('moduloID', $this->moduloID)
    //         ->where('orden', $this->orden - 1)
    //         ->first();

    //     if (!$unidadAnterior) {
    //         return false; // Si no hay unidad anterior, no está habilitada
    //     }

    //     $puntajeUnidadAnterior = PuntajeUnidad::where('usuario_id', $usuario->id)
    //         ->where('unidad_id', $unidadAnterior->id)
    //         ->first();

    //     return $puntajeUnidadAnterior && $puntajeUnidadAnterior->aprobado;
    // }
}
