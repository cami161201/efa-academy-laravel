<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opcion extends Model
{
    use HasFactory;

    protected $table = 'opciones'; // Nombre de la tabla en la base de datos

    protected $fillable = [
        'pregunta_id',
        'textoOpcion',
        'esCorrecta',
    ];

    public function pregunta()
    {
        return $this->belongsTo(Pregunta::class);
    }
}
