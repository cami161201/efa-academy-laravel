<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acceso extends Model
{
    use HasFactory;


    protected $table = 'module_user';

    public function users()
    {
        return $this->belongsToMany(User::class, 'module_user', 'modulo_id', 'user_id');
    }

    public function modulos()
    {
        return $this->belongsToMany(Modulo::class, 'module_user', 'user_id', 'modulo_id');
    }
}
