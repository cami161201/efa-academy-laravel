<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\Access\Authorizable;
use App\Models\PuntajeAcumulado;



class User extends Authenticatable
{
    use HasRoles;
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'primer_apellido',
        'segundo_apellido',
        'correo',
        'academia',
        'ocupacion',
        'fecha_nacimiento',
        'celular',
        'password',
        'start_access',
        'end_access',
        'funcion',
        'tipo_curso',

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function modulos()
    {
        return $this->belongsToMany(Modulo::class, 'module_user', 'user_id', 'modulo_id');
    }

    public function setPasswordAttribute($value)
    {

        $this->attributes['password'] = $value;
    }


    public function respuestasUsuario()
    {
        return $this->hasMany(RespuestaUsuario::class, 'usuario_id');
    }

    public function puntajesAcumulados()
    {
        return $this->hasMany(PuntajeAcumulado::class, 'usuario_id');
    }

    public function puntajesUnidades()
    {
        return $this->hasMany(PuntajeUnidad::class, 'usuario_id');
    }
    
    public function puntajesModulos()
    {
        return $this->hasMany(PuntajeModulo::class, 'usuario_id');
    }
}
