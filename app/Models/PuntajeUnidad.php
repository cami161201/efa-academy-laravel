<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuntajeUnidad extends Model
{
    use HasFactory;
    protected $fillable = [
        'usuario_id',
        'unidad_id',
        'puntaje_obtenido_unidad',
        'aprobado'
    ];

    protected $table = 'puntajes_acumulados_unidades';

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }
    public function unidad()
    {
        return $this->belongsTo(Unidad::class, 'unidad_id');
    }
    
    
}
