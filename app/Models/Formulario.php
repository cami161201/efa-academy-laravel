<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Formulario extends Model
{
    use HasFactory;

    protected $table = 'formularios'; 

    protected $fillable = [
        'unidad_id',
        'titulo',
        'descripcion',
        'puntaje_total',
        'duracion'
    ];

    public function unidad()
    {
        return $this->belongsTo(Unidad::class, 'unidad_id');
    }

    public function preguntas()
    {
        return $this->hasMany(Pregunta::class);
    }

    public function actualizarPuntajeTotal()
    {
        $puntajeTotal = $this->preguntas->sum('puntaje');
        $this->update(['puntaje_total' => $puntajeTotal]);
    }
    
    public function puntajesAcumulados()
    {
        return $this->hasMany(PuntajeAcumulado::class, 'formulario_id');
    }
}

