<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->view('emails.contacto')
                    ->with(['data' => $this->data])
                    ->subject('Informacion sobre EFA Academy')
                    ->from($this->data['correo'])  // Establecer el remitente
                    ->replyTo($this->data['correo']);  // Establecer la dirección de respuesta
    }
}
