<?php

namespace App\Http\Livewire;

use App\Models\Contenido;
use Livewire\WithPagination;
use Livewire\Component;

class ContenidosTable extends Component
{


    public $campoBusqueda = '';



    public function render()
    {
        $contenidos = Contenido::where('titulo', 'like', '%' . $this->campoBusqueda . '%')->get();

        return view('livewire.contenidos-table', ['contenidos' => $contenidos]);
    }
}
