<?php

namespace App\Http\Livewire;

use Livewire\Component;
use app\Models\User;
use Livewire\WithPagination;
use App\Models\Modulo;

class UsuariosTable extends Component
{

    public $campoBusqueda = '';

    public function render()
    {
        $usuarios = User::where('nombre', 'like', '%' . $this->campoBusqueda . '%')->get();
        $modulos = Modulo::all();

        return view('livewire.usuarios-table', compact('usuarios', 'modulos'));
    }
}
