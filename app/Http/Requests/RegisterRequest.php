<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'primer_apellido' => 'required',
            'segundo_apellido',
            'correo' => 'required|unique:users,correo',
            'academia' => 'required',
            'ocupacion' => 'required',
            'fecha_nacimiento' => 'required',
            'celular' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'contenidos_acceso' => 'required|array',
        ];
    }
}
