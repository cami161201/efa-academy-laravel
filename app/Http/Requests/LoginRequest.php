<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'correo' => 'required',
            'password' => 'required',
            'fecha_nacimiento' => 'required'
        ];
    }

    public function getCredentials()
    {
        $correo = $this->get('correo');
        $password = $this->get('password'); 
        $fecha_nacimiento = $this->get('fecha_nacimiento');

        return [
            'correo' => $correo,
            'password' => $password,
            'fecha_nacimiento' => $fecha_nacimiento
        ];
    }

}
