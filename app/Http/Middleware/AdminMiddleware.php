<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
class AdminMiddleware
{
    use HasRoles;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    
     public function handle($request, Closure $next)
    {
        // Verifica si el usuario está autenticado y tiene el rol de 'admin'
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            return $next($request);
        }

        // Si no es administrador, puedes redirigirlo a una página de acceso denegado o realizar otra acción.
        return redirect()->back();
    }
}
