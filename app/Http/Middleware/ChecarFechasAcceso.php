<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ChecarFechasAcceso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next)
    {
        // $user = auth()->user();

        // if ($user && $user->accesses->count() > 0) {
        //     $now = Carbon::now();

        //     foreach ($user->accesses as $access) {
        //         if ($now->gte($access->start_access) && $now->lte($access->end_access)) {
        //             return $next($request);
        //         }
        //     }
        // }

        // return redirect()->route('login.show')->with('error', 'Tu acceso ha caducado.');
    }
}
