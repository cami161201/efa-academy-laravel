<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Unidad;
use App\Models\Modulo;

class UnidadController extends Controller
{
    public function create(Modulo $modulo)
    {
        return view('admin.modulo', compact('modulo'));
    }

    public function showUnidades($moduloId)
    {
        $modulo = Modulo::findOrFail($moduloId);
        $unidades = $modulo->unidades;
    
        if ($unidades->isNotEmpty()) {
            $unidadId = $unidades->first()->id; // Supongamos que obtienes la primera unidad del módulo
            $unidad = Unidad::findOrFail($unidadId);        
        }
    
        return view('admin.modulo', compact('modulo', 'unidades'));
    }
    

    public function store(Request $request, Modulo $modulo)
    {
        $request->validate([
            'nombreUnidad' => 'required|string',
            'descripcionUnidad' => 'nullable|string',
            'orden'=>'required|integer',
        ]);

        $unidad = new Unidad([
            'nombreUnidad' => $request->input('nombreUnidad'),
            'descripcionUnidad' => $request->input('descripcionUnidad'),    
            'orden'=>$request->input('orden'),
        ]);
        

        $modulo->unidades()->save($unidad);
        
        return redirect()->route('modulo.show', $modulo->id)->with('success', 'Unidad creada con éxito.');
    }

    public function update(Request $request, Unidad $unidad)
    {
        $request->validate([
            'nombreUnidad' => 'required|string',
            'descripcionUnidad' => 'nullable|string',
            'orden'=>'required|integer'
        ]);
    
        $unidad->nombreUnidad = $request->input('nombreUnidad');
        $unidad->descripcionUnidad = $request->input('descripcionUnidad');
        $unidad->orden = $request->input('orden');
        $unidad->save();  // Guardar la unidad actualizada
    
        return redirect()->route('modulo.show', $unidad->modulo_id)->with('success', 'Unidad actualizada con éxito.');
    }
    

    public function destroy(Unidad $unidad)
    {
        $unidad->delete();
        return redirect()->back()->with('success', 'Unidad eliminada con éxito.');
    }
}
