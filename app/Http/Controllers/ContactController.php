<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactoMailable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function index()
    {
        return view('welcome');
    }


    public function enviar(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'correo' => 'required|email',
            'mensaje' => 'required',
        ]);
    
        $data = [
            'nombre' => $request->input('nombre'),
            'correo' => $request->input('correo'),
            'mensaje' => $request->input('mensaje'),
        ];
    
        // Crear el objeto del correo
        $correo = new ContactoMailable($data);
    
        // Configurar el remitente del correo como la dirección de correo proporcionada en el formulario
        $correo->from($data['correo'])
               ->replyTo($data['correo'], $data['nombre']); // Establecer el "Reply-To"
    
        // Enviar el correo
        Mail::to('efaacademy2022@gmail.com')->send($correo);
        
        Session::flash('success', '¡Gracias por tu mensaje! Nos pondremos en contacto contigo pronto.');

        return redirect('/');
    }
            
}
