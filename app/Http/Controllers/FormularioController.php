<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Formulario;
use App\Models\Modulo;
use App\Models\PuntajeAcumulado;
use App\Models\Unidad;

class FormularioController extends Controller
{
    public function show()
    {
        $unidades = Unidad::all();
        $formularios = Formulario::all();

        return view('admin.formulario', compact('unidades', 'formularios'));
    }

    public function crearFormulario(Request $request)
    {

        $request->validate([
            'titulo' => 'required|string|max:255',
            'unidad' => 'required|exists:unidades,id',
            'descripcion' => 'nullable|string',
            'duracion' =>'nullable'
        ]);

        $formulario = new Formulario();
        $formulario->titulo = $request->input('titulo');
        $formulario->unidad_id = $request->input('unidad');
        $formulario->descripcion = $request->input('descripcion');
        $formulario->duracion = $request->input('duracion');
        $formulario->save();

        return redirect()->route('formularios.show')->with('success', 'Formulario creado exitosamente');
    }


    public function actualizarFormulario(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required|string|max:255',
            'unidad' => 'required|exists:unidades,id',
            'descripcion' => 'nullable|string',
            'duracion' =>'nullable'
        ]);

        $formulario = Formulario::findOrFail($id);
        $formularioOriginal = clone $formulario; // Clonar el formulario original antes de realizar cambios

        // Restar puntaje_total del formulario original de la unidad original
        $unidadOriginal = Unidad::find($formulario->unidad_id);
        $unidadOriginal->puntaje_total_unidad -= $formularioOriginal->puntaje_total;

        // Actualizar formulario con la nueva unidad
        $formulario->titulo = $request->input('titulo');
        $formulario->unidad_id = $request->input('unidad');
        $formulario->descripcion = $request->input('descripcion');
        $formulario->duracion = $request->input('duracion');
        $formulario->save();
        // Obtener la nueva unidad del formulario y sumar el puntaje_total al puntaje_total_unidad
        $unidadNueva = Unidad::find($formulario->unidad_id);
        $unidadNueva->puntaje_total_unidad += $formulario->puntaje_total;

        // Guardar cambios en las unidades
        $unidadOriginal->save();
        $unidadNueva->save();

        return redirect()->route('formularios.show')->with('success', 'Formulario actualizado exitosamente');
    }


    public function eliminarFormulario($id)
    {
        $formulario = Formulario::findOrFail($id);
        $formulario->delete();

        $unidad = Unidad::find($formulario->unidad_id);
        $unidad->actualizarPuntajeTotalUnidad();

        return redirect()->route('formularios.show')->with('success', 'Formulario eliminado exitosamente');
    }
}
