<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RespuestaUsuario;
use App\Models\Formulario;
use App\Models\User;
use App\Models\PuntajeAcumulado;
use app\Models\Unidad;
use app\Models\PuntajeUnidad;

class RespuestaUsuarioController extends Controller
{


    public function show()
    {
        $usuarios = User::with('respuestasUsuario.pregunta.formulario', 'puntajesUnidades', 'puntajesModulos')->get();
        return view('admin.respuestas', compact('usuarios'));
    }


    public function guardarCorreccion(Request $request, $respuestaId)
    {
        $respuesta = RespuestaUsuario::findOrFail($respuestaId);

        // Validaciones, si es necesario
        $request->validate([
            'correccion' => 'required|string',
            'puntaje' => 'required|numeric'
        ]);

        // Guardar la corrección en el campo correccion_usuario
        $respuesta->correccion_usuario = $request->input('correccion');
        $respuesta->puntaje = $request->input('puntaje');
        $respuesta->save();

        return redirect()->back()->with('success', 'Corrección guardada exitosamente');
    }
    
    public function mostrarPuntajesUsuarios()
    {
        // Obtener todos los usuarios con sus puntajes acumulados por formulario, unidades y módulos
        $usuariosConPuntajes = User::with([
            'puntajesAcumulados.formulario',
            'puntajesUnidades.unidad',
            'puntajesModulos.modulo'
        ])->get();

        // Pasar los datos a la vista
        return view('admin.notas', compact('usuariosConPuntajes'));
    }
}
