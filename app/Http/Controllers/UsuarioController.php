<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Modulo;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{


   

 public function registro(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'primer_apellido' => 'required',
            'correo' => 'required|unique:users,correo',
            'academia' => 'required',
            'ocupacion' => 'required',
            'fecha_nacimiento' => 'required',
            'celular' => 'required',
            'password' => 'required|min:5',
            'funcion' => 'required|in:admin,estudiante',
            'start_access' => 'nullable|date',
            'end_access' => 'nullable|date|after:start_access',
            'tipo_curso' => 'required|in:Online,Intensivo,Grupal,Regular', 
        ]);


        $data = $request->all();

        $user = User::create($data);

        if ($request->input('funcion') === 'admin') {
            $user->assignRole('admin');
        } else {
            $user->assignRole('estudiante');
        }

        // Asigna las fechas de acceso al usuario si se proporcionaron
        if ($request->filled('start_access') && $request->filled('end_access')) {
            $user->start_access = $request->input('start_access');
            $user->end_access = $request->input('end_access');
            $user->save();
        }
        $mensajeExito = 'Registro exitoso!';

        return redirect('/lista-estudiantes')->with('success', $mensajeExito);
    }
    public function mostrar()
    {
        $usuarios = User::all();
        $modulos = Modulo::all();
        return view('admin.usuarios', compact('usuarios', 'modulos'));
    }


    public function destroy($id)
    {
        $registro = User::findOrFail($id);

        $registro->delete();

        return redirect()->back()->with('success', 'Usuario eliminado correctamente.');
    }



    public function actualizar(Request $request, $id)
    {
        $usuario = User::findOrFail($id);

        $request->validate([
            'nombre' => 'required',
            'primer_apellido' => 'required',
            'correo' => 'required|unique:users,correo,' . $id,
            'academia' => 'required',
            'ocupacion' => 'required',
            'fecha_nacimiento' => 'required|date',
            'celular' => 'required',
            'funcion' => 'required|in:admin,estudiante',
            'tipo_curso' => 'required|in:Online,Intensivo,Grupal,Regular',    
            'password' => 'nullable|min:5',
            'start_access' => 'nullable|date',
            'end_access' => 'nullable|date|after:start_access',
            
        ]);

        $data = $request->all();

        // Asignar la nueva contraseña sin cifrar si se proporciona
        if ($request->filled('password')) {
            $data['password'] = $request->input('password');
        } else {
            // Si no se proporciona una nueva contraseña, elimina la entrada 'password'
            unset($data['password']);
        }
        $usuario->update($data);


        if ($request->input('funcion') === 'admin') {
            $usuario->syncRoles(['admin']);
        } else {
            $usuario->syncRoles(['estudiante']);
        }

        return redirect('/lista-estudiantes')->with('success', 'Usuario actualizado correctamente.');
    }

    //------------Asignacion de modulos a cada usuarios----------------
    public function asignarEditarModulos(Request $request, $userId)
    {
        // Validación de datos común
        $request->validate([
            'modulo_id' => 'array',
        ]);

        $user = User::find($userId);

        if (!$user) {
            return redirect()->back()->with('error', 'Usuario no encontrado');
        }

        // Determinar si estás asignando o editando módulos
        $accion = $request->input('accion');

        if ($accion == 'asignar') {
            // Asignar módulos al usuario
            $user->modulos()->sync($request->modulo_id);
            return redirect()->back()->with('success', 'Módulos asignados exitosamente.');
        } elseif ($accion == 'editar') {
            // Editar módulos asignados al usuario
            $user->modulos()->sync($request->modulo_id);
            return redirect()->back()->with('success', 'Módulos asignados actualizados exitosamente');
        }
        // Si no se reconoce la acción, redireccionar con un mensaje de error
        return redirect()->back()->with('error', 'Acción no reconocida');
    }
}
