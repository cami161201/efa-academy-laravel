<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Contenido;
// use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Storage;



class ContenidoController extends Controller
{

    public function index()
    {
        $contenidos = Contenido::all(); 
        return view('admin.contenido', compact('contenidos'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'titulo' => 'required|string|max:255|min:5',
            'tipo_contenido' => 'required|in:video,audio,pdf,imagen',
            'url_archivo' => 'required|url',
        ]);

            $contenido = new Contenido([
                'titulo' => $request->input('titulo'),
                'tipo_contenido' => $request->input('tipo_contenido'),
                'url_archivo' => $request->input('url_archivo'), 
            ]);

            $contenido->save();
            return back()->with('success', 'Contenido creado exitosamente');
    }


    public function update(Request $request, Contenido $contenido)
    {
        $request->validate([
            'titulo' => 'required|string|max:255|min:5',
            'tipo_contenido' => 'required|in:video,audio,pdf,imagen',
            'url_archivo' => 'required|url',
        ]);

        $contenido->update($request->all());

        return redirect()->back()->with('success', 'Contenido actualizado exitosamente');
    }

    public function destroy(Contenido $contenido)
    {
        $contenido->delete();

        return redirect()->back()->with('success', 'Contenido eliminado exitosamente');
    }
}
