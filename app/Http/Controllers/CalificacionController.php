<?php

namespace App\Http\Controllers;

use App\Models\Formulario;
use Illuminate\Http\Request;
use App\Models\RespuestaUsuario;
use App\Models\User;
use App\Models\Pregunta;
use App\Models\PuntajeAcumulado;
use App\models\PuntajeUnidad;
use App\Models\Unidad;
use App\models\PuntajeModulo;
use App\Models\Modulo;

class CalificacionController extends Controller
{

    public function verFormulario($id, Request $request)
    {
        $formulario = Formulario::with('preguntas')->find($id);

        if (!$formulario) {
            return redirect()->route('bienvenida')->with('error', 'Formulario no encontrado');
        }

        $usuario = auth()->user();

        // Obtener las respuestas anteriores del usuario para este formulario
        $respuestasAnteriores = RespuestaUsuario::where('usuario_id', $usuario->id)
            ->where('formulario_id', $formulario->id)
            ->get();

        return view('user.cuestionario', compact('formulario', 'request', 'respuestasAnteriores'));
    }


    public function guardarRespuestas(Request $request, $formularioId)
    {
        $formulario = Formulario::findOrFail($formularioId);
        $usuario = auth()->user();

        // Verificar si el usuario ya ha respondido este formulario
        $respuestasUsuario = RespuestaUsuario::where('usuario_id', $usuario->id)
            ->where('formulario_id', $formulario->id)
            ->count();

        if ($respuestasUsuario > 0) {
            // El usuario ya ha respondido este formulario, no permitir más respuestas
            return redirect()->route('user.bienvenida')->with('error', 'Ya has respondido este formulario. No se permiten más respuestas.');
        }

        foreach ($formulario->preguntas as $pregunta) {
            $respuesta = new RespuestaUsuario();
            $respuesta->usuario_id = $usuario->id;
            $respuesta->pregunta_id = $pregunta->id;
            $respuesta->formulario_id = $formulario->id;

            // Obtén la respuesta del formulario según el tipo de pregunta
            if ($pregunta->tipo_pregunta == 'Corta') {
                $respuesta->respuesta_usuario = $request->input('respuestaCorta' . $pregunta->id);
            } elseif ($pregunta->tipo_pregunta == 'Larga') {
                $respuesta->respuesta_usuario = $request->input('respuestaLarga' . $pregunta->id);
            } elseif ($pregunta->tipo_pregunta == 'Verdadero/Falso') {
                $respuesta->respuesta_usuario = $request->input('respuestaCorrecta' . $pregunta->id);
            } elseif ($pregunta->tipo_pregunta == 'Opcion multiple') {
                // Obtén las opciones asociadas a la pregunta
                $opciones = $pregunta->opciones;

                $respuestasSeleccionadas = (array)$request->input('opcion' . $pregunta->id, []);


                $puntaje = $this->calificarOpcionMultiple($pregunta, $respuestasSeleccionadas);
                $respuesta->puntaje = $puntaje;
                
                $respuesta->respuesta_usuario = $respuestasSeleccionadas ? implode(', ', $respuestasSeleccionadas) : null;
            } elseif ($pregunta->tipo_pregunta == 'Seleccion multiple') {
                // Obtén las opciones asociadas a la pregunta
                $opciones = $pregunta->opciones;

                // Obtén las respuestas seleccionadas desde el formulario
                $respuestasSeleccionadas = (array)$request->input('opcion' . $pregunta->id, []);

                // Calificar la respuesta para preguntas de Selección Múltiple
                $puntaje = $this->calificarSeleccionMultiple($pregunta, $respuestasSeleccionadas);
                $respuesta->puntaje = $puntaje;

                // Guardar las respuestas seleccionadas
                $respuesta->respuesta_usuario = $respuestasSeleccionadas ? implode(', ', $respuestasSeleccionadas) : null;
            }

            // Calificar la respuesta de tipo corta, verdadero/falso, etc.
            $this->calificarRespuesta($respuesta);

            // Guardar la respuesta
            $respuesta->save();
        }
        $this->actualizarPuntajesAcumuladosFormulario($usuario->id, $formulario->id);
        $unidadId = $formulario->unidad->id;
        $moduloId = $formulario->unidad->modulo->id;
        $this->actualizarPuntajesAcumuladosUnidad($usuario->id, $unidadId);
        $this->actualizarPuntajesAcumuladosModulo($usuario->id, $moduloId);

        return back()->with('success', 'Respuestas guardadas exitosamente.');
    }

    // Método para calificar la respuesta de tipo corta, verdadero/falso, etc.
    private function calificarRespuesta(RespuestaUsuario $respuesta)
    {
        $pregunta = $respuesta->pregunta;

        // Verificar si la pregunta es de tipo "Larga" y asignar puntaje 0
        if ($pregunta->tipo_pregunta == 'Larga') {
            $respuesta->puntaje = 0;
        } elseif ($respuesta->respuesta_usuario === $pregunta->respuestaCorrecta) {
            // Verificar si la respuesta es correcta y asignar puntaje
            $puntaje = $pregunta->puntaje;
            $respuesta->puntaje = $puntaje;
        }

        // Guardar la respuesta
        $respuesta->save();
    }

    private function calificarSeleccionMultiple(Pregunta $pregunta, $respuestas)
    {
        // Obtén las opciones correctas
        $opcionesCorrectas = $pregunta->opciones->filter(function ($opcion) {
            return $opcion->esCorrecta;
        });

        // Verifica si las respuestas seleccionadas coinciden con las opciones correctas
        $respuestasCorrectas = $opcionesCorrectas->pluck('textoOpcion')->toArray();

        // Si el usuario no seleccionó ninguna opción
        if (empty($respuestas)) {
            return 0; // Asigna 0 puntos si no hay ninguna opción seleccionada
        }

        // Si el usuario seleccionó todas las opciones correctas
        if (count($respuestasCorrectas) === count($respuestas) && empty(array_diff($respuestasCorrectas, $respuestas))) {
            return $pregunta->puntaje; // Puntaje completo si las respuestas coinciden
        }

        return 0; // Si no coinciden o si no seleccionó todas las opciones correctas, se asigna 0 puntos
    }

    private function calificarOpcionMultiple(Pregunta $pregunta, $respuestas)
    {
        // Obtén las opciones correctas
        $opcionesCorrectas = $pregunta->opciones->filter(function ($opcion) {
            return $opcion->esCorrecta;
        });

        // Verifica si las respuestas seleccionadas coinciden con las opciones correctas
        $respuestasCorrectas = $opcionesCorrectas->pluck('textoOpcion')->toArray();

        // Si el usuario no seleccionó ninguna opción o seleccionó incorrectamente
        if (empty($respuestas) || !empty(array_diff($respuestas, $respuestasCorrectas))) {
            return 0; // Asigna 0 puntos si no hay coincidencia
        }

        return $pregunta->puntaje; // Puntaje completo si las respuestas coinciden
    }

    public function eliminarRespuestasUsuario(Request $request, $formularioId, $usuarioId)
    {
        $formulario = Formulario::findOrFail($formularioId);
        $usuario = User::findOrFail($usuarioId);

        $respuestasUsuario = RespuestaUsuario::where('usuario_id', $usuario->id)
            ->where('formulario_id', $formulario->id);

        $respuestasEliminadas = $respuestasUsuario->get();

        if ($respuestasUsuario->count() > 0) {
            $respuestasUsuario->delete();

            foreach ($respuestasEliminadas as $respuestaEliminada) {
                $this->actualizarPuntajesAcumuladosFormulario($respuestaEliminada->usuario_id, $respuestaEliminada->formulario_id);
            }

            $unidadId = $formulario->unidad->id;
            $moduloId = $formulario->unidad->modulo->id;
            $this->actualizarPuntajesAcumuladosUnidad($usuario->id, $unidadId);
            $this->actualizarPuntajesAcumuladosModulo($usuario->id, $moduloId);

            return back()->with('success', 'Respuestas del usuario eliminadas exitosamente para este formulario.');
        } else {
            return back()->with('error', 'No se encontraron respuestas del usuario para este formulario.');
        }
    }

    private function actualizarPuntajesAcumuladosFormulario($usuario_id, $formulario_id)
    {
        $respuestasFormulario = RespuestaUsuario::where('usuario_id', $usuario_id)
            ->where('formulario_id', $formulario_id)
            ->get();
    
        $puntajeTotalFormulario = $respuestasFormulario->sum('puntaje');
        $puntajeTotal = Formulario::find($formulario_id)->puntaje_total;
    
        $porcentajeAprobacion = ($puntajeTotalFormulario / $puntajeTotal) * 100;
    
        PuntajeAcumulado::updateOrCreate(
            ['usuario_id' => $usuario_id, 'formulario_id' => $formulario_id],
            [
                'puntaje_obtenido_formulario' => $puntajeTotalFormulario,
                'aprobado' => $porcentajeAprobacion >= 75
            ]
        );
    }


    private function actualizarPuntajesAcumuladosUnidad($usuario_id, $unidad_id)
    {
        $puntajeTotalUnidad = PuntajeAcumulado::where('usuario_id', $usuario_id)
            ->whereHas('formulario', function ($query) use ($unidad_id) {
                $query->where('unidad_id', $unidad_id);
            })
            ->sum('puntaje_obtenido_formulario');
    
        $puntajeTotalUnidadCompleto = Unidad::findOrFail($unidad_id)->puntaje_total_unidad;
    
        $porcentajeAprobacionUnidad = ($puntajeTotalUnidad / $puntajeTotalUnidadCompleto) * 100;
    
        PuntajeUnidad::updateOrCreate(
            ['usuario_id' => $usuario_id, 'unidad_id' => $unidad_id],
            [
                'puntaje_obtenido_unidad' => $puntajeTotalUnidad,
                'aprobado' => $porcentajeAprobacionUnidad >= 75,
            ]
        );
    }
    

    private function actualizarPuntajesAcumuladosModulo($usuario_id, $modulo_id)
    {
        $puntajeTotalModulo = PuntajeUnidad::where('usuario_id', $usuario_id)
            ->whereHas('unidad', function ($query) use ($modulo_id) {
                $query->where('moduloID', $modulo_id);
            })
            ->sum('puntaje_obtenido_unidad');
    
        $puntajeTotalModuloCompleto = Modulo::findOrFail($modulo_id)->puntaje_total_modulo;

        $porcentajeAprobacionModulo = ( $puntajeTotalModulo / $puntajeTotalModuloCompleto) * 100;
        PuntajeModulo::updateOrCreate(
            ['usuario_id' => $usuario_id, 'modulo_id' => $modulo_id],
            [
                'puntaje_obtenido_modulo' => $puntajeTotalModulo,
                'aprobado' =>  $porcentajeAprobacionModulo >= 75,
            ]
        );
    }    
}
