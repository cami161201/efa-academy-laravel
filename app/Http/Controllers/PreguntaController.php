<?php

namespace App\Http\Controllers;

use App\Models\Contenido;
use Illuminate\Http\Request;
use App\Models\Pregunta;
use App\Models\Opcion;
use App\Models\Formulario;
use App\Models\Unidad;

class PreguntaController extends Controller
{

    public function mostrarPreguntas(Request $request, $id)
    {
        $formulario = Formulario::findOrFail($id);
        $contenidos = Contenido::all();
        $formulario->load('preguntas.contenido');

        return view('admin.pregunta', compact('formulario', 'contenidos'));
    }


    public function guardarPregunta(Request $request, $formulario_id)
    {
        $pregunta = new Pregunta();
        $pregunta->formulario_id = $formulario_id;
        $pregunta->texto_pregunta = $request->input('texto_pregunta');
        $pregunta->tipo_pregunta = $request->input('tipo_pregunta');
        $pregunta->respuestaCorrecta = $request->input('respuestaCorrecta');
        $pregunta->puntaje = $request->input('puntaje');

        // Asignar el contenido_id si está presente en la solicitud
        if ($request->has('contenido_id')) {
            $pregunta->contenido_id = $request->input('contenido_id');
        }

        $pregunta->save();
        // Actualizar puntaje_total del formulario
        $formulario = Formulario::find($formulario_id);
        $formulario = Formulario::find($formulario_id);       
        $formulario->actualizarPuntajeTotal(); 
        $formulario->unidad->actualizarPuntajeTotalUnidad();
        $formulario->unidad->modulo->actualizarPuntajeTotalModulo();

        // Código para guardar las opciones en la base de datos si existen
        if ($request->has('opciones')) {
            foreach ($request->input('opciones') as $opcion) {
                $opcion_model = new Opcion();
                $opcion_model->pregunta_id = $pregunta->id;
                $opcion_model->textoOpcion = $opcion['textoOpcion'];
                $opcion_model->esCorrecta = isset($opcion['esCorrecta']);
                $opcion_model->save();
            }
        }

        return back()->with('success', 'Pregunta creada exitosamente');
    }


    public function editarPregunta(Request $request, $pregunta_id)
    {
        // Encuentra la pregunta existente por su ID
        $pregunta = Pregunta::find($pregunta_id);

        // Guarda el ID del formulario antes de la edición
        $formulario_id = $pregunta->formulario_id;

        // Actualiza los campos de la pregunta con los valores del formulario
        $pregunta->texto_pregunta = $request->input('texto_pregunta');
        $pregunta->tipo_pregunta = $request->input('tipo_pregunta');
        $pregunta->respuestaCorrecta = $request->input('respuestaCorrecta');
        $pregunta->puntaje = $request->input('puntaje');

        // Asignar el contenido_id si está presente en la solicitud
        if ($request->has('contenido_id')) {
            $pregunta->contenido_id = $request->input('contenido_id');
        }

        $pregunta->save();

        // Elimina las opciones existentes antes de agregar las nuevas opciones
        Opcion::where('pregunta_id', $pregunta->id)->delete();

        // Código para guardar las opciones en la base de datos si existen
        if ($request->has('opciones')) {
            foreach ($request->input('opciones') as $opcion) {
                $opcion_model = new Opcion();
                $opcion_model->pregunta_id = $pregunta->id;
                $opcion_model->textoOpcion = $opcion['textoOpcion'];
                $opcion_model->esCorrecta = isset($opcion['esCorrecta']);
                $opcion_model->save();
            }
        }

        // Actualizar puntaje_total del formulario después de editar la pregunta
        $formulario = Formulario::find($formulario_id);       
        $formulario->actualizarPuntajeTotal(); 
        $formulario->unidad->actualizarPuntajeTotalUnidad();
        $formulario->unidad->modulo->actualizarPuntajeTotalModulo();

        return back()->with('success', 'Pregunta editada exitosamente');
    }


    public function eliminarPregunta($id)
    {
        $pregunta = Pregunta::findOrFail($id);
        $formulario_id = $pregunta->formulario_id; // Obtener el ID del formulario antes de eliminar la pregunta
        $pregunta->delete();

        // Actualizar puntaje_total del formulario después de eliminar la pregunta
        $formulario = Formulario::find($formulario_id);       
        $unidad = $formulario->unidad;

        $formulario->actualizarPuntajeTotal();
        $unidad->actualizarPuntajeTotalUnidad();

        return redirect()->back()->with('success', 'Pregunta eliminada correctamente.');
    }
}
