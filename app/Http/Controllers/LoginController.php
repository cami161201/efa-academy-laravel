<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function show()
    {
        if (Auth::check()) {
            // Verificar si el usuario es un administrador
            if (Auth::user()->hasRole('admin')) {
                return redirect('/lista-estudiantes'); // Redirigir al panel de administración
            } else {
                return redirect()->route('user.bienvenida'); // Redirigir al panel de usuario
            }
        }
        return view('login');
    }


    public function login(LoginRequest $request)
    {
        $credentials = $request->getCredentials();

        $user = Auth::getProvider()->retrieveByCredentials($credentials);

        if (!$user) {
            return redirect()->to('/login')->withErrors(' Cuenta de estudiante no válida.');
        }

        // Comprobar la contraseña sin cifrar
        if ($user->password != $request->input('password')) {
            return redirect()->to('/login')->withErrors(' Cuenta de estudiante no válida.');
        }

        Auth::login($user);
        return $this->authenticated($request, $user);
    }


    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('admin')) {
            return redirect('/lista-estudiantes'); // Ruta para el panel de administración
        } else {
            return redirect('/bienvenido'); // Ruta para el panel de usuario
        }
    }
    // para hacer logout
    public function logout()
    {
        Session::flush();

        Auth::logout();

        return redirect()->to('/login');
    }
}
