<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Estudiante;
use App\Models\Formulario;
use App\Models\Modulo;

class HomeController extends Controller
{
    public function show()
    {
        return view('user.bienvenida');
    }

    public function showModulo()
    {
        return view('user.curso');
    }

    public function verModulo($id)
    {
        $modulo = Modulo::with(['unidades.formularios.puntajesAcumulados'])->find($id);

        if (!$modulo) {
            return redirect()->route('bienvenida')->with('error', 'Módulo no encontrado');
        }

        return view('user.curso', compact('modulo'));
    }
}
