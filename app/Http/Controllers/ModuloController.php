<?php

namespace App\Http\Controllers;

use App\Models\Modulo;
use App\Models\Contenido;
use Illuminate\Http\Request;

class ModuloController extends Controller
{

    public function show()
    {
        $modulos = Modulo::with('unidades')->get();
        return view('admin.modulo', compact('modulos'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required|unique:modulos,titulo',
            'descripcion' => 'nullable',
            'num_modulo'=> 'required'
        ]);
        $data = $request->all();
        Modulo::create($data);

        return redirect()->back()->with('success', 'Modulo creado exitosamente.');
    }

    public function editar(Request $request, $id)
    {
        $modulo = Modulo::findOrFail($id);
        $modulo->update($request->all());
        return redirect()->back()->with('success', 'Módulo actualizado correctamente.');
    }

    //Eliminar
    public function destroy($id)
    {
        $registro = Modulo::findOrFail($id); // Encuentra el registro por su ID
        $registro->delete(); // Elimina el registro
        return redirect()->back()->with('success', 'Modulo eliminado correctamente.');
    }


    public function mostrarContenido($id)
    {

        $contenido = Contenido::find($id);

        if (!$contenido) {
            abort(404);
        }

        return view('admin.visor', compact('contenido'));
    }
}
