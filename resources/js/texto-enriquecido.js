import tinymce from 'tinymce/tinymce';
import 'tinymce/themes/silver/theme';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/link';

document.addEventListener('DOMContentLoaded', function() {
    tinymce.init({
        selector: 'textarea',
        plugins: 'lists link',
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
        menubar: false,
    });
});
