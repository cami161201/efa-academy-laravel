@extends('inicio')

@section('content')

    <head>
        <link rel="stylesheet" href="css/pagos.css">
    </head>

    <h1>Nuestros paquetes</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Paquete Online</h5>
                        <p class="card-price">$60<span style="font-size: 0.8em;">/mensual</span></p>
                        <p class="card-text">Lorem ipsum dolor sit amet.</p>
                      
                        <button class="whatsapp-btn">
                            <i class="bi bi-whatsapp"></i>
                            Contactar por WhatsApp
                          </button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Paquete Perzonalizado</h5>
                        <p class="card-price">$200<span style="font-size: 0.8em;">/mensual</span></p>
                        <p class="card-text">Lorem ipsum dolor sit amet.
                        </p>
                        <button class="whatsapp-btn">
                            <i class="bi bi-whatsapp"></i>
                            Contactar por WhatsApp
                          </button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Paquete Grupal</h5>
                        <p class="card-price">$50<span style="font-size: 0.8em;">/mensual</span></p>
                        <p class="card-text">Lorem ipsum dolor sit
                            amet.</p>
                            <button class="whatsapp-btn">
                                <i class="bi bi-whatsapp"></i>
                                Contactar por WhatsApp
                              </button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Paquete Intensivo</h5>
                        <p class="card-price">$199.99<span style="font-size: 0.8em;">/mensual</span></p>
                        <p class="card-text">Lorem ipsum dolor sit amet.</p>
                        <button class="whatsapp-btn">
                            <i class="bi bi-whatsapp"></i>
                            Contactar por WhatsApp
                          </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1>Pasos para inscribirte</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col">
                <div class="informacion">
                    <div class="card" style="width: 18rem; text-align: center;">
                        <div class="card-body">
                            <i class="bi bi-1-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                            <h5 class="card-title" style="margin-top: 10px;">Elige y contáctanos</h5>
                            <p class="card-text">
                                Selecciona el paquete y comunícate con nosotros haciendo clic en el botón
                                correspondiente de tu interés desde la app de Whatsapp.
                            </p>
                        </div>
                    </div>


                    <div class="card" style="width: 18rem; text-align: center;">
                        <div class="card-body">
                            <i class="bi bi-2-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                            <h5 class="card-title" style="margin-top: 10px;">Inscríbete y paga</h5>
                            <p class="card-text">
                                Proporciona los datos necesarios para tu inscripción, como ser: nombre completo,
                                correo electrónico, etc.
                            </p>
                        </div>
                    </div>

                    <div class="card" style="width: 18rem; text-align: center;">
                        <div class="card-body">
                            <i class="bi bi-3-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                            <h5 class="card-title" style="margin-top: 10px;">Realiza el pago</h5>
                            <p class="card-text">
                                Una vez confirmados los detalles del curso, te informaremos acerca del costo del
                                paquete que seleccionaste y puedes proceder a realizar tu pago.
                            </p>
                        </div>
                    </div>

                    <div class="card" style="width: 18rem; text-align: center;">
                        <div class="card-body">
                            <i class="bi bi-4-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                            <h5 class="card-title" style="margin-top: 10px;">Confirma</h5>
                            <p class="card-text">
                                Tras recibir y verificar tu pago, te enviaremos una confirmación de inscripción,
                                garantizando tu lugar en el curso.
                            </p>
                        </div>
                    </div>

                    <div class="card" style="width: 18rem; text-align: center;">
                        <div class="card-body">
                            <i class="bi bi-5-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                            <h5 class="card-title" style="margin-top: 10px;">Comienza</h5>
                            <p class="card-text">
                                Te facilitaremos tu nombre de usuario, contraseña para acceder a nuestra plataforma
                                en línea y ya puedes comenzar.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                <i class="bi bi-1-circle-fill" style="font-size: 2em; margin-bottom: 10px;"></i>
                <h5 class="card-title" style="margin-top: 10px;">Elige y contáctanos</h5>
            </button>
          </h2>
          <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show">
            <div class="accordion-body">
         
              <div class="vr" style="height:200px"> </div>
              <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
              Accordion Item #2
            </button>
          </h2>
          <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse">
            <div class="accordion-body">
              <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
              Accordion Item #3
            </button>
          </h2>
          <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse">
            <div class="accordion-body">
              <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
            </div>
          </div>
        </div>
      </div>
@endsection
