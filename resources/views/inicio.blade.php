<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- Fuentes montserrat y roboto--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
    href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;800&family=Roboto:wght@100;300;400;500;700;900&display=swap"
    rel="stylesheet">
    {{-- bootstrap --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <title>EFA academy</title>
</head>
<style>
    .btn-cancelar {
        background-color: #E0E0E0;
        font-family: 'Montserrat', sans-serif;
    }

    .btn-actualizar {
        background-color: #2196F3;
        color: white;
        font-family: 'Montserrat', sans-serif;
    }

    .btn-eliminar {
        background-color: #F44336;
        color: white;
    }

    .btn-crear {
        background-color: #4CAF50;
        color: white;
    }
</style>

<body>
    

    @yield('content')

</body>

</html>
