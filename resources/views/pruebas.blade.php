<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="{{ asset('js/tinymce/tinymce.js') }}"></script>
    <title>Document</title>
</head>

<body>
    <textarea name="" id="editor" cols="30" rows="10"></textarea>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            tinymce.init({
                selector: 'textarea#editor',
                plugins: 'lists link',
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                menubar: false,
            });
        });
    </script>

</body>

</html>
