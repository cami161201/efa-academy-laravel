@if (isset($errors) && count($errors) > 0)
    <div class="alert alert-warning" id="error-alert">
        <ul class="list-unstyled mb-0">
            @foreach ($errors->all() as $error)
                <li><i class="bi bi-exclamation-circle-fill"></i> {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::get('success', false))
    <?php $data = Session::get('success'); ?>
    @if (is_array($data))
        @foreach ($data as $message)
            <div class="alert alert-success" role="alert" id="success-alert">
                <i class="bi bi-check2"></i> 
                {{ $message }}
            </div>
        @endforeach
    @else
        <div class="alert alert-success" role="alert" id="success-alert">
            <i class="bi bi-check2"></i> 
            {{$data}}
        </div>
    @endif
@endif

<script>
    // Mostrar mensajes de error y éxito durante 5 segundos
    const errorAlert = document.getElementById('error-alert');
    const successAlert = document.getElementById('success-alert');

    if (errorAlert) {
        setTimeout(function () {
            errorAlert.style.display = 'none';
        }, 5000);
    }

    if (successAlert) {
        setTimeout(function () {
            successAlert.style.display = 'none';
        }, 5000);
    }
</script>
