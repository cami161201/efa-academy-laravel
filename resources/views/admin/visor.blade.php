@if (session('success'))
    <div class="toast position-fixed bottom-0 end-0 m-3" role="alert" aria-live="assertive" aria-atomic="true"
        data-bs-delay="5000">
        <div class="toast-header">
            <strong class="me-auto">Mensaje de Éxito</strong>
            <small>hace un momento</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            {{ session('success') }}
        </div>
    </div>

    <script>
        var successToast = new bootstrap.Toast(document.querySelector('.toast'));
        successToast.show();
    </script>
@endif

