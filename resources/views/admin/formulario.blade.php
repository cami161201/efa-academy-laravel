<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/admin/formulario.css') }}">
    <script src="{{ asset('js/tinymce/tinymce.js') }}"></script>
    <title>EFA Academy</title>
</head>

<body>
    @extends('admin.home')
    @section('secciones')
        @auth
            @role('admin')
                <h1 class="titulo">Formularios</h1>

                <button type="button" class="btn btn-nuevo" data-bs-toggle="modal" data-bs-target="#crearFormulario">
                    <i class="bi bi-plus-circle"></i>Crear formulario
                </button>

                <form method="POST" action="{{ route('formularios.store') }}">
                    <div class="modal fade" id="crearFormulario" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                        aria-labelledby="crearFormularioLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="crearFormularioLabel">Crear formulario</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>

                                <div class="modal-body">
                                    @csrf
                                    <div>
                                        <label for="titulo">Título<span style="color: red">*</span> <small
                                                style="font-size: 11px">(255 caracteres)</small></label>
                                        <input type="text" class="form-control" name="titulo" id="titulo" required>                                        
                                        <div class="invalid-feedback" id="tituloWarning"></div>
                                    </div>

                                    <div>
                                        <label for="unidad">Unidad <span style="color: red">*</span></label>
                                        <select class="form-select" id="unidad" name="unidad"
                                            aria-label="Floating label select example" required>
                                            <option value="" selected>Selecciona una unidad</option>
                                            @foreach ($unidades as $unidad)
                                                <option value="{{ $unidad->id }}">{{ $unidad->nombreUnidad }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback" id="unidadWarning"></div>
                                    </div>

                                    <div>
                                        <label for="duracion">Duración (minutos) <span style="color: red">*</span></label>
                                        <input type="number" class="form-control" name="duracion" id="duracion" required>
                                        <div class="invalid-feedback" id="duracionWarning"></div>
                                    </div>
                                    <div>
                                        <label for="descripcion">Descripción <small
                                                style="font-size: 11px">(opcional)</small></label>
                                        <textarea name="descripcion" id="descripcion" rows="5"></textarea>                                  
                                        <script>
                                            document.addEventListener('DOMContentLoaded', function() {
                                                tinymce.init({
                                                    selector: 'textarea#descripcion',
                                                    plugins: 'lists link',
                                                    toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | links',
                                                    menubar: false,
                                                    license_key: 'gpl'
                                                });
                                            });
                                        </script>
                                        <div class="invalid-feedback" id="descripcionWarning"></div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-crear" id="btnCrearCurso">Crear Curso</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>


                <div class="container" id="formulario">
                    @foreach ($formularios as $formulario)
                        <div class="row lista-formulario">

                            <div class="col-md-6 d-flex align-items-center informacion">
                                <a class="" href="{{ route('preguntas.show', ['id' => $formulario->id]) }}">
                                    <i class="bi bi-file-earmark-text-fill"></i>{{ $formulario->titulo }}
                                    <small>({{ $formulario->unidad->nombreUnidad }})</small>
                                </a>
                            </div>

                            <div class="col-md-6 botones">
                                <button type="button" class="btn btn-actualizar" data-bs-toggle="modal"
                                    data-bs-target="#editarformulario{{ $formulario->id }}">
                                    <i class="bi bi-pencil-square"></i>
                                </button>

                                <button type="button" class="btn btn-eliminar" data-bs-toggle="modal"
                                    data-bs-target="#eliminarFormulario{{ $formulario->id }}">
                                    <i class="bi bi-trash"></i>
                                </button>


                            </div>
                        </div>

                        <form action="{{ route('formulario.actualizar', $formulario->id) }}" method="POST" id="formEditar">
                            <div class="modal fade" id="editarformulario{{ $formulario->id }}" data-bs-backdrop="static"
                                data-bs-keyboard="false" tabindex="-1"
                                aria-labelledby="editarformulario{{ $formulario->id }}Label" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="editarformulario{{ $formulario->id }}Label">Editar
                                                Formulario</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body row">
                                            @csrf
                                            @method('PUT')

                                            <div class="col-md-6 mb-3">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control" id="titulo" name="titulo"
                                                        placeholder="Titulo" value="{{ $formulario->titulo }}" required>
                                                    <label for="titulo">Titulo</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6 mb-3">
                                                <div class="form-floating">
                                                    <select class="form-select" id="unidad" name="unidad"
                                                        aria-label="Floating label select example" required>
                                                        @foreach ($unidades as $unidad)
                                                            <option value="{{ $unidad->id }}"
                                                                {{ $formulario->unidad_id == $unidad->id ? 'selected' : '' }}>
                                                                {{ $unidad->nombreUnidad }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="unidad">Selecciona el unidad</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12 mb-3">
                                                <label for="duracion">Duración (minutos)</label>
                                                <input type="number" class="form-control" name="duracion" id="duracion"
                                                    value="{{ $formulario->duracion }}">
                                                <div class="invalid-feedback" id="duracionWarning"></div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="form-floating mb-3">
                                                    <label for="descripcion">Descripcion</label>

                                                    <textarea name="descripcion" id="descripcion" rows="5">
                                                        {!! $formulario->descripcion !!}</textarea>                                  
                                                    <script>
                                                        document.addEventListener('DOMContentLoaded', function() {
                                                            tinymce.init({
                                                                selector: 'textarea#descripcion',
                                                                plugins: 'lists link',
                                                                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | links',
                                                                menubar: false,
                                                                license_key: 'gpl'
                                                            });
                                                        });
                                                    </script>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-cancelar"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-actualizar">Editar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form action="{{ route('formulario.eliminar', $formulario->id) }}" method="POST">
                            <div class="modal fade" id="eliminarFormulario{{ $formulario->id }}" data-bs-backdrop="static"
                                data-bs-keyboard="false" tabindex="-1"
                                aria-labelledby="eliminarFormulario{{ $formulario->id }}Label" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">

                                        <div class="modal-body">
                                            @csrf
                                            @method('DELETE')
                                            ¿Está seguro de eliminar el formulario "<strong>{{ $formulario->titulo }}</strong>"?
                                            <br>
                                            Esta acción no se puede deshacer.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-cancelar"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-eliminar">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endforeach

                </div>

                <script src="{{ asset('js/admin/validacionFormulario.js') }}"></script>

                @include('admin.visor')
            @endrole
        @endauth
    @endsection
</body>

</html>
