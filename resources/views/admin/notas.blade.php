@extends('admin.home')


@section('secciones')

    <head>
        <link rel="stylesheet" href="{{ asset('css/admin/notas.css') }}">
    </head>
    <h1 class="titulo">Notas</h1>
    {{-- <div class="container-fluid" id="notas">
        @foreach ($usuariosConPuntajes as $usuario)
            @if ($usuario->puntajesAcumulados->isNotEmpty())
                <table class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th colspan="6" class="nombre">{{ $usuario->nombre }} {{ $usuario->primer_apellido }}
                                {{ $usuario->segundo_apellido }}</th>
                        </tr>
                        <tr>
                            <th>Formulario</th>
                            <th>Unidad</th>
                            <th>Módulo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $puntajesPorUnidad = $usuario->puntajesAcumulados->groupBy('formulario.unidad.id');
                        @endphp
    
                        @foreach ($puntajesPorUnidad as $unidad => $puntajes)
                            @php
                                $unidadNombre = $puntajes->first()->formulario->unidad->nombreUnidad;
                                $unidadPuntaje = round(($puntajes->first()->formulario->unidad->puntajesUnidades->where('usuario_id', $usuario->id)->first()->puntaje_obtenido_unidad / $puntajes->first()->formulario->unidad->puntaje_total_unidad) * 100);
                            @endphp
                         
                            @foreach ($puntajes as $puntaje)
                                <tr>
                                    <td >{{ $puntaje->formulario->titulo }} - Nota: {{ $puntaje->puntaje_obtenido_formulario }} / 100</td>
                                    <td>{{ $unidadNombre }} - Nota: {{ $unidadPuntaje }} / 100</td>
                                    <td >{{ $puntaje->formulario->unidad->modulo->titulo }} - Nota: {{ round(($puntaje->formulario->unidad->modulo->puntajesModulos->where('usuario_id', $usuario->id)->first()->puntaje_obtenido_modulo / $puntaje->formulario->unidad->modulo->puntaje_total_modulo) * 100) }} / 100</td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div> --}}
    
    <div class="container-fluid" id="notas">
        @foreach ($usuariosConPuntajes as $usuario)
            @if ($usuario->puntajesAcumulados->isNotEmpty())
                <table class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th colspan="4" class="nombre">{{ $usuario->nombre }} {{ $usuario->primer_apellido }}
                                {{ $usuario->segundo_apellido }}</th>
                        </tr>
                        <tr>
                            <th>Formulario</th>
                             <th>Nota</th>
                            <th>Unidad</th>
                             <th>Nota</th>
                            {{-- <th>Módulo</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $puntajesPorModulo = $usuario->puntajesAcumulados->groupBy('formulario.unidad.modulo.id');
                        @endphp
    
                        @foreach ($puntajesPorModulo as $modulo => $puntajes)
                            @php
                                $moduloNombre = $puntajes->first()->formulario->unidad->modulo->titulo;
                                $moduloPuntaje = round(($puntajes->first()->formulario->unidad->modulo->puntajesModulos->where('usuario_id', $usuario->id)->first()->puntaje_obtenido_modulo / $puntajes->first()->formulario->unidad->modulo->puntaje_total_modulo) * 100);
                            @endphp
                            <tr>
                                <td colspan="4"><strong>Módulo:</strong> {{ $moduloNombre }} - Nota: {{ $moduloPuntaje }} / 100</td>
                            </tr>
                            @foreach ($puntajes as $puntaje)
                                <tr>
                                    <td >{{ $puntaje->formulario->titulo }}</td>
                                    <td>{{ $puntaje->puntaje_obtenido_formulario }} / 100</td>
                                    <td>{{ $puntaje->formulario->unidad->nombreUnidad }}</td>
                                    <td>{{ round(($puntaje->formulario->unidad->puntajesUnidades->where('usuario_id', $usuario->id)->first()->puntaje_obtenido_unidad / $puntaje->formulario->unidad->puntaje_total_unidad) * 100) }} / 100</td>
                                    {{-- <td>{{ $moduloNombre }} - Nota: {{ $moduloPuntaje }} / 100</td> --}}
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div>
    
 
    
    
@endsection
