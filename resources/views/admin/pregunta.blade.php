<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/admin/pregunta.css') }}">
    
    <script src="{{ asset('js/tinymce/tinymce.js') }}"></script>
    {{-- <script src="{{ asset('js/admin/text-editor.js') }}"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <title>EFA Academy</title>
</head>

<body>
    @extends('admin.home')
    @section('secciones')
        <div id="pregunta">
            <div class="container-fluid" id="descripcionForm">
                <h1 class="titulo">{{ $formulario->titulo }}</h1>
                <p class="text-center"> {!! $formulario->descripcion !!}</p>
            </div>

            <div class="row" style=" width: 70%;">
                <div class="col">
                    <p><strong>Puntaje Total</strong> {{ $formulario->puntaje_total }}</p>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-crear" data-bs-toggle="modal"
                        data-bs-target="#nuevaPregunta{{ $formulario->id }}" id="botonFlotante">
                        <i class="bi bi-plus-circle"></i> Agregar pregunta
                    </button>
                </div>
            </div>         
            @foreach ($formulario->preguntas as $pregunta)
                <div class="pregunta">
                    <div id="contenidoPregunta">
                        @if ($pregunta->contenido)
                            @if ($pregunta->contenido->tipo_contenido === 'audio')
                                <audio src="{{ $pregunta->contenido->url_archivo }}" controls controlsList="nodownload"
                                    style="width: 90%"></audio>
                            @elseif ($pregunta->contenido->tipo_contenido === 'imagen')
                                <img src="{{ $pregunta->contenido->url_archivo }}" alt="{{ $pregunta->contenido->titulo }}"
                                    style="width:60%">
                            @elseif ($pregunta->contenido->tipo_contenido === 'video')
                                <video width="100%" src="{{ $pregunta->contenido->url_archivo }}" controls
                                    controlsList="nodownload"></video>
                            @elseif ($pregunta->contenido->tipo_contenido === 'pdf')
                                {{-- <a href="{{ $pregunta->contenido->url_archivo }}" class="btn btn-primary"
                                target="_blank"><i class="bi bi-eye"></i> {{ $pregunta->contenido->titulo }}</a> --}}
                                <iframe src="{{ $pregunta->contenido->url_archivo }}" width="700"
                                    height="600"></iframe>
                            @else
                            @endif
                        @endif
                    </div>
                    <div class="row">
                        <div class="col mt-2">
                            {!! $pregunta->texto_pregunta !!}

                        </div>

                        <div class="col-auto">
                            <div class="dropdown">
                                <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    <i class="bi bi-three-dots-vertical"></i>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                            data-bs-target="#editarPregunta{{ $pregunta->id }}">
                                            <i class="bi bi-pencil-square"></i>
                                            Editar pregunta
                                        </button>
                                    </li>

                                    <li>
                                        <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                            data-bs-target="#eliminarPregunta{{ $pregunta->id }}">
                                            <i class="bi bi-trash"></i>
                                            Eliminar pregunta
                                        </button>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="respuesta">
                            @if ($pregunta->tipo_pregunta == 'Corta')
                                <input type="text" class="form-control" value="{{ $pregunta->respuestaCorrecta }}"
                                    readonly id="respuestaCorta{{ $pregunta->id }}">
                            @elseif ($pregunta->tipo_pregunta == 'Verdadero/Falso')
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                        name="respuestaCorrecta{{ $pregunta->id }}" id="verdadero{{ $pregunta->id }}"
                                        value="Verdadero" @if ($pregunta->respuestaCorrecta == 'Verdadero') checked @endif>
                                    <label class="form-check-label" for="verdadero{{ $pregunta->id }}">
                                        Verdadero
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                        name="respuestaCorrecta{{ $pregunta->id }}" id="falso{{ $pregunta->id }}"
                                        value="Falso" @if ($pregunta->respuestaCorrecta == 'Falso') checked @endif>
                                    <label class="form-check-label" for="falso{{ $pregunta->id }}">
                                        Falso
                                    </label>
                                </div>
                            @elseif ($pregunta->tipo_pregunta == 'Seleccion multiple')
                                @foreach ($pregunta->opciones as $opcion)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="{{ $opcion->textoOpcion }}"
                                            id="opcion{{ $opcion->id }}"
                                            @if ($opcion->esCorrecta) checked @endif disabled>
                                        <label class="form-check-label" for="opcion{{ $opcion->id }}">
                                            {{ $opcion->textoOpcion }}
                                        </label>
                                    </div>
                                @endforeach
                            @elseif ($pregunta->tipo_pregunta == 'Opcion multiple')
                                <input class="form-check-input" type="radio" name="respuestaCorrecta_{{ $pregunta->id }}"
                                    id="pregunta{{ $pregunta->id }}" checked disabled>
                                <label class="form-check-label" for="pregunta{{ $pregunta->id }}">
                                    {{ $pregunta->respuestaCorrecta }}
                                </label>
                                @foreach ($pregunta->opciones as $opcion)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="respuestaCorrecta_{{ $pregunta->id }}" id="opcion{{ $opcion->id }}"
                                            value="{{ $opcion->textoOpcion }}" disabled>
                                        <label class="form-check-label" for="opcion{{ $opcion->id }}">
                                            {{ $opcion->textoOpcion }}
                                        </label>
                                    </div>
                                @endforeach
                                <style>
                                    .form-check-input:disabled {
                                        opacity: 1;
                                    }

                                    .form-check-input:disabled+.form-check-label {
                                        color: #000;
                                        opacity: 1;
                                    }
                                </style>
                            @elseif ($pregunta->tipo_pregunta == 'Larga')
                                <input type="text" class="form-control mb-3"
                                    value="La respuesta de esta pregunta es para revision del docente" readonly
                                    id="respuestaLarga{{ $pregunta->id }}">
                            @endif
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <span><strong>Puntaje:</strong> {{ $pregunta->puntaje }}</span>
                                </div>
                                <div class="col-md-6 d-flex justify-content-end">
                                    <span><strong>Tipo de pregunta:</strong> {{ $pregunta->tipo_pregunta }}</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <form action="{{ route('eliminar.pregunta', $pregunta->id) }}" method="POST">
                    <div class="modal fade" id="eliminarPregunta{{ $pregunta->id }}" data-bs-backdrop="static"
                        data-bs-keyboard="false" tabindex="-1"
                        aria-labelledby="eliminarPregunta{{ $pregunta->id }}Label" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    @csrf
                                    @method('DELETE')
                                    Esta seguro que desea eliminar la pregunta?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-cancelar"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-eliminar">Eliminar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <form action="{{ route('editar.pregunta', ['pregunta_id' => $pregunta->id]) }}" method="POST">
                    <div class="modal fade" id="editarPregunta{{ $pregunta->id }}" data-bs-backdrop="static"
                        data-bs-keyboard="false" tabindex="-1" aria-labelledby="editarPregunta{{ $pregunta->id }}Label"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="editarPregunta{{ $pregunta->id }}Label">Editar
                                        Pregunta</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    @method('PUT')

                                    <label for="texto_pregunta_editar">Pregunta:</label><br>
                                    {{-- <input class="form-control" type="text" id="texto_pregunta_editar"
                                        name="texto_pregunta" > --}}
                                    <textarea class="form-control" name="texto_pregunta" id="texto_pregunta_editar" cols="30" rows="5"
                                    >{{ $pregunta->texto_pregunta }}</textarea>
                                        <script>
                                            document.addEventListener('DOMContentLoaded', function() {
                                                tinymce.init({
                                                    selector: 'textarea#texto_pregunta_editar',
                                                    plugins: 'lists link',
                                                    toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | links',
                                                    menubar: false,
                                                    license_key: 'gpl'
                                                });
                                            });
                                        </script>

                                    <div class="row">
                                        <div class="col-md-7">
                                            <label for="tipo_pregunta_editar">Tipo de Pregunta:</label>
                                            <input type="text" class="form-control" id="tipo_pregunta_editar"
                                                name="tipo_pregunta" value="{{ $pregunta->tipo_pregunta }}" readonly>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="puntaje">Puntaje</label><br>
                                            <input class="form-control" type="number" id="puntaje" name="puntaje"
                                                min="0" max="100" value="{{ $pregunta->puntaje }}">
                                        </div>
                                    </div>
                                    <label for="contenido">Contenido (opcional)</label>
                                    <select class="form-select" aria-label="Default select example" name="contenido_id"
                                        id="contenido">
                                        <option selected value="">Selecciona un contenido (opcional)</option>
                                        @foreach ($contenidos as $contenido)
                                            <option value="{{ $contenido->id }}"
                                                @if ($contenido->id == $pregunta->contenido_id) selected @endif>{{ $contenido->titulo }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <div id="opciones-editar">
                                        <div id="opciones-container-editar">
                                            @if ($pregunta->tipo_pregunta == 'Corta')
                                                <label for="respuestaCorrecta">Respuesta Correcta</label><br>
                                                <input class="form-control mb-3" type="text" id="respuestaCorrecta"
                                                    name="respuestaCorrecta" value="{{ $pregunta->respuestaCorrecta }}">
                                            @elseif ($pregunta->tipo_pregunta == 'Seleccion multiple')
                                                <h5>Opciones</h5>


                                                @foreach ($pregunta->opciones as $opcion)
                                                    <div>
                                                        <input class="form-control mb-3" type="text"
                                                            name="opciones[{{ $opcion->id }}][textoOpcion]"
                                                            id="opcion-editar{{ $opcion->id }}"
                                                            value="{{ $opcion->textoOpcion }}">
                                                        <label>
                                                            <input class="form-check-input mb-3" type="checkbox"
                                                                name="opciones[{{ $opcion->id }}][esCorrecta]"
                                                                {{ $opcion->esCorrecta ? 'checked' : '' }}>
                                                            Es Correcta
                                                        </label>

                                                    </div>
                                                @endforeach
                                            @elseif ($pregunta->tipo_pregunta == 'Verdadero/Falso')
                                                <p>Selecciona la respuesta:</p>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="respuestaCorrecta" value="Falso" id="Falso"
                                                        {{ $pregunta->respuestaCorrecta == 'Falso' ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="Falso">Falso</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="respuestaCorrecta" value="Verdadero" id="Verdadero"
                                                        {{ $pregunta->respuestaCorrecta == 'Verdadero' ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="Verdadero">Verdadero</label>
                                                </div>
                                            @elseif ($pregunta->tipo_pregunta == 'Opcion multiple')
                                                <label for="respuestaCorrecta">Respuesta Correcta</label><br>
                                                <input class="form-control mb-3" type="text" id="respuestaCorrecta"
                                                    name="respuestaCorrecta" value="{{ $pregunta->respuestaCorrecta }}">
                                                <h5>Opciones</h5>

                                                <br>
                                                @foreach ($pregunta->opciones as $opcion)
                                                    <input class="form-control mb-3" type="text"
                                                        name="opciones[{{ $opcion->id }}][textoOpcion]"
                                                        id="opcion-editar{{ $opcion->id }}"
                                                        value="{{ $opcion->textoOpcion }}">
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-cancelar"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-editar">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endforeach


        </div>
        <form action="{{ route('crear.pregunta', ['formulario_id' => $formulario->id]) }}" method="POST">
            <div class="modal fade" id="nuevaPregunta{{ $formulario->id }}" data-bs-backdrop="static"
                data-bs-keyboard="false" tabindex="-1" aria-labelledby="nuevaPregunta{{ $formulario->id }}Label"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="nuevaPregunta{{ $formulario->id }}Label">Nueva Pregunta</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            @csrf

                            <label for="texto_pregunta">Pregunta:</label><br>
                            <textarea name="texto_pregunta" id="texto_pregunta" cols="30" rows="10"></textarea>
                            <script>
                                document.addEventListener('DOMContentLoaded', function() {
                                    tinymce.init({
                                        selector: 'textarea#texto_pregunta',
                                        plugins: 'lists link',
                                        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | links',
                                        menubar: false,
                                        license_key: 'gpl'
                                    });
                                });
                            </script>

                            <div id="texto_pregunta-error" class="invalid-feedback"></div>

                            <div class="row mb-3">
                                <div class="col-md-8">
                                    <label for="tipo_pregunta_crear">Tipo de Pregunta:</label>
                                    <select class="form-select campo" id="tipo_pregunta_crear" name="tipo_pregunta">
                                        <option value="Corta">Respuesta Corta</option>
                                        <option value="Larga">Respuesta Larga</option>
                                        <option value="Verdadero/Falso">Verdadero/Falso</option>
                                        <option value="Opcion multiple">Opción Múltiple</option>
                                        <option value="Seleccion multiple">Selección Múltiple</option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="puntaje">Puntaje</label><br>
                                    <input class="form-control campo" type="number" id="puntaje" name="puntaje"
                                        min="0" max="100" autocomplete="off" required>
                                    <div id="puntaje-error" class="invalid-feedback"></div>
                                </div>
                            </div>
                            <select class="form-select mt-3 campo" aria-label="Default select example"
                                name="contenido_id">
                                <option selected value="">Selecciona un contenido (opcional)</option>
                                @foreach ($contenidos as $contenido)
                                    <option value="{{ $contenido->id }}">{{ $contenido->titulo }}</option>
                                @endforeach
                            </select>

                            <div id="respuestaCorrectaDiv-crear">
                                <label for="respuestaCorrecta">Respuesta Correcta</label><br>
                                <input class="form-control campo" type="text" id="respuestaCorrecta"
                                    name="respuestaCorrecta" autocomplete="off">
                            </div>

                            <div id="opciones-crear" style="display: none; margin:2% 0%;">
                                <div class="row">
                                    <div class="col">
                                        <h5>Opciones de respuestas</h5>
                                    </div>
                                    <div class="col-auto">
                                        <button class="btn btn-crear" type="button" onclick="agregarOpcionCrear()"><i
                                                class="bi bi-plus-circle"></i> Agregar
                                            Opción</button>
                                    </div>
                                </div>


                                <div id="opciones-container-crear">
                                </div>

                            </div>

                            <div id="verdaderoFalsoDiv-crear" style="display: none;">
                                <strong>Selecciona la respuesta:</strong><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="respuestaCorrecta"
                                        value="Falso">
                                    <label class="form-check-label" for="respuestaCorrecta">Falso</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="respuestaCorrecta"
                                        value="Verdadero">
                                    <label class="form-check-label" for="respuestaCorrecta">Verdadero</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-crear">Crear Pregunta</button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
        <script src="{{ asset('js/CrearOpciones.js') }}"></script>


        @include('admin.visor')
    @endsection
</body>

</html>
