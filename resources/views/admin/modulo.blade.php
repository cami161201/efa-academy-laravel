@extends('admin.home')

<head>
    <link rel="stylesheet" href="{{ asset('css/admin/modulo.css') }}">
</head>
@section('secciones')
    @auth
        @role('admin')
            <h1 class="titulo">Modulos</h1>

            <button type="button" class="btn btn-nuevo" data-bs-toggle="modal" data-bs-target="#nuevoModulo">
                <i class="bi bi-plus-circle"></i> Crear modulo
            </button>

            <div class="container" id="modulos">
                @foreach ($modulos as $modulo)
                    <div class="row lista-modulos">
                        <div class="col-md-6  d-flex align-items-center informacion">
                            <a class="" data-bs-toggle="collapse" href="#modulo{{ $modulo->id }}" role="button"
                                aria-expanded="false" aria-controls="modulo{{ $modulo->id }}">
                                <i class="bi bi-bookmark-fill"></i>
                                Modulo {{$modulo->num_modulo}}: {{ $modulo->titulo }}
                            </a>
                        </div>

                        <div class="col-md-6 botones">
                            <button type="button" class="btn btn-actualizar" data-bs-toggle="modal"
                                data-bs-target="#editarModal{{ $modulo->id }}">
                                <i class="bi bi-pencil-square"></i>
                            </button>
                            <button type="button" class="btn btn-eliminar" data-bs-toggle="modal"
                                data-bs-target="#eliminarModulo{{ $modulo->id }}">
                                <i class="bi bi-trash-fill"></i>
                            </button>
                        </div>
                        <div class="collapse info-modulo" id="modulo{{ $modulo->id }}">
                            <p class="descripcion">
                                {{ $modulo->descripcion }}
                            </p>
                            
                            <p class="titulo-unidad d-flex justify-content-between">
                                Unidades
                                <button type="button" class="btn btn-crear p-1" data-bs-toggle="modal"
                                    data-bs-target="#anadirUnidad{{ $modulo->id }}">
                                    <i class="bi bi-plus-circle"></i>
                                    Agregar unidad
                                </button>
                            </p>

                            @foreach ($modulo->unidades as $unidad)
                                <div class="unidad row">
                                    <div class="col-md-10 d-flex align-items-center">
                                        <a data-bs-toggle="collapse" href="#unidad{{ $unidad->id }}" aria-expanded="false"
                                            aria-controls="unidad{{ $unidad->id }}">
                                            <i class="bi bi-folder-fill"></i>
                                            {{ $unidad->nombreUnidad }}
                                        </a>
                                    </div>

                                    <div class="btn-unidad col-md-2 d-md-flex justify-content-md-end">
                                        <button type="button" class="btn btn-actualizar" data-bs-toggle="modal"
                                            data-bs-target="#editarUnidad{{ $unidad->id }}">
                                            <i class="bi bi-pencil-square"></i>
                                        </button>

                                        <button type="button" class="btn btn-eliminar" data-bs-toggle="modal"
                                            data-bs-target="#eliminarUnidad{{ $unidad->id }}">
                                            <i class="bi bi-trash-fill"></i>
                                        </button>
                                    </div>


                                </div>

                                <div class="collapse cont-unidad" id="unidad{{ $unidad->id }}">
                                    <p>{{ $unidad->descripcionUnidad }}</p>                                 
                                    @if ($unidad->formularios)
                                        <ul>
                                            @foreach ($unidad->formularios as $formulario)                                                                             
                                            <li>
                                                <a href="{{ route('preguntas.show', ['id' => $formulario->id]) }}" type="button"
                                                    class="mb-2 formulario"><i class="bi bi-file-text-fill"></i> {{ $formulario->titulo }} </a>                                                                                      
                                            </li>
                                        
                                            @endforeach
                                        </ul>
                                       
                                    @else
                                        No hay formularios disponibles.
                                    @endif


                                </div>

                                {{-- Actualizar unidad --}}
                                <form action="{{ route('unidad.update', $unidad->id) }}" method="POST">
                                    <div class="modal fade" id="editarUnidad{{ $unidad->id }}" data-bs-backdrop="static"
                                        data-bs-keyboard="false" tabindex="-1"
                                        aria-labelledby="editarUnidad{{ $unidad->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title fs-5" id="editarUnidad{{ $unidad->id }}Label">
                                                        Actualizar unidad</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    @csrf
                                                    @method('PUT')

                                                    <div class="mb-3">
                                                        <label for="orden" class="form-label">Orden</label>
                                                        <input class="form-control" type="text"
                                                            id="orden" name="orden" value="{{ $unidad->orden }}">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="tituloModulo" class="form-label">Módulo</label>
                                                        <input class="form-control" type="text" disabled
                                                            value="{{ $modulo->titulo }}">
                                                    </div>
                                                    
                                                    <div class="mb-3">
                                                        <label for="tituloUnidad" class="form-label">Título de la
                                                            unidad <span style="color: red">*</span> <small
                                                            style="font-size: 11px">(255 caracteres)</small></label>
                                                        <input type="text" class="form-control"
                                                            id="tituloUnidad{{ $unidad->id }}" name="nombreUnidad"
                                                            value="{{ $unidad->nombreUnidad }}" maxlength="255" required>
                                                    </div>

                                                    <div class="mb-3">
                                                        <label for="descripcionUnidad">Descripción:</label>
                                                        <textarea class="form-control" id="descripcionUnidad{{ $unidad->id }}" name="descripcionUnidad" rows="4">{{ $unidad->descripcionUnidad }}</textarea>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-cancelar"
                                                        data-bs-dismiss="modal">Cancelar</button>
                                                    <button type="submit" class="btn btn-actualizar">Actualizar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                {{-- Eliminar unidad --}}
                                <form action="{{ route('unidad.destroy', $unidad->id) }}" method="POST">
                                    <div class="modal fade" id="eliminarUnidad{{ $unidad->id }}" tabindex="-1"
                                        aria-labelledby="eliminarUnidad{{ $unidad->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="d-flex align-items-center justify-content-center">
                                                        <i class="bi bi-exclamation-triangle-fill text-danger me-2"
                                                            style="font-size: 1.5em;"></i>
                                                        <strong class="fs-4">¡Advertencia!</strong>
                                                    </div>
                                                    <p class="mb-2">Al eliminar esta unidad resultará en la pérdida de los
                                                        formularios que pertenecen a la unidad</p>
                                                    <ul>
                                                        @foreach ($unidad->formularios as $formulario)
                                                            <li>Formulario: {{ $formulario->titulo }}</li>
                                                        @endforeach
                                                    </ul>
                                                    <p class="mb-3 text-center">
                                                        <strong class="text-center">¿Confirma que desea eliminar la unidad 
                                                            {{ $unidad->nombreUnidad }}?
                                                        </strong>
                                                    </p>                                                  
                                                    <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                                        <button type="button" class="btn btn-cancelar me-2"
                                                            data-bs-dismiss="modal">Cancelar</button>
                                                        <button type="submit" class="btn btn-danger">Eliminar</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endforeach

                        </div>
                    </div>

                    {{-- Editar un modulo --}}
                    <form action="{{ route('modulo.actualizar', $modulo->id) }}" method="POST">
                        <div class="modal fade" id="editarModal{{ $modulo->id }}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="editarModalLabel{{ $modulo->id }}"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="editarModalLabel{{ $modulo->id }}">
                                            Editar módulo
                                        </h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">

                                        @csrf
                                        @method('PUT')

                                        <label for="num_modulo">Numero de modulo</label>
                                        <input type="number" min="0" max="99" name="num_modulo" id="num_modulo" class="form-control" value="{{ $modulo->num_modulo }}">

                                        <label for="titulo">Título del módulo <span style="color: red">*</span> <small
                                            style="font-size: 11px">(255 caracteres)</small></label>
                                        <input type="text" class="form-control" name="titulo" id="titulo"
                                            value="{{ $modulo->titulo }}" required maxlength="255">

                                        <label for="descripcion">Descripción:</label>
                                        <textarea class="form-control" name="descripcion" id="descripcion" rows="4">{{ $modulo->descripcion }}</textarea>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-cancelar"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-actualizar">Guardar Cambios</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{-- Eliminar un modulo --}}
                    <form action="{{ route('modulo.eliminar', $modulo->id) }}" method="POST">

                        <div class="modal fade" id="eliminarModulo{{ $modulo->id }}" tabindex="-1"
                            aria-labelledby="eliminarModulo{{ $modulo->id }}Label" aria-hidden="true"
                            data-bs-backdrop="static">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        @method('DELETE')
                                        @csrf
                                        <div class="d-flex align-items-center justify-content-center">
                                            <i class="bi bi-exclamation-triangle-fill text-danger me-2"
                                                style="font-size: 1.5em;"></i>
                                            <strong class="fs-4">¡Advertencia!</strong>
                                        </div>
                                        <p class="mb-2">Al eliminar este modulo resultará en la pérdida de las
                                            unidades y formularios que pertenecen al módulo</p>
                                        <ul>
                                            @foreach ($modulo->unidades as $unidad)
                                                <li>Unidad: {{ $unidad->nombreUnidad }}
                                                    @if ($unidad->formularios)
                                                        <ul>
                                                            @foreach ($unidad->formularios as $formulario)
                                                                <li>Formulario: {{ $formulario->titulo }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>



                                        <p class="mb-3 text-center">
                                            <strong class="text-center">¿Confirma que desea eliminar el módulo 
                                                {{ $modulo->titulo }}?
                                            </strong>
                                        </p>

                                        <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                            <button type="button" class="btn btn-cancelar me-2"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-danger">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{-- Crear una unidad --}}
                    <form action="{{ route('unidad.store', ['modulo' => $modulo->id]) }}" method="POST">
                        <div class="modal fade" id="anadirUnidad{{ $modulo->id }}" tabindex="-1"
                            aria-labelledby="anadirUnidadLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="anadirUnidadLabel">Nueva unidad</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <input type="hidden" name="modulo_id" value="{{ $modulo->id }}">
                                    <div class="modal-body">

                                        @csrf
                                        <div class="mb-3">
                                            <label for="orden" class="form-label">Orden</label>
                                            <input class="form-control" type="text"
                                                id="orden" name="orden">
                                        </div>
                                        <div class="mb-3">
                                            <label for="tituloModulo" class="form-label">Módulo</label>
                                            <input class="form-control" type="text" value="{{ $modulo->titulo }}"
                                                id="tituloModulo" readonly>
                                        </div>

                                        <div class="mb-3">
                                            <label for="nombreUnidad" class="form-label">Título de la unidad <span style="color: red">*</span> <small
                                                style="font-size: 11px">(255 caracteres)</small></label>
                                            <input type="text" class="form-control" id="nombreUnidad{{ $modulo->id }}"
                                                name="nombreUnidad" required maxlength="255">
                                            <div id="nombreUnidad-error{{ $modulo->id }}" class="invalid-feedback"></div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="descripcionUnidad" class="form-label">Descripción</label>
                                            <textarea class="form-control" id="descripcionUnidad{{ $modulo->id }}" name="descripcionUnidad" rows="4" maxlength="500"></textarea>
                                            <div id="descripcionUnidad-error{{ $modulo->id }}" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-crear"
                                            id="btnCrearUnidad{{ $modulo->id }}">Crear
                                            unidad</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>

            {{-- Crear un nuevo modulo --}}
            <form action="{{ route('modulo.registrar') }}" method="POST">
                <div class="modal fade" id="nuevoModulo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                    aria-labelledby="nuevoModuloLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title fs-5" id="nuevoModuloLabel">
                                    Nuevo modulo
                                </h2>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                @csrf
                                <label for="num_modulo">Numero de modulo</label>
                                <input type="number" min="0" max="99" name="num_modulo" id="num_modulo" class="form-control">

                                <label for="tituloModulo">Título del módulo <span style="color: red">*</span> <small
                                        style="font-size: 11px">(255 caracteres)</small></label>
                                <input type="text" class="form-control" name="titulo" id="tituloModulo" required maxlength="255">
                                <div class="invalid-feedback" id="tituloModulo-error"></div>
                                 
                                <label for="descripcionModulo">Descripción <small style="font-size: 11px">(500
                                        caracteres)</small></label>
                                <textarea class="form-control" name="descripcion" id="descripcionModulo" rows="5" maxlength="500"></textarea>
                                <div class="invalid-feedback" id="descripcionModulo-error"></div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-crear" id="btnCrearCurso">Crear
                                    Curso</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

       
            
            
            
            @include('admin.visor')
        @endrole
    @endauth
@endsection
