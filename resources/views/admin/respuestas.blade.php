@extends('admin.home')

@section('secciones')

    <head>
        <link rel="stylesheet" href="{{ asset('css/admin/respuesta.css') }}">
    </head>
    <h1 class="titulo">Respuestas</h1>
    @php
        use App\Models\Formulario;
    @endphp
    <div id="respuesta" class="d-flex align-items-center justify-content-center">
        <table class="table table-responsive table-bordered table-hover">
            <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Fecha de entrega</th>
                    <th>Formulario</th>

                    <th></th>                       
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                    @if ($usuario->respuestasUsuario->count() > 0)
                        @foreach ($usuario->respuestasUsuario->groupBy('formulario_id') as $formularioId => $respuestasPorFormulario)
                            @php
                                $formulario = Formulario::find($formularioId);
                                $modalId = "modalFormulario{$formularioId}_{$usuario->id}";
                            @endphp
                            <tr>
                                <td>{{ $usuario->nombre }} {{ $usuario->primer_apellido }}</td>
                                <td>
                                    {{ \Carbon\Carbon::parse($respuestasPorFormulario->first()->created_at)->locale('es')->isoFormat('D [de] MMMM [del] YYYY [Hora:] H:mm') }}
                                </td>
                                <td>
                                    <button type="button" class="btn btn-actualizar" data-bs-toggle="modal"
                                        data-bs-target="#{{ $modalId }}{{ $usuario->id }}" style="width: 90%">
                                        {{ $formulario->titulo }}
                                    </button>
                                </td>
                                <td>
                                    <!-- Botón para abrir el modal de eliminación -->
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                        data-bs-target="#eliminarModal{{ $formularioId }}_{{ $usuario->id }}">
                                        <i class="bi bi-trash-fill"></i>
                                    </button>

                                </td>
                                                          
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="{{ $modalId }}{{ $usuario->id }}" tabindex="-1"
                                aria-labelledby="{{ $modalId }}{{ $usuario->id }}Label" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="{{ $modalId }}Label">
                                                {{ $formulario->titulo }}</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p><strong>Modulo:</strong> {{ $formulario->unidad->modulo->titulo }}</p>
                                            <p> <strong>Unidad:</strong> {{ $formulario->unidad->nombreUnidad }}</p>

                                            @foreach ($respuestasPorFormulario as $respuesta)
                                                @if ($respuesta->pregunta)
                                                    <div class="pregunta border">
                                                        <p>{{ $respuesta->pregunta->texto_pregunta }}</p>


                                                        <div class="respuesta">
                                                            @if ($respuesta->pregunta->tipo_pregunta == 'Corta')
                                                                <input type="text" class="form-control"
                                                                    value="{{ $respuesta->respuesta_usuario }}" readonly
                                                                    id="respuestaCorta{{ $respuesta->pregunta->id }}{{ $usuario->id }}">
                                                            @elseif ($respuesta->pregunta->tipo_pregunta == 'Verdadero/Falso')
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="respuestaCorrecta{{ $respuesta->pregunta->id }}"
                                                                        id="verdadero{{ $respuesta->pregunta->id }}{{ $usuario->id }}"
                                                                        value="Verdadero"
                                                                        @if ($respuesta->respuesta_usuario == 'Verdadero') checked @endif
                                                                        disabled>
                                                                    <label class="form-check-label"
                                                                        for="verdadero{{ $respuesta->pregunta->id }}{{ $usuario->id }}">
                                                                        Verdadero
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="respuestaCorrecta{{ $respuesta->pregunta->id }}"
                                                                        value="Falso"
                                                                        id="falso{{ $respuesta->pregunta->id }}{{ $usuario->id }}"
                                                                        @if ($respuesta->respuesta_usuario == 'Falso') checked @endif
                                                                        disabled>
                                                                    <label class="form-check-label"
                                                                        for="falso{{ $respuesta->pregunta->id }}{{ $usuario->id }}">
                                                                        Falso
                                                                    </label>
                                                                </div>
                                                            @elseif ($respuesta->pregunta->tipo_pregunta == 'Seleccion multiple')
                                                                @foreach ($respuesta->pregunta->opciones as $opcion)
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            value="{{ $opcion->textoOpcion }}"
                                                                            id="opcion{{ $opcion->id }}{{ $usuario->id }}"
                                                                            @if (in_array($opcion->textoOpcion, explode(', ', $respuesta->respuesta_usuario))) checked @endif
                                                                            disabled>
                                                                        <label class="form-check-label"
                                                                            for="opcion{{ $opcion->id }}{{ $usuario->id }}">
                                                                            {{ $opcion->textoOpcion }}
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                            @elseif ($respuesta->pregunta->tipo_pregunta == 'Opcion multiple')
                                                                <input class="form-check-input" type="radio"
                                                                    name="respuestaCorrecta_{{ $respuesta->pregunta->id }}"
                                                                    id="pregunta{{ $respuesta->pregunta->id }}{{ $usuario->id }}"
                                                                    value="{{ $respuesta->pregunta->respuestaCorrecta }}"
                                                                    checked disabled>
                                                                <label class="form-check-label"
                                                                    for="pregunta{{ $respuesta->pregunta->id }}{{ $usuario->id }}">{{ $respuesta->pregunta->respuestaCorrecta }}
                                                                </label>
                                                                @foreach ($respuesta->pregunta->opciones as $opcion)
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio"
                                                                            name="respuestaCorrecta_{{ $respuesta->pregunta->id }}"
                                                                            id="opcion{{ $opcion->id }}{{ $usuario->id }}"
                                                                            value="{{ $opcion->textoOpcion }}"
                                                                            @if ($opcion->textoOpcion == $respuesta->respuesta_usuario) checked @endif
                                                                            disabled>
                                                                        <label class="form-check-label"
                                                                            for="opcion{{ $opcion->id }}{{ $usuario->id }}">
                                                                            {{ $opcion->textoOpcion }}
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                                <style>
                                                                    .form-check-input:disabled {
                                                                        opacity: 1;
                                                                    }

                                                                    .form-check-input:disabled+.form-check-label {
                                                                        color: #000;
                                                                        opacity: 1;
                                                                    }
                                                                </style>
                                                            @elseif ($respuesta->pregunta->tipo_pregunta == 'Larga')
                                                                <textarea class="form-control mb-3" id="" cols="30" rows="4" readonly
                                                                    id="respuestaLarga{{ $respuesta->pregunta->id }}">
                                                    {{ $respuesta->respuesta_usuario }}
                                                </textarea>
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="alert @if ($respuesta->puntaje > 0) alert-success @else alert-danger @endif mt-3">
                                                            <span>Puntaje
                                                                {{ $respuesta->puntaje }} /
                                                                {{ $respuesta->pregunta->puntaje }}

                                                                @if ($respuesta->puntaje > 0)
                                                                    Respuesta correcta <i
                                                                        class="bi bi-check-circle-fill"></i>
                                                                @else
                                                                    Respuesta Incorrecta <i class="bi bi-x-circle-fill"></i>
                                                                @endif
                                                                <br>
                                                                @if ($respuesta->pregunta->tipo_pregunta == 'Larga')
                                                                    <strong>Correccion:</strong>
                                                                    {{ $respuesta->correccion_usuario }}
                                                                    <br>
                                                                    <a class="btn btn-primary" data-bs-toggle="collapse"
                                                                        href="#respuesta{{ $respuesta->id }}"
                                                                        role="button" aria-expanded="false"
                                                                        aria-controls="respuesta{{ $respuesta->id }}">
                                                                        <i class="bi bi-file-earmark-medical-fill"></i>
                                                                        Revision
                                                                    </a>


                                                                    <div class="collapse"
                                                                        id="respuesta{{ $respuesta->id }}">
                                                                        <form method="post"
                                                                            action="{{ route('guardar_correccion', ['respuestaId' => $respuesta->id]) }}">
                                                                            @csrf

                                                                            <label for="puntaje">Puntaje</label>
                                                                            <input type="number" name="puntaje"
                                                                                class="form-control" min="0"
                                                                                max="{{ $respuesta->pregunta->puntaje }}"
                                                                                id="puntaje"
                                                                                value="{{ $respuesta->puntaje }}">
                                                                            <br>
                                                                            <label
                                                                                for="correccionTextarea">Corrección:</label>
                                                                            <textarea class="form-control" name="correccion" id="correccionTextarea" cols="30" rows="7">{{ $respuesta->correccion_usuario ?? '' }}</textarea>

                                                                            <button type="submit"
                                                                                class="btn btn-crear mt-3">Guardar
                                                                                Corrección
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                @else
                                                                    <strong>Respuesta correcta:</strong>

                                                                    @if ($respuesta->pregunta->tipo_pregunta == 'Seleccion multiple')
                                                                        @foreach ($respuesta->pregunta->opciones as $opcion)
                                                                            @if ($opcion->esCorrecta)
                                                                                {{ $opcion->textoOpcion }},
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        {{ $respuesta->pregunta->respuestaCorrecta }}
                                                                    @endif
                                                                @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal de eliminación -->                        
                            <div class="modal fade" id="eliminarModal{{ $formularioId }}_{{ $usuario->id }}"
                                tabindex="-1"
                                aria-labelledby="eliminarModalLabel{{ $formularioId }}_{{ $usuario->id }}"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <form
                                                action="{{ route('eliminar.respuestas', ['formularioId' => $formularioId, 'usuarioId' => $usuario->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method ('DELETE')
                                                <div class="d-flex align-items-center justify-content-center">
                                                    <i class="bi bi-exclamation-triangle-fill text-danger me-2"
                                                        style="font-size: 1.5em;"></i>
                                                    <strong class="fs-4">¡Advertencia!</strong>
                                                </div>
                                                <p class="mb-2">Al eliminar las respuestas de {{ $usuario->nombre }} {{ $usuario->primer_apellido }} para el formulario "{{ $formulario->titulo }}", se le brindará la oportunidad de volver a responder este formulario.</p>
                                                <p class="mb-3 text-center">
                                                    <strong class="text-center">¿Confirma que desea eliminar las respuestas de {{ $usuario->nombre }} {{ $usuario->primer_apellido }} para este formulario?</strong>
                                                </p>                                                
                                                <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                                    <button type="button" class="btn btn-cancelar me-2"
                                                        data-bs-dismiss="modal">Cancelar</button>
                                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>


@endsection
