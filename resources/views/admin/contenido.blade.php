@extends('admin.home')

<head>
    <link rel="stylesheet" href="{{ asset('css/admin/contenido.css') }}">
</head>
@section('secciones')
    @auth
        @role('admin')
            <h1 class="titulo">Contenidos</h1>


            <button type="button" class="btn btn-nuevo" data-bs-toggle="modal" data-bs-target="#subirContenido">
                <i class="bi bi-plus-circle"></i> Agregar Contenido
            </button>

            <form action="{{ route('contenido.subir-archivo') }}" method="POST">
                <div class="modal fade" id="subirContenido" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                    aria-labelledby="subirContenidoLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="subirContenidoLabel">Nuevo Contenido</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                @csrf

                                <div class="form-floating mb-3">
                                    <input type="text" placeholder="Título del contenido" class="form-control" id="titulo"
                                        name="titulo" required>
                                    <label for="titulo">Título del contenido</label>

                                    <div class="invalid-feedback" id="titulo-error">
                                        Este campo es obligatorio y debe tener al menos 5 caracteres.
                                    </div>
                                </div>

                                {{-- <div class="form-floating mb-3">
                                    <input type="text" placeholder="Descripción" class="form-control" id="descripcion"
                                        name="descripcion">
                                    <label for="descripcion">Descripción</label>
                                    <div class="invalid-feedback" id="descripcion-error">
                                        Este campo es obligatorio y debe tener al menos 5 caracteres.
                                    </div>
                                </div> --}}

                                <div class="form-floating mb-3">
                                    <input type="url" placeholder="URL del archivo" class="form-control" id="url_archivo"
                                        name="url_archivo" required>
                                    <label for="url_archivo">URL del archivo</label>
                                    <div class="invalid-feedback" id="url_archivo-error">
                                        Este campo es obligatorio y debe ser una URL válida.
                                    </div>
                                </div>

                                <div class="col-md">
                                    <div class="form-floating mb-3">
                                        <select class="form-select" id="tipo_contenido" name="tipo_contenido" required>
                                            <option selected>Seleccione el tipo de contenido</option>
                                            <option value="pdf">PDF</option>
                                            <option value="video">Video</option>
                                            <option value="audio">Audio</option>
                                            <option value="imagen">Imagen</option>
                                        </select>
                                        <label for="tipo_contenido">Tipo de contenido</label>
                                    </div>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-crear" id="submit-button">Subir Archivo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @foreach ($contenidos as $contenido)
                <div>
                    <div class="modal fade" id="mostrar{{ $contenido->id }}" tabindex="-1" aria-labelledby="mostrarLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="mostrarLabel">{{ $contenido->titulo }}
                                    </h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">                                   

                                    @if ($contenido->tipo_contenido === 'audio')
                                        <audio src="{{ $contenido->url_archivo }}" controls controlsList="nodownload"
                                            style="width: 100%"></audio>
                                    @elseif ($contenido->tipo_contenido === 'imagen')
                                        <img src="{{ $contenido->url_archivo }}" alt="{{ $contenido->titulo }}" style="width:100%">
                                    @elseif ($contenido->tipo_contenido === 'video')
                                        <video width="100%" src="{{ $contenido->url_archivo }}" controls
                                            controlsList="nodownload"></video>
                                    @elseif ($contenido->tipo_contenido === 'pdf')
                                        <iframe src="{{ $contenido->url_archivo }}" style="width:100%; height:100vh;"
                                            frameborder="0"></iframe>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal para eliminar -->
                    <form action="{{ route('contenidos.destroy', $contenido) }}" method="POST">
                        <div class="modal fade" id="eliminar{{ $contenido->id }}" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
                            <div class="modal-dialog modal-dialog-centered ">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        @csrf
                                        @method('DELETE')
                                        ¿Estás seguro de que deseas eliminar este contenido?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-eliminar">
                                            <i class="bi bi-trash"></i> Eliminar
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <form action="{{ route('contenidos.update', $contenido) }}" method="POST">
                        <div class="modal fade" id="staticBackdrop{{ $contenido->id }}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="staticBackdropLabel">Actualizar contenido</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-floating mb-3">
                                            <input type="text" placeholder="Título del contenido" class="form-control"
                                                id="floatingInput" name="titulo" autocomplete="off"
                                                value="{{ $contenido->titulo }}" required>
                                            <label for="floatingInput">Título del contenido</label>
                                        </div>

                                        {{-- <div class="form-floating mb-3">
                                            <input type="text" placeholder="Descripción" class="form-control"
                                                id="descripcion" name="descripcion" value="{{ $contenido->descripcion }}">
                                            <label for="descripcion">Descripción</label>
                                        </div> --}}

                                        <div class="form-floating mb-3">
                                            <input type="url" placeholder="URL del contenido" class="form-control"
                                                id="url_archivo" name="url_archivo" autocomplete="off"
                                                value="{{ $contenido->url_archivo }}" required>
                                            <label for="url_archivo">URL del contenido </label>
                                        </div>

                                        <div class="col-md">
                                            <div class="form-floating mb-3">
                                                <select class="form-select" id="tipoContenido" name="tipo_contenido" required>
                                                    <option value="pdf" @if ($contenido->tipo_contenido === 'pdf') selected @endif>
                                                        PDF
                                                    </option>
                                                    <option value="video" @if ($contenido->tipo_contenido === 'video') selected @endif>
                                                        Video
                                                    </option>
                                                    <option value="audio" @if ($contenido->tipo_contenido === 'audio') selected @endif>
                                                        Audio
                                                    </option>
                                                    <option value="imagen" @if ($contenido->tipo_contenido === 'imagen') selected @endif>
                                                        Imagen
                                                    </option>
                                                </select>
                                                <label for="tipoContenido">Tipo de contenido</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-cancelar" data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-actualizar">Guardar cambios</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            @endforeach
         
            <div>
                @livewire('contenidos-table')
            </div>

            @livewireScripts


            
      
            @include('admin.visor')
        @endrole
    @endauth
@endsection
