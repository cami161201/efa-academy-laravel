<!DOCTYPE html>

<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;800&family=Roboto:wght@100;300;400;500;700;900&display=swap"
        rel="stylesheet">
    {{-- bootstrap --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>

    {{-- Iconos de Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="stylesheet" href="{{ asset('css/admin/home.css') }}">

    <title>EFA academy</title>
</head>
<header>
    <nav class="navbar navbar-expand-lg">
       
        <div class="container-fluid">
            <a class="navbar-brand" href="#">EFA Academy</a>
            <button class="navbar-toggler" style="background-color: white;" type="button" data-bs-toggle="collapse" data-bs-target="#navbar"
                aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation" >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/lista-estudiantes"><i
                                class="bi bi-people-fill"></i> Usuarios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/modulo"><i
                                class="bi bi-journal-bookmark-fill"></i> Modulos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/contenidos"><i class="bi bi-folder"></i>
                            Contenidos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/formularios"> <i
                                class="bi bi-ui-checks"></i> Formularios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/respuesta"><i
                                class="bi bi-card-checklist"></i> Respuestas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/notas-estudiantes">
                            <i class="bi bi-bar-chart"></i> Notas</a>
                    </li>
                </ul>
                <a href="/logout" type="button" id="logout">
                <i class="bi bi-box-arrow-right"></i>
                </a>
            </div>
        </div>
    </nav>
</header>
<body>

    
 
    <div class="home_content">
        @yield('secciones')
    </div>
  


</body>

</html>
