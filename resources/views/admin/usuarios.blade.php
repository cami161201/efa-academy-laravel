@extends('admin.home')

<head>
    <link rel="stylesheet" href="{{ asset('css/admin/usuario.css') }}">
</head>

@section('secciones')
    @auth
        @role('admin')
            <h1 class="titulo">Usuarios</h1>

            <button type="button" class="btn btn-nuevo" data-bs-toggle="modal" data-bs-target="#nuevoUsuario">
                <i class="bi bi-plus-circle"></i>Agregar usuario
            </button>

            <form action="{{ route('registro.registrar') }}" method="POST">
                <div class="modal fade" id="nuevoUsuario" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                    aria-labelledby="nuevoUsuarioLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="nuevoUsuarioLabel">Nuevo usuario</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                @csrf
                                {{-- @include('mensaje') --}}
                                <div class="row">
                                    <div class="col-lg-6 datos">
                                        <div class="form-floating mt-3">
                                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" autocomplete="off" maxlength="35">
                                            <label for="nombre">Nombre <span style="color: red">*</span></label>
                                            <div class="invalid-feedback" id="nombre-error"></div>
                                        </div>
                                                   
                                        <div class="form-floating mt-3">
                                            <input type="text" class="form-control" id="primer_apellido" name="primer_apellido"
                                                placeholder="Primer Apellido" autocomplete="off" maxlength="35">
                                            <label for="primer_apellido">Primer Apellido <span style="color: red">*</span></label>
                                            <div class="invalid-feedback" id="primer_apellido-error"></div>
                                        </div>

                                        <div class="form-floating mt-3">
                                            <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido"
                                                placeholder="Segundo Apellido" autocomplete="off" maxlength="35">
                                            <label for="segundo_apellido">Segundo Apellido</label>
                                            <div class="invalid-feedback" id="segundo_apellido-error"></div>
                                        </div>

                                        <div class="form-floating mt-3">
                                            <input type="text" class="form-control" id="academia" name="academia"
                                                placeholder="Academia" autocomplete="off" maxlength="30">
                                            <label for="academia">Academia <span style="color: red">*</span></label>
                                            <div class="invalid-feedback" id="academia-error"></div>
                                        </div>

                                        <div class="form-floating mt-3">
                                            <input type="text" class="form-control" id="ocupacion" name="ocupacion"
                                                placeholder="Ocupación" autocomplete="off" maxlength="35">
                                            <label for="ocupacion">Ocupación <span style="color: red">*</span></label>
                                            <div class="invalid-feedback" id="ocupacion-error"></div>
                                        </div>

                                        <div class="form-floating mt-3">
                                            <select class="form-select" aria-label="Default select example" id="tipo_curso"
                                                name="tipo_curso">
                                                <option selected>Seleccione el tipo de curso</option>
                                                <option value="Online">Online</option>
                                                <option value="Regular">Regular</option>
                                                <option value="Intensivo">Intensivo</option>
                                                <option value="Grupal">Grupal</option>
                                            </select>
                                            <label for="tipo_curso">Tipo de curso</label>
                                            <div class="invalid-feedback" id="tipo_curso-error"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 datos">
                                        <div class="form-floating mt-3">
                                            <input type="number" class="form-control" id="celular" name="celular"
                                                placeholder="N° Celular" autocomplete="off" maxlength="35">
                                            <label for="celular">N° Celular <span style="color: red">*</span> </label>
                                            <div class="invalid-feedback" id="celular-error"></div>
                                        </div>

                                        <div class="group mt-3">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="password" name="password"
                                                    placeholder="Carnet de identidad" autocomplete="off"
                                                    aria-describedby="toggle-password" maxlength="25">
                                                <label for="password">Carnet de identidad <span
                                                        style="color: red">*</span></label>

                                            </div>
                                            {{-- <button class="btn" type="button" id="toggle-password">
                                                <i class="bi bi-eye-slash"></i>
                                            </button> --}}
                                        </div>
                                        <div class="invalid-feedback" id="password-error"></div>

                                        <div class="form-floating mt-3">
                                            <input type="date" class="form-control" id="fecha_nacimiento"
                                                name="fecha_nacimiento" placeholder="Fecha de Nacimiento" autocomplete="off">
                                            <label for="fecha_nacimiento">Fecha de Nacimiento <span
                                                    style="color: red">*</span></label>
                                            <div class="invalid-feedback" id="fecha_nacimiento-error"></div>
                                        </div>
                                        <div class="form-floating mt-3">
                                            <input type="email" class="form-control" id="correo" name="correo"
                                                placeholder="Correo electrónico" autocomplete="off" maxlength="50">
                                            <label for="correo">Correo electrónico<span style="color: red">*</span></label>

                                            <div class="invalid-feedback" id="correo-error"></div>
                                        </div>
                                        <div class="form-floating mt-3">
                                            <select class="form-select" id="funcion" name="funcion">
                                                <option value="estudiante" selected>Estudiante</option>
                                                <option value="admin">Administrador</option>
                                            </select>
                                            <label for="funcion">Seleccione un rol</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <h1 class="fs-5">Fechas de acceso</h1>
                                    <div class="col-lg-6">
                                        <div class="form-floating mt-3">
                                            <input type="date" class="form-control" id="start_access" name="start_access">
                                            <label for="start_access">Fecha de inicio (opcional)</label>
                                            <div class="invalid-feedback" id="start_access-error"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-floating mt-3">
                                            <input type="date" class="form-control" id="end_access" name="end_access">
                                            <label for="end_access">Fecha de Fin (opcional)</label>
                                            <div class="invalid-feedback" id="end_access-error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                    <button type="button" class="btn btn-cancelar me-2"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-crear">Añadir nuevo usuario</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
      
    
            <div class="container-fluid">
                <div>
                    @livewire('usuarios-table')
                </div>
                @livewireScripts
            </div>
         
            @include('admin.visor')
        @endrole
    @endauth
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const nombreInput = document.getElementById("nombre");
            const nombreError = document.getElementById("nombre-error");
    
            const primerApellidoInput = document.getElementById("primer_apellido");
            const primerApellidoError = document.getElementById("primer_apellido-error");
    
            const segundoApellidoInput = document.getElementById("segundo_apellido");
            const segundoApellidoError = document.getElementById("segundo_apellido-error");
    
            const academiaInput = document.getElementById("academia");
            const academiaError = document.getElementById("academia-error");
    
            const ocupacionInput = document.getElementById("ocupacion");
            const ocupacionError = document.getElementById("ocupacion-error");
    
            const celularInput = document.getElementById("celular");
            const celularError = document.getElementById("celular-error");
    
            const correoInput = document.getElementById("correo");
            const correoError = document.getElementById("correo-error");
    
            const fechaNacimientoInput = document.getElementById("fecha_nacimiento");
            const fechaNacimientoError = document.getElementById("fecha_nacimiento-error");
    
            const tipoCursoInput = document.getElementById("tipo_curso");
            const tipoCursoError = document.getElementById("tipo_curso-error");
    
            const submitButton = document.querySelector("button[type='submit']");
    
            const campos = [nombreInput, primerApellidoInput, segundoApellidoInput, academiaInput, ocupacionInput,
                celularInput, correoInput, fechaNacimientoInput, tipoCursoInput
            ];
    
            campos.forEach(function(campo) {
                campo.addEventListener('input', function() {
                    validarCampo(campo);
                });
            });
    
            submitButton.disabled = true;
    
            function validarCampo(input) {
                const inputError = document.getElementById(input.id + '-error');
    
                if (input.value.trim() === '' || (input === tipoCursoInput && input.value === "Seleccione el tipo de curso")) {
                    input.classList.add('is-invalid');
                    inputError.style.display = 'block';
                    inputError.innerHTML = 'Este campo es obligatorio.';
                } else if (input === nombreInput || input === primerApellidoInput || input === academiaInput ||
                    input === ocupacionInput) {
    
                    const patron = /^[a-zA-ZñÑáÁéÉíÍóÓúÚ\s]+$/;
                    if (!patron.test(input.value)) {
                        input.classList.add('is-invalid');
                        inputError.style.display = 'block';
                        inputError.innerHTML = 'Este campo solo debe contener letras';
                    } else {
                        input.classList.remove('is-invalid');
                        inputError.style.display = 'none';
                        inputError.innerHTML = '';
                    }
                } else if (input === segundoApellidoInput) {
                    const patron =
                        /^[a-zA-ZñÑáÁéÉíÍóÓúÚ\s]*$/; // Permitir letras y espacios, pero no es obligatorio
                    if (!patron.test(input.value)) {
                        input.classList.add('is-invalid');
                        inputError.style.display = 'block';
                        inputError.innerHTML = 'Este campo solo debe contener letras.';
                    } else {
                        input.classList.remove('is-invalid');
                        inputError.style.display = 'none';
                        inputError.innerHTML = '';
                    }
                } else if (input === celularInput) {
    
                    const patronNumeros = /^[0-9]+$/;
                    if (!patronNumeros.test(input.value)) {
                        input.classList.add('is-invalid');
                        inputError.style.display = 'block';
                        inputError.innerHTML = 'Este campo debe contener solo números';
                    } else {
                        input.classList.remove('is-invalid');
                        inputError.style.display = 'none';
                        inputError.innerHTML = '';
                    }
                } else if (input === correoInput) {
                    // Validación para el campo de correo
                    const patronCorreo = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                    if (!patronCorreo.test(input.value)) {
                        input.classList.add('is-invalid');
                        inputError.style.display = 'block';
                        inputError.innerHTML = 'Por favor, ingrese un correo electrónico válido.';
                    } else {
                        input.classList.remove('is-invalid');
                        inputError.style.display = 'none';
                        inputError.innerHTML = '';
                    }
                } else if (input === fechaNacimientoInput) {
                    if (input.value.trim() === '') {
                        input.classList.add('is-invalid');
                        fechaNacimientoError.style.display = 'block';
                        fechaNacimientoError.innerHTML = 'Este campo es obligatorio.';
                    } else {
                        input.classList.remove('is-invalid');
                        fechaNacimientoError.style.display = 'none';
                        fechaNacimientoError.innerHTML = '';
                    }
                } else if (input === tipoCursoInput) {
                    // Validación específica para tipo_curso
                    if (input.value === "Seleccione el tipo de curso") {
                        input.classList.add('is-invalid');
                        tipoCursoError.style.display = 'block';
                        tipoCursoError.innerHTML = 'Seleccione un tipo de curso válido.';
                    } else {
                        input.classList.remove('is-invalid');
                        tipoCursoError.style.display = 'none';
                        tipoCursoError.innerHTML = '';
                    }
                }
    
                // Deshabilitar o habilitar el botón de submit según si hay errores en algún campo
                submitButton.disabled = hayErrores();
            }
    
            // Función para comprobar si hay errores en algún campo
            function hayErrores() {
                return campos.some(function(campo) {
                    return campo.value.trim() === '' || campo.classList.contains('is-invalid');
                });
            }
        });
    </script>
    
    
@endsection
