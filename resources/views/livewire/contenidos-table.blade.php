<div>

    <div class="input-group mb-3 mt-3">
        <label for="campoBusqueda" class="visually-hidden">Buscar:</label>
        <span class="input-group-text" id="icono">
            <i class="bi bi-search"></i>
        </span>
        <input type="text" id="campoBusqueda" class="form-control rounded" wire:model="campoBusqueda" placeholder="Buscar por título" aria-label="Buscar" aria-describedby="icono">
    </div>
    
    <div class="table-responsive">
        <table class="table table-bordered" style="border:#04AAE8 2px solid;">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Tipo de contenido</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contenidos as $contenido)
                    <tr>
                        <td>{{ $contenido->titulo }}</td>
                        <td>{{ $contenido->tipo_contenido }}</td>
                        <td>
                            @if ($contenido->tipo_contenido === 'pdf')
                                <a href="{{ $contenido->url_archivo }}" class="btn btn-primary" target="_blank"><i
                                        class="bi bi-eye"></i> </a>
                            @else
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#mostrar{{ $contenido->id }}">
                                    <i class="bi bi-eye"></i>
                                </button>
                            @endif

                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#eliminar{{ $contenido->id }}">
                                <i class="bi bi-trash"></i>
                            </button>
                            <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                data-bs-target="#staticBackdrop{{ $contenido->id }}">
                                <i class="bi bi-pencil-square"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>
