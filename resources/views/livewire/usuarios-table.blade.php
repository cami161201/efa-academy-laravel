<div>
    <div class="input-group mb-3 mt-3">
        <span class="input-group-text" id="icono">
            <i class="bi bi-search"></i>
        </span>
        <input type="text" class="form-control" wire:model="campoBusqueda" name="campoBusqueda" placeholder="Titulo"
            aria-label="Usuario" aria-describedby="icono">
    </div>

    <div class="table-responsive">
        <table class="table align-middle table-bordered table-hover" style="border:#04AAE8 2px solid;">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Fecha de registro</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Curso</th>
                    <th scope="col">Datos</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td> {{ $usuario->nombre }} {{ $usuario->primer_apellido }}</td>
                        <td> {{ \Carbon\Carbon::parse($usuario->created_at)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                        </td>
                        <td>{{ $usuario->funcion }}</td>
                        <td>{{ $usuario->tipo_curso }}</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#verDatos{{ $usuario->id }}" title="Ver datos de usuario">
                                <i class="bi bi-eye"></i> Revisar
                            </button>

                            <div class="modal fade datosUsuario" id="verDatos{{ $usuario->id }}"
                                data-bs-keyboard="false" tabindex="-1" aria-labelledby="verDatosLabel"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-body" id="datosUsuario">

                                            <div class="row titulo">
                                                <div class="col">
                                                    <h5 class="subtitulo">Datos de usuario</h5>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="dropdown">
                                                        <a class="btn btn-actualizar" href="#" type="button"
                                                            data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="bi bi-list"></i>
                                                        </a>

                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <button type="button" class="dropdown-item"
                                                                    data-bs-toggle="modal"
                                                                    data-bs-target="#editarDatos{{ $usuario->id }}">
                                                                    <i class="bi bi-pencil-square text-primary"></i>
                                                                    Modificar usuario
                                                                </button>

                                                            </li>

                                                            @if ($usuario->modulos->count() == 0)
                                                                <li>
                                                                    <button type="button" class="dropdown-item"
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#modulos{{ $usuario->id }}"
                                                                        data-accion="asignar">
                                                                        <i class="bi bi-bookmark-plus text-success"></i>
                                                                        Asignar modulo
                                                                    </button>
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <button type="button" class="dropdown-item"
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#modulos{{ $usuario->id }}"
                                                                        data-accion="editar">
                                                                        <i class="bi bi-pencil text-warning"></i>
                                                                        Cambiar
                                                                        asignación
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            <li>
                                                                <button type="button" class="dropdown-item"
                                                                    data-bs-toggle="modal"
                                                                    data-bs-target="#eliminarUsuario{{ $usuario->id }}">
                                                                    <i class="bi bi-trash text-danger""></i>
                                                                    Eliminar usuario
                                                                </button>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>


                                            <p><strong><i class="bi bi-person-circle"></i> Nombre:</strong>
                                                {{ $usuario->nombre }}</p>
                                            <p><strong><i class="bi bi-person-vcard-fill"></i> Primer
                                                    apellido:</strong>
                                                {{ $usuario->primer_apellido }}</p>
                                            <p><strong><i class="bi bi-person-vcard-fill"></i> Segundo
                                                    apellido:</strong>
                                                {{ $usuario->segundo_apellido }}</p>
                                            <p><strong><i class="bi bi-building"></i> Academia:</strong>
                                                {{ $usuario->academia }}</p>
                                            <p><strong><i class="bi bi-briefcase-fill"></i> Ocupación:</strong>
                                                {{ $usuario->ocupacion }}</p>
                                            <p><strong><i class="bi bi-telephone-fill"></i> Celular:</strong>
                                                {{ $usuario->celular }}</p>
                                            <p><strong><i class="bi bi-envelope-fill"></i> Correo:</strong>
                                                {{ $usuario->correo }}</p>
                                            <p><strong><i class="bi bi-calendar-fill"></i> Fecha de
                                                    nacimiento:</strong>
                                                {{ \Carbon\Carbon::parse($usuario->fecha_nacimiento)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                                            </p>
                                            <p><strong><i class="bi bi-journal-bookmark-fill"></i> Tipo de
                                                    curso:</strong>{{ $usuario->tipo_curso }}</p>
                                            <p><strong><i class="bi bi-file-text-fill"></i>
                                                    Fecha de registro:</strong>
                                                {{ \Carbon\Carbon::parse($usuario->created_at)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                                            </p>
                                            <h5 class="subtitulo">Fechas de acceso</h5>
                                            @if ($usuario->start_access && $usuario->end_access)
                                                <p><strong><i class="bi bi-calendar-check"></i> Fecha de
                                                        inicio:</strong>
                                                    {{ \Carbon\Carbon::parse($usuario->start_access)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                                                </p>

                                                <p><strong><i class="bi bi-calendar-x"></i> Fecha de
                                                        fin:</strong>
                                                    {{ \Carbon\Carbon::parse($usuario->end_access)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                                                </p>
                                            @else
                                                <p>No se ha designado fechas de acceso a este usuario</p>
                                            @endif
                                            <h5 class="subtitulo">Modulos asignados</h5>

                                            @if ($usuario->modulos->count() > 0)
                                                @foreach ($usuario->modulos as $modulo)
                                                    <p><i class="bi bi-airplane-fill"></i>
                                                        {{ $modulo->titulo }}</p>
                                                @endforeach
                                            @else
                                                <p>Pendiente</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <form action="{{ route('asignar.editar.modulos', ['userId' => $usuario->id]) }}" method="POST">
                        <div class="modal fade" id="modulos{{ $usuario->id }}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="modulos{{ $usuario->id }}Label"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="d-flex align-items-center justify-content-center">
                                            @if (isset($usuario->modulos) && $usuario->modulos->isNotEmpty())
                                                <strong class="fs-4"> Editar módulos asignados</strong>
                                            @else
                                                <strong class="fs-4">Asignar módulos</strong>
                                            @endif
                                        </div>
                                        @csrf
                                        <input type="hidden" name="accion"
                                            value="{{ isset($usuario->modulos) && $usuario->modulos->isNotEmpty() ? 'editar' : 'asignar' }}">
                                        <label for="usuario{{ $usuario->id }}">Usuario</label>
                                        <input type="text" class="form-control mt-2"
                                            id="usuario{{ $usuario->id }}"
                                            value="{{ $usuario->nombre }} {{ $usuario->primer_apellido }}" disabled>

                                        <strong class="mt-3">Módulos:</strong>
                                        @foreach ($modulos as $modulo)
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="modulo_id[]"
                                                    value="{{ $modulo->id }}"
                                                    {{ isset($usuario->modulos) && $usuario->modulos->contains($modulo) ? 'checked' : '' }}
                                                    id="modulo_{{ $modulo->id }}_{{ $usuario->id }}">
                                                <label class="form-check-label"
                                                    for="modulo_{{ $modulo->id }}_{{ $usuario->id }}">{{ $modulo->titulo }}</label>
                                            </div>
                                        @endforeach

                                        <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                            <button type="button" class="btn btn-secondary me-2"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('usuario.actualizar', ['id' => $usuario->id]) }}" method="POST">
                        <div class="modal fade" id="editarDatos{{ $usuario->id }}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="editarDatosLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="editarDatosLabel">Actualizar datos</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">

                                        @method('PUT')
                                        @csrf

                                        <div class="row">
                                            <div class="col-lg-6 datos">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="nombre{{ $usuario->id }}" name="nombre"
                                                        placeholder="Nombre" autocomplete="off"
                                                        value="{{ $usuario->nombre }}">
                                                    <label for="nombre">Nombre <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="nombre-error"></div>
                                                </div>

                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="primer_apellido{{ $usuario->id }}"
                                                        name="primer_apellido" placeholder="Primer Apellido"
                                                        autocomplete="off" value="{{ $usuario->primer_apellido }}">
                                                    <label for="primer_apellido">Primer Apellido <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="primer_apellido-error"></div>
                                                </div>

                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="segundo_apellido{{ $usuario->id }}"
                                                        name="segundo_apellido" placeholder="Segundo Apellido"
                                                        autocomplete="off" value="{{ $usuario->segundo_apellido }}">
                                                    <label for="segundo_apellido">Segundo Apellido</label>
                                                    <div class="invalid-feedback" id="segundo_apellido-error"></div>
                                                </div>

                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="academia{{ $usuario->id }}" name="academia"
                                                        placeholder="Academia" autocomplete="off"
                                                        value="{{ $usuario->academia }}">
                                                    <label for="academia">Academia <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="academia-error"></div>
                                                </div>

                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="ocupacion{{ $usuario->id }}" name="ocupacion"
                                                        placeholder="Ocupación" autocomplete="off"
                                                        value="{{ $usuario->ocupacion }}">
                                                    <label for="ocupacion">Ocupación <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="ocupacion-error"></div>
                                                </div>

                                                <div class="form-floating mt-3">
                                                    <select class="form-select" id="tipo_curso{{ $usuario->id }}"
                                                        name="tipo_curso">
                                                        <option selected>Seleccione el tipo de curso</option>
                                                        <option value="Online"
                                                            {{ $usuario->tipo_curso === 'Online' ? 'selected' : '' }}>
                                                            Online</option>
                                                        <option value="Regular"
                                                            {{ $usuario->tipo_curso === 'Regular' ? 'selected' : '' }}>
                                                            Regular</option>
                                                        <option value="Intensivo"
                                                            {{ $usuario->tipo_curso === 'Intensivo' ? 'selected' : '' }}>
                                                            Intensivo</option>
                                                        <option value="Grupal"
                                                            {{ $usuario->tipo_curso === 'Grupal' ? 'selected' : '' }}>
                                                            Grupal</option>
                                                    </select>
                                                    <label for="tipo_curso">Tipo de curso</label>
                                                </div>

                                            </div>

                                            <div class="col-lg-6 datos">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control"
                                                        id="celular{{ $usuario->id }}" name="celular"
                                                        placeholder="N° Celular" autocomplete="off"
                                                        value="{{ $usuario->celular }}">
                                                    <label for="celular">N° Celular <span style="color: red">*</span>
                                                    </label>
                                                    <div class="invalid-feedback" id="celular-error"></div>
                                                </div>

                                                <div class="form-floating mb-3">
                                                    <input type="email" class="form-control"
                                                        id="correo{{ $usuario->id }}" name="correo"
                                                        placeholder="Correo electrónico" autocomplete="off"
                                                        value="{{ $usuario->correo }}">
                                                    <label for="correo">Correo electrónico <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="correo-error"></div>
                                                </div>

                                                <div class="group mb-3">
                                                    <div class="form-floating">
                                                        <input type="password" class="form-control"
                                                            id="password{{ $usuario->id }}" name="password"
                                                            placeholder="Nueva Contraseña" autocomplete="off"
                                                            aria-describedby="toggle-password">
                                                        <label for="password">Nueva Contraseña</label>
                                                    </div>
                                                    <button class="btn" type="button"
                                                        id="toggle-password{{ $usuario->id }}">
                                                        <i class="bi bi-eye-slash"></i>
                                                    </button>
                                                </div>
                                                <div class="invalid-feedback" id="password-error"></div>

                                                <div class="form-floating mb-3">
                                                    <input type="date" class="form-control"
                                                        id="fecha_nacimiento{{ $usuario->id }}"
                                                        name="fecha_nacimiento" placeholder="Fecha de Nacimiento"
                                                        autocomplete="off" value="{{ $usuario->fecha_nacimiento }}">
                                                    <label for="fecha_nacimiento">Fecha de Nacimiento <span
                                                            style="color: red">*</span></label>
                                                    <div class="invalid-feedback" id="fecha_nacimiento-error">
                                                    </div>
                                                </div>
                                                <div class="form-floating mb-3">
                                                    <select class="form-select" id="funcion{{ $usuario->id }}"
                                                        name="funcion">
                                                        <option value="estudiante"
                                                            {{ $usuario->funcion === 'estudiante' ? 'selected' : '' }}>
                                                            Estudiante</option>
                                                        <option value="admin"
                                                            {{ $usuario->funcion === 'admin' ? 'selected' : '' }}>
                                                            Administrador</option>
                                                    </select>
                                                    <label for="funcion">Seleccione un rol</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <h1 class="fs-5">Fechas de acceso</h1>
                                            <div class="col-lg-6">
                                                <div class="form-floating mb-3">
                                                    <input type="date" class="form-control"
                                                        id="start_access{{ $usuario->id }}" name="start_access"
                                                        value="{{ $usuario->start_access }}">
                                                    <label for="start_access">Fecha de inicio (opcional)</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-floating mb-3">
                                                    <input type="date" class="form-control"
                                                        id="end_access{{ $usuario->id }}" name="end_access"
                                                        value="{{ $usuario->end_access }}">
                                                    <label for="end_access">Fecha de Fin (opcional)</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary enviar"
                                            data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="bi bi-send"></i>
                                            Actualizar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <form action="{{ route('eliminar.usuario', ['id' => $usuario->id]) }}" method="POST">
                        <div class="modal fade" id="eliminarUsuario{{ $usuario->id }}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="eliminarUsuarioLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        @method('DELETE')
                                        @csrf
                                        <div class="d-flex align-items-center justify-content-center">
                                            <i class="bi bi-exclamation-triangle-fill text-danger me-2"
                                                style="font-size: 1.5em;"></i>
                                            <strong class="fs-4">¡Advertencia!</strong>
                                        </div>
                                        <p class="mb-2">Al eliminar a este usuario resultará en la pérdida de las
                                            respuestas proporcionadas por él. Esta acción no se podrá deshacer.</p>

                                        <p class="mb-3 text-center">
                                            <strong class="text-center">¿Confirma que desea eliminar al usuario
                                                {{ $usuario->nombre }} {{ $usuario->primer_apellido }}?
                                            </strong>
                                        </p>

                                        <div class="container-fluid d-flex flex-row justify-content-end mt-3">
                                            <button type="button" class="btn btn-secondary me-2"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-danger">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
            </tbody>
        </table>


    </div>

</div>




<script>
    document.addEventListener("DOMContentLoaded", function() {
        @foreach ($usuarios as $usuario)
            const passwordInput{{ $usuario->id }} = document.getElementById("password{{ $usuario->id }}");
            const togglePasswordButton{{ $usuario->id }} = document.getElementById(
                "toggle-password{{ $usuario->id }}");
            const passwordError{{ $usuario->id }} = document.getElementById(
                'password-error{{ $usuario->id }}');

            togglePasswordButton{{ $usuario->id }}.addEventListener("click", function() {
                togglePasswordVisibility(passwordInput{{ $usuario->id }},
                    togglePasswordButton{{ $usuario->id }});
            });

            passwordInput{{ $usuario->id }}.addEventListener('input', function(e) {
                validarPassword(e, {{ $usuario->id }});
            });

            function togglePasswordVisibility(input, toggleButton) {
                if (input.type === "password") {
                    input.type = "text";
                    toggleButton.innerHTML = '<i class="bi bi-eye"></i>';
                } else {
                    input.type = "password";
                    toggleButton.innerHTML = '<i class="bi bi-eye-slash"></i>';
                }
            }
        @endforeach
    });
</script>
