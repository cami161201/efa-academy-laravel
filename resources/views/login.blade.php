@extends('inicio')

@section('content')

    <head>
        <link rel="stylesheet" href="css/login.css">
    </head>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7" id="informacion">
                <img src="images/logoEfa.png" alt="">
                {{-- <img src="images/police-3.svg" alt=""> --}}

            </div>
            <div class="col-lg-5" id="login">
                <h2 class="titulo">¡Bienvenido a tu Curso!</h2>
                <p>Una vez que te proporcionemos tus datos de acceso, podrás ingresar a nuestra
                    plataforma sin ningún problema.</p>

                <form action="{{ route('login.ingresar') }}" method="POST">
                    @csrf
                    @include('mensaje')

                    <div class="caja">
                        <label for="correo">Correo electronico</label>
                        <input class="form-control" type="text" autocomplete="off" name="correo">
                    </div>


                    <div class="caja">
                        <label class="form-label">Contraseña</label>
                        <div class="input-contra">
                            <input type="password" id="password" class="form-control"
                               autocomplete="off" aria-describedby="toggle-password" name="password">
                            <button class="btn" type="button" id="toggle-password">
                                <i class="bi bi-eye-slash"></i>
                            </button>
                        </div>
                    </div>

                    <div class="caja">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date" class="form-control" name="fecha_nacimiento">
                    </div>


                    <button type="submit" class="btn btn-login">
                        <i class="bi bi-box-arrow-in-right"></i>
                        Ingresar
                    </button>
                </form>


            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const passwordInput = document.getElementById("password");
            const togglePasswordButton = document.getElementById("toggle-password");

            togglePasswordButton.addEventListener("click", function() {
                togglePasswordVisibility(passwordInput, togglePasswordButton);
            });

            function togglePasswordVisibility(input, toggleButton) {
                if (input.type === "password") {
                    input.type = "text";
                    toggleButton.innerHTML = '<i class="bi bi-eye"></i>';
                } else {
                    input.type = "password";
                    toggleButton.innerHTML = '<i class="bi bi-eye-slash"></i>';
                }
            }
        });
    </script>
@endsection
