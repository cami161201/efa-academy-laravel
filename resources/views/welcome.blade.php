@extends('inicio')

@section('content')

    <head>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const parallaxContainer = document.getElementById('parallax-container');

            window.addEventListener('scroll', function() {
                let scrollPosition = window.scrollY;
                parallaxContainer.style.backgroundPositionY =
                    `${scrollPosition * 0.5}px`; // Ajusta el valor según la velocidad de parallax deseada
            });
        });
    </script>


    <div class="container-fluid principal" id="parallax-container">
        <nav class="navbar">
            <div class="container-fluid">
                <a class="btn" href="#contacto"><i class="bi bi-chat-dots-fill"></i> Contactanos</a>
                <a class="btn" aria-current="page" href="/login"><i class="bi bi-person-fill"></i> Inicia
                    Sesion</a>
            </div>
        </nav>
        <div class="row">
            <div class="col-12 text-principal">

                <h1 class="text">Aprende Inglés<br>Aeronáutico</h1>
                <h3>¡Únete a <i>EFA Academy</i>!</h3>
                <p class="text mb-0" style="font-size: 20px">Descubre cómo podemos ayudarte a alcanzar tus objetivos
                    lingüísticos y profesionales.</p>
                <a type="button" class="btn-inicio text" href="#paquetes">Inscribete aqui</a>
            </div>
        </div>

        <div class="row justify-content-center align-items-center mt-3" id="redes_sociales">
            <a class="btn" type="button" href="https://www.facebook.com/EFAACADEMY1" target="_blank">
                <i class="bi bi-facebook"></i>
            </a>
            <a class="btn" type="button"
                href="https://wa.me/59167555735?text=Quisiera%20saber%20m%C3%A1s%20sobre%20EFA%20Academy" target="_blank">
                <i class="bi bi-whatsapp"></i>
            </a>
            <a class="btn" type="button" href="https://instagram.com/englishforaviationacademy?igshid=MzRlODBiNWFlZA=="
                target="_blank">
                <i class="bi bi-instagram"></i>
            </a>
        </div>


    </div>
    <div class="container-fluid" id="bienvenida">

        <h1>¡Bienvenido a EFA Academy!</h1>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <p>El inglés se ha convertido en el idioma universal de comunicación entre pilotos, tripulación de
                    cabina y controladores de tráfico aéreo. Esto se debe a la necesidad de garantizar la seguridad y la
                    eficiencia en los vuelos
                    <br><br>
                    Como academia especializada en inglés para aviación, estamos comprometidos en ayudarte a alcanzar un
                    nivel óptimo de competencia en inglés, cumpliendo con los estándares establecidos por la
                    Organización de Aviación Civil Internacional (OACI).
                    <br><br>
                    Nuestros cursos están diseñados específicamente para proporcionarte las habilidades lingüísticas
                    necesarias para tener éxito en la aviación. Únete a nosotros y comienza a comunicarte con confianza
                    en inglés en este apasionante campo.
                </p>
            </div>
            <div class="col-lg-6 col-md-12 img">
                <img src="/images/piloto.jpg" alt="piloto y azafata">
            </div>
        </div>
    </div>
    <div class="container-fluid" id="oferta">
        <h2>EN EFA ACADEMY OFRECEMOS</h2>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="text-oferta">
                    <i class="bi bi-person"></i>
                    <span>Clases personalizadas (uno a uno)</span>
                </div>
                <div class="text-oferta">
                    <i class="bi bi-people"></i>
                    <span>Clases grupales</span>
                </div>
                <div class="text-oferta">
                    <i class="bi bi-house"></i>
                    <span> Cursos desde la comodidad de tu casa</span>
                </div>
                <div class="text-oferta">
                    <i class="bi bi-journal-bookmark"></i>
                    <span>Material completamente avalado por la OACI</span>
                </div>
                <div class="text-oferta">
                    <i class="bi bi-book"></i>
                    <span> Simulacros de la prueba de competencia linguistica</span>
                </div>
                <div class="text-oferta">

                    <i class="bi bi-person-workspace"></i>
                    <span>Instructores expertos</span>
                </div>
            </div>
            {{-- <div class="col-lg-6 col-md-12 imag">
                <img src="/images/LogoEfa.png" alt="Efa academy logo">
            </div> --}}
        </div>
    </div>
    <div class="container-fluid" id="contenido">
        <div class="row">
            <div class="col-lg-12 int_temas">
                <h2 style="color: white">Contenido del curso</h2>
                <p>Cada unidad aborda procedimientos habituales y situaciones
                    excepcionales relevantes para pilotos
                    comerciales en aeropuertos de destino, como impacto de aves, fallas en motores y equipos, planificación
                    de combustible, entre otros. Estas unidades también involucran actividades de trabajo en pareja y grupo
                    para mejorar la comunicación segura y efectiva en la aviación.
                    A lo largo del curso, abordaremos una variedad de temas que les ayudarán a mejorar su dominio del idioma
                    inglés:</p>
            </div>
            <div class="col-lg-12">
                <div class="tarjeta_temas">
                    <fieldset>
                        <legend>Temas</legend>
                        <div class="row">

                            <div class="col-lg-4 temas">
                                <p><i class="bi bi-airplane-fill"></i>Sesiones informativas</p>
                                <p><i class="bi bi-airplane-fill"></i>Vuelta y Salida</p>
                                <p><i class="bi bi-airplane-fill"></i>Mal funcionamiento del equipo</p>
                                <p><i class="bi bi-airplane-fill"></i>Vida silvestre</p>
                                <p><i class="bi bi-airplane-fill"></i>Aterrizajes</p>
                                <p><i class="bi bi-airplane-fill"></i>Incursiones en pista</p>

                            </div>
                            <div class="col-lg-4 temas">
                                <p><i class="bi bi-airplane-fill"></i>Incidentes en ruta</p>
                                <p><i class="bi bi-airplane-fill"></i>Tiempo</p>
                                <p><i class="bi bi-airplane-fill"></i>Fallo del motor</p>
                                <p><i class="bi bi-airplane-fill"></i>Desembarques forzosos</p>
                                <p><i class="bi bi-airplane-fill"></i>Bustos nivelados</p>
                                <p><i class="bi bi-airplane-fill"></i>Mercancías peligrosas</p>

                            </div>
                            <div class="col-lg-4 temas">
                                <p><i class="bi bi-airplane-fill"></i>Seguridad</p>
                                <p><i class="bi bi-airplane-fill"></i>Accidentes geográficos</p>
                                <p><i class="bi bi-airplane-fill"></i>Riesgos ambientales</p>
                                <p><i class="bi bi-airplane-fill"></i>Espacio aéreo restringido</p>
                                <p><i class="bi bi-airplane-fill"></i>Infraestructura aeroportuaria</p>
                                <p><i class="bi bi-airplane-fill"></i>Colisiones durante el rodaje</p>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid" id="niveles">
        <h2>Niveles de Competencia Lingüística Operacional </h2>
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <p class="text-nivel">La Organización de Aviación Civil Internacional (OACI) ha establecido niveles de
                    competencia
                    lingüística para pilotos y tripulantes aéreos con el fin de asegurar una comunicación efectiva y
                    segura en el ámbito de la aviación.
                    <br><br>
                    El curso de inglés de aviación para pilotos comerciales de la OACI se basa en habilidades, con
                    actividades y simulacros diseñados para ayudar, en última instancia, a los pilotos a alcanzar el nivel
                    4+ (5, 6) de la OACI. El nivel 4, es el nivel mínimo aceptado por la OACI para pilotos y tripulantes
                    aéreos.
                    <br><br>
                    Nuestros cursos se ajustan a las recomendaciones del Documento 9835 de la OACI y a la Circular 323 de la
                    OACI, así como al marco desarrollado por la Aviation English Organisation.
                    <br><br>
                    A medida que los pilotos asistan a cada unidad del curso, nuestros profesores evaluarán su competencia
                    en los seis criterios de la OACI: pronunciación, estructura, vocabulario, fluidez, comprensión e
                    interacción.
                </p>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="lista">
                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">1</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>

                        <div>
                            <h4>Pre-Elemental</h4>
                            <p>Duración: 1 mes (2 módulos)</p>
                        </div>
                    </div>

                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">2</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>

                        <div>
                            <h4>Elemental</h4>
                            <p>Duración: 2 meses (3 módulos)</p>
                        </div>
                    </div>

                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">3</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>

                        <div>
                            <h4>Pre-Operacional</h4>
                            <p>Duración: 4 meses (4 módulos)</p>
                        </div>
                    </div>

                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">4</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>
                        <div>
                            <h4>Operacional</h4>
                            <p>Duración: 6 meses (6 módulos)</p>
                        </div>
                    </div>

                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">5</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>

                        <div>
                            <h4>Avanzado</h4>
                            <p>Duración: 2 meses (2 módulos)</p>
                        </div>
                    </div>

                    <div class="nivel">
                        <svg xmlns="http://www.w3.org/2000/svg" width="62" height="74" viewBox="0 0 62 74"
                            fill="none">
                            <g filter="url(#filter0_d_12_9)">
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" fill="#56CCF2" />
                                <path d="M31 3L56 18V48L31 63L6 48V18L31 3Z" stroke="white" stroke-width="4" />
                            </g>
                            <text x="31" y="55%" font-size="24" text-anchor="middle" fill="white">6</text>
                            <defs>
                                <filter id="filter0_d_12_9" x="0" y="0.66748" width="62" height="72.665"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                    <feOffset dy="4" />
                                    <feGaussianBlur stdDeviation="2" />
                                    <feComposite in2="hardAlpha" operator="out" />
                                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_12_9" />
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_12_9"
                                        result="shape" />
                                </filter>
                            </defs>
                        </svg>

                        <div>
                            <h4>Experto</h4>
                            <p>Duración: 2 meses (Conversacion)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="paquetes">
        <h2 class="mb-3">Paquetes de curso</h2>
        {{-- <p style="text-align: left">El programa de inglés de aviación para pilotos comerciales de EFA ACADEMY elabora
            estrategias de comunicación
            eficaces para las operaciones de vuelo, lo que, en última instancia, proporciona a los pilotos las
            habilidades lingüísticas necesarias para obtener un buen desempeño en un examen de inglés de la OACI.</p> --}}
        <div class="row">
            <div class="col-md-6 col-lg-3 mb-3">
                <div class="tarjeta h-100">
                    <div class="card-title">
                        <h5><i class="bi bi-globe"></i> Online</h5>
                    </div>
                    <div class="card-body">
                        <p class="precio"><span>$60</span>/mensual</p>
                        <p class="card-text"> Acceso a material específico adaptado a tu nivel de inglés aeronáutico.
                            <br>Diseñado para que puedas estudiar a tu propio ritmo y en tus propios horarios
                        </p>
                    </div>
                    <div class="card-footer text-center">
                        <a href="https://wa.link/32hpq0" type="button" class="btn-paquete">Adquiere tu paquete</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 mb-3">
                <div class="tarjeta h-100">
                    <div class="card-title">
                        <h5><i class="bi bi-tools"></i> Personalizado</h5>
                    </div>
                    <div class="card-body">
                        <p class="precio"><span>$200</span>/mensual</p>
                        <p class="card-text"> Asesoría por parte de un instructor especializado<br> Control de avance
                            detallado para evaluar tu progreso</p>
                    </div>
                    <div class="card-footer text-center">
                        <a href="https://wa.link/8n8q8r" type="button" class="btn-paquete">Adquiere tu paquete</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 mb-3">
                <div class="tarjeta h-100">
                    <div class="card-title">
                        <h5><i class="bi bi-people-fill"></i> Grupal</h5>
                    </div>
                    <div class="card-body">
                        <p class="precio"><span>$50</span>/mensual</p>
                        <p class="card-text">Potencia tu aprendizaje junto a tus compañeros con nuestro paquete grupal
                            diseñado
                            para la colaboración y el éxito compartido</p>
                    </div>
                    <div class="card-footer text-center">
                        <a href="https://wa.link/q66q0e" type="button" class="btn-paquete">Adquiere tu paquete</a>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-lg-3 mb-3">
                <div class="tarjeta h-100">
                    <div class="card-title">
                        <h5><i class="bi bi-lightning-charge-fill"></i> Intensivo</h5>
                    </div>
                    <div class="card-body">
                        <p class="precio"><span>$199.99</span>/mensual</p>
                        <p class="card-text"> Diseñado para aquellos que buscan avanzar rápidamente, este programa ofrece
                            clases personalizadas y un enfoque adaptativo para asegurar un aprendizaje óptimo</p>
                    </div>
                    <div class="card-footer text-center">
                        <a href="https://wa.link/hvyirz" type="button" class="btn-paquete">Adquiere tu paquete</a>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="d-flex row justify-content-center align-items-center">
            <a href="/pagoCurso" class="btn btn-inicio" type="button">Inscribete Aqui</a>
        </div> --}}
        <div class="row beneficios">
            <style>
                .beneficios .accordion-button {
                    color: #04AAE8;
                    font-weight: 500;
                    font-size: 18px;
                }

                .beneficios .accordion-button i {
                    color: #FBB641;
                    margin: 0px 5px;
                    font-size: 18px;
                }
            </style>
            <div class="col-md-6">
                <div class="accordion">
                    <div class="accordion-item 1">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <i class="bi bi-award"></i>
                                Ventajas del curso
                            </button>

                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li> <i class="bi bi-airplane-fill"></i> Formación profesional para la práctica de
                                        fraseología estándar en situaciones
                                        rutinarias.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Énfasis en funciones comunicativas para
                                        manejar situaciones rutinarias y no
                                        rutinarias.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Desarrollo de habilidades en los seis
                                        criterios lingüísticos de la OACI.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Temas interesantes relacionados con la
                                        seguridad del vuelo.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Horario flexible y entrega en el sitio para
                                        satisfacer necesidades.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Cientos de horas de material de práctica de
                                        escucha calificado para autoestudio.
                                    </li>
                                    <li> <i class="bi bi-airplane-fill"></i> Diseñado por expertos lingüísticos y expertos
                                        en aviación.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Ajustado al Marco de la OACI.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Habilidades y estrategias relevantes para la
                                        vida cotidiana en inglés general.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Clases privadas, grupales y en línea
                                        disponibles.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Curso intensivo y regular para estudiantes
                                        pilotos o controladores aéreos.</li>
                                    <li> <i class="bi bi-airplane-fill"></i> Ayuda a comprender las ventajas y limitaciones
                                        de los tipos de prueba de la OACI.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item 2">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="bi bi-chat-dots"></i>
                                Funciones lingüísticas
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionExample">

                            <div class="accordion-body">
                                <ul>
                                    <li><i class="bi bi-airplane-fill"></i> Aclaración y solicitud de información</li>
                                    <li><i class="bi bi-airplane-fill"></i> Corregir errores y malentendidos</li>
                                    <li><i class="bi bi-airplane-fill"></i> Descripción de fallas y daños</li>
                                    <li><i class="bi bi-airplane-fill"></i> Frases descriptivas complejas</li>
                                    <li><i class="bi bi-airplane-fill"></i> Descripción de situaciones hipotéticas</li>
                                    <li><i class="bi bi-airplane-fill"></i> Manejo de un cambio de plan</li>
                                    <li><i class="bi bi-airplane-fill"></i> Notificación de eventos</li>
                                    <li><i class="bi bi-airplane-fill"></i> Informar a los miembros de la tripulación</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="accordion">

                    <div class="accordion-item 3">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="bi bi-translate"></i>
                                Habilidades lingüísticas
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p style="text-align: left">En el curso de EFA Academy podrá desarrollar diversas
                                    habilidades lingüísticas
                                    fundamentales como:</p>
                                <ul>
                                    <li><i class="bi bi-airplane-fill"></i> Familiarización con el acento y neutralidad
                                    </li>
                                    <li><i class="bi bi-airplane-fill"></i> Phrasal verbs como se usa en un contexto de
                                        aviación</li>
                                    <li><i class="bi bi-airplane-fill"></i> Identificar jerga y expresiones idiomáticas
                                    </li>
                                    <li><i class="bi bi-airplane-fill"></i> Frecuencia del habla, contracciones y habla
                                        conectada</li>
                                    <li><i class="bi bi-airplane-fill"></i> Conciencia fonológica</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <style>
                        ul {
                            list-style: none;
                            text-align: left;
                        }

                        ul li {
                            font-size: 14px;
                        }

                        ul li i {
                            color: var(--color-secundario);
                        }

                        .accordion {
                            width: 90%;
                            margin: 2%;
                        }

                        .accordion .accordion-item {}
                    </style>
                    <div class="accordion-item 4">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="bi bi-spellcheck"></i>
                                Actividades auténticas
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                En cada módulo podrá encontrar actividades como:
                                <ul>
                                    <li><i class="bi bi-airplane-fill"></i> Descripción de la imagen.</li>
                                    <li><i class="bi bi-airplane-fill"></i> Práctica de pronunciación.</li>
                                    <li><i class="bi bi-airplane-fill"></i> Ejercicios gramaticales y estructuras
                                        relevantes</li>
                                    <li><i class="bi bi-airplane-fill"></i>
                                        Comprensión auditiva de inglés sencillo en un contexto de aviación, como información
                                        meteorológica, noticias, experiencias personales, entrevistas y discusiones
                                        grupales.
                                    </li>
                                    <li><i class="bi bi-airplane-fill"></i> Comprensión auditiva de diálogos
                                        piloto/controlador en situaciones rutinarias y no
                                        rutinarias.</li>
                                    <li><i class="bi bi-airplane-fill"></i> Juego de roles de comunicaciones
                                        piloto-controlador/piloto-tripulante con fraseología estándar de la OACI y inglés
                                        sencillo.</li>
                                    <li><i class="bi bi-airplane-fill"></i> Discusión grupal y debate sobre temas polémicos
                                        relacionados con la seguridad de los vuelos.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="contacto">
        <h2>¡Contáctanos!</h2>
        <p>¿Tienes dudas sobre nuestros cursos? ¿Quieres conocer más sobre nuestros profesores altamente calificados?
            Estamos aquí para ayudarte en cada paso de tu viaje hacia la excelencia lingüística y profesional.</p>

        <form action="{{ route('contacto.enviar') }}" method="POST">
            @csrf

            <div style="width: 70%;margin:0% 15%;">
                <label for="nombre" class="form-label">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre"
                    value="{{ isset($data['nombre']) ? $data['nombre'] : '' }}">

                <label for="correo" class="form-label">Email</label>
                <input type="email" class="form-control" id="correo" name="correo"
                    value="{{ isset($data['correo']) ? $data['correo'] : '' }}">

                <label for="mensaje" class="form-label">Mensaje</label>
                <textarea class="form-control" id="mensaje" name="mensaje">{{ isset($data['mensaje']) ? $data['mensaje'] : '' }}</textarea>
            </div>

            <div class="row boton-enviar">
                <div class="col-12 d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn enviar align-self-center">Enviar</button>
                </div>
            </div>

        </form>

        @if (Session::has('success'))
            <div class="mt-3">
                <p class="text-center" style="color: white"><strong>{{ Session::get('success') }}</strong></p>
            </div>
        @endif
    </div>



    <footer>
        <div class="container-fluid" id="piePagina">
            <div class="row">
                <div class="col-md-12 col-lg-6 text-center">
                    <img src="{{ asset('images/logo.png') }}" alt="" class="mx-auto img-fluid">
                    <p class="mb-0">¡Aprende inglés aeronáutico con nosotros y aprueba tu competencia lingüística!
                        <br>©2023 - EFA Academy
                    </p>

                </div>
                <div class="col-md-12 col-lg-6 text-center datos">
                    <p><b>Correo electrónico:</b> efaacademy2022@gmail.com</p>
                    <p><b>Teléfono:</b> +591 67555735</p>
                    <div class="iconos">
                        <a class="btn" href="https://www.facebook.com/EFAACADEMY1" role="button" target="_blank">
                            <i class="bi bi-facebook"></i>
                        </a>
                        <a class="btn"
                            href="https://wa.me/59167555735?text=Quisiera%20saber%20m%C3%A1s%20sobre%20EFA%20Academy"
                            role="button" target="_blank">
                            <i class="bi bi-whatsapp"></i>
                        </a>
                        <a class="btn" href="https://instagram.com/englishforaviationacademy?igshid=MzRlODBiNWFlZA=="
                            role="button" target="_blank">
                            <i class="bi bi-instagram"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </footer>



    <script>
        // Oculta el mensaje de éxito después de un minuto
        setTimeout(function() {
            var successMessage = document.getElementById('successMessage');
            if (successMessage) {
                successMessage.style.display = 'none';
            }
        }, 60000); // 60000 milisegundos = 1 minuto
    </script>
@endsection
