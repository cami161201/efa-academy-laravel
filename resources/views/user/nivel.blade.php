@extends('inicio')

<head>
    <link rel="stylesheet" href="{{ asset('css/user/nivel.css') }}">
</head>
@section('content')
    <div id="cabeza">
        <h1>Niveles</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2">
                <ul>
                    <li><a href="#pre-elemental">Pre elemental</a></li>
                    <li><a href="#elemental">Elemental</a></li>
                    <li><a href="#pre-operacional">Pre-operacional</a></li>
                    <li><a href="#operacional">Operacional</a></li>
                    <li><a href="#avanzado">Avanzado</a></li>
                    <li><a href="#experto">Experto</a></li>
                </ul>
            </div>

            <div class="col-10">
                <p>Estos niveles son esenciales para garantizar que nuestros estudiantes y profesionales de la aviación alcancen la competencia necesaria en inglés aeronáutico, un estándar esencial para la comunicación segura en la industria de la aviación a nivel mundial.
                </p>
                <div id="pre-elemental">
                    <h2 class="titulo-nivel">Pre-elemental</h2>
                    <div class="row nivel">
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-volume-up"></i> Pronunciación
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-diagram-2"></i> Structure
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-book"></i> Vocabulary
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-dots"></i> Fluidez
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-list-check"></i> Comprensión
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-left-quote"></i> Interacción
                            </h5>
                            <p>
                                Se desempeña en un nivel por debajo del nivel elemental.
                            </p>
                        </div>
                    </div>
                    <div class="row" id="contenidosPre-elemental">
                        <div class="titulo-contenido">
                            <span class="numero">1</span>
                            <span class="nombre">At The academy</span>
                        </div>
                       
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Verbo TO BE (+) (-) (?).</li>
                            <li><i class="bi bi-airplane-fill"></i> Presentaciones personales.</li>
                            <li><i class="bi bi-airplane-fill"></i> Saludos. </li>
                            <li><i class="bi bi-airplane-fill"></i> Presente simple. </li>
                            <li><i class="bi bi-airplane-fill"></i> Descripción de lugares, objetos y personas.</li>
                            <li><i class="bi bi-airplane-fill"></i> Futuro WILL.</li>
                            <li><i class="bi bi-airplane-fill"></i> Alfabeto estándar y fonético.</li>
                            <li><i class="bi bi-airplane-fill"></i> Uso del alfabeto fonético mensajes, códigos, deletreo.
                            </li>
                        </ul>
                       
                        
                    </div>
                </div>
                <div id="elemental">
                    <h2 class="titulo-nivel">Elemental</h2>
                    <div class="row nivel">
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-volume-up"></i> Pronunciación
                            </h5>
                            <p>
                                Pronunciación, estrés, ritmo y entonación están muy influenciados por la primera
                                lengua o regional variación y por lo general interfieren con facilidad de
                                comprensión.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-diagram-2"></i> Estructura
                            </h5>
                            <p>
                                Muestra solo un control limitado de unos pocos simple memorizado gramaticalmente
                                estructuras y patrones de oración.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-book"></i> Vocabulary
                            </h5>
                            <p>
                                Rango de vocabulario limitado
                                que consiste únicamente en
                                palabras y frases memorizadas.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-dots"></i> Fluidez
                            </h5>
                            <p>
                                Puede producir muy corto, aislado,
                                palabras memorizadas con
                                pausas frecuentes y una distracción
                                uso de rellenos para buscar
                                expresiones y a articular menos
                                palabras familiares.
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-list-check"></i> Comprensión
                            </h5>
                            <p>
                                La comprensión se limita a
                                frases aisladas, memorizadas
                                cuando son cuidadosamente y
                                articulado lentamente.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-left-quote"></i> Interacción
                            </h5>
                            <p>
                                El tiempo de respuesta es lento, y a menudo
                                inapropiado.
                                La interacción se limita a simple
                                intercambios rutinarios.
                            </p>
                        </div>



                    </div>
                    <div class="row" id="contenidosElemental">
                        <div class="titulo-contenido">
                            <span class="numero">3</span>
                            <span class="nombre">i am a pilot</span>
                        </div>
      
                        <ul>                     	
                            <li><i class="bi bi-airplane-fill"></i> Tiempo presente simple (+) (-) (?).</i>
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Forma “s” de los verbos.</li>
                            <li><i class="bi bi-airplane-fill"></i> Llegadas y salidas de vuelos.</li>
                            <li><i class="bi bi-airplane-fill"></i> Rutinas y hábitos del personal aeronáutico.</li>
                            <li><i class="bi bi-airplane-fill"></i> Verbo TO HAVE  (+) (-) (?).</li>
                            <li><i class="bi bi-airplane-fill"></i> Las partes de la casa.</li>
                            <li><i class="bi bi-airplane-fill"></i> Las partes del avión.</li>
                            <li><i class="bi bi-airplane-fill"></i> El clima.</li>
                            <li><i class="bi bi-airplane-fill"></i> ¿Cuál es el clima en ...?.</li>
                            <li><i class="bi bi-airplane-fill"></i> Tipo de nubes.</li>
                            <li><i class="bi bi-airplane-fill"></i> Las estaciones del año.</li>
                            <li><i class="bi bi-airplane-fill"></i> Presente progresivo (+) (-) (?).</li>
                            <li><i class="bi bi-airplane-fill"></i> Presente continuo versus presente simple (Usos y formas).
                            </li>

                        </ul>
                       
                        <div class="titulo-contenido">
                            <span class="numero">4</span>
                            <span class="nombre">At the airport</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i>Verbo CAN (+) (-) (?).</li>
                            <li><i class="bi bi-airplane-fill"></i>Características y funciones del avión y sus componentes.
                            </li>
                            <li><i class="bi bi-airplane-fill"></i>Tipos de aviones y naves.</li>
                            <li><i class="bi bi-airplane-fill"></i>Expresiones de cantidad.</li>
                            <li><i class="bi bi-airplane-fill"></i>There is/there are (hay (+) (-) (?)).</li>
                            <li><i class="bi bi-airplane-fill"></i>Descripción de aviones.</li>
                            <li><i class="bi bi-airplane-fill"></i>Tipos de refrigerios en cabina.</li>
                            <li><i class="bi bi-airplane-fill"></i>Tipos de empaques y cantidades.</li>
                            <li><i class="bi bi-airplane-fill"></i>HOW MUCH? HOW MANY?.</li>
                            <li><i class="bi bi-airplane-fill"></i>Sustantivos contables.</li>
                            <li><i class="bi bi-airplane-fill"></i>Sustantivos no contables.</li>
                            <li><i class="bi bi-airplane-fill"></i>Ofertas y petición (I’d like).</li>

                        </ul>
                        
                        <div class="titulo-contenido">
                            <span class="numero">5</span>
                            <span class="nombre">at the check in</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Pasado simple. (+) (-) (?).</li>
                            <li><i class="bi bi-airplane-fill"></i> Verbos regulares verbs/spelling rules.</li>
                            <li><i class="bi bi-airplane-fill"></i> Verbos irregulares.</li>
                            <li><i class="bi bi-airplane-fill"></i> Reportando problemas en el avión.</li>
                            <li><i class="bi bi-airplane-fill"></i> Disposición de asientos en cabina.</li>
                            <li><i class="bi bi-airplane-fill"></i> Verbo WILL/Future tense (+) (-) (?).</li>

                        </ul>

                    </div>
                </div>
                <div id="pre-operacional">
                    <h2 class="titulo-nivel">Pre - operacional</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-volume-up"></i> Pronunciación
                            </h5>
                            <p>
                                Pronunciación, estrés, ritmo y
                                entonación están influenciados por la
                                primera lengua o variación regional
                                y frecuentemente interfieren con facilidad
                                de comprensión.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-diagram-2"></i> Estructura
                            </h5>
                            <p>
                                Estructuras gramaticales básicas y
                                patrones de oración asociados
                                con situaciones previsibles son
                                no siempre bien controlado. Errores
                                frecuentemente interfieren con el significado.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-book"></i> Vocabulario
                            </h5>
                            <p>
                                Rango de vocabulario y precisión
                                son a menudo suficientes para
                                comunicarse en común,
                                temas concretos o relacionados con el trabajo
                                pero el rango es limitado y la palabra
                                elección a menudo inapropiado. Es
                                a menudo incapaz de parafrasear
                                con éxito cuando falta
                                vocabulario.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-dots"></i> Fluidez
                            </h5>
                            <p>
                                Produce tramos de lenguaje,
                                pero el fraseo y la pausa son
                                a menudo inapropiado. Vacilaciones o
                                lentitud en el procesamiento del lenguaje
                                puede impedir la eficacia
                                comunicación.
                                Los rellenos a veces distraen.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-list-check"></i> Comprensión
                            </h5>
                            <p>
                                La comprensión es a menudo precisa
                                común, concreto y de trabajo
                                temas relacionados cuando el acento o
                                variedad utilizada es suficientemente
                                inteligible para una organización internacional
                                comunidad de usuarios. Puede no
                                entender un lingüístico o
                                cambio de situación de los acontecimientos.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-left-quote"></i> Interacción
                            </h5>
                            <p>
                                Las respuestas son a veces
                                inmediata, adecuada y
                                informativo. Puede iniciar y
                                mantener los intercambios con
                                facilidad razonable en temas familiares
                                y en situaciones predecibles.
                                Generalmente inadecuado al tratar
                                con un giro inesperado de los acontecimientos.
                            </p>
                        </div>

                    </div>
                    <div class="row" id="contenidosPre-operacional">
                       
                        <div class="titulo-contenido">
                            <span class="numero">6</span>
                            <span class="nombre">runway incursion</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> El alfabeto aeronautico y su historia.</li>
                            <li><i class="bi bi-airplane-fill"></i> Diseño del aeropuerto.</li>
                            <li><i class="bi bi-airplane-fill"></i> Luces de entrada a la pista.</li>
                            <li><i class="bi bi-airplane-fill"></i> Marcas de pista.</li>
                            <li><i class="bi bi-airplane-fill"></i> Iluminacion del aeropuerto.</li>
                            <li><i class="bi bi-airplane-fill"></i> Tipos de aeronave.</li>
                            <li><i class="bi bi-airplane-fill"></i> Partes de la aeronave y sus funciones.</li>
                            <li><i class="bi bi-airplane-fill"></i> Luces exteriores.</li>
                            <li><i class="bi bi-airplane-fill"></i> Codigo SQWACK.</li>
                            <li><i class="bi bi-airplane-fill"></i> Drones Basicos.</li>
                            <li><i class="bi bi-airplane-fill"></i> Radar y Transponedores.</li>
                        </ul>
                       
                        <div class="titulo-contenido">
                            <span class="numero">7</span>
                            <span class="nombre">Comunicaciones aereas</span>
                        </div>
                        <ul>
                            <li> <i class="bi bi-airplane-fill"></i> Qué es la navegación aérea.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Cómo realizar un plan de vuelo.</li>                            
                            <li> <i class="bi bi-airplane-fill"></i> VFR y IFR.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Explicación y uso de abreviaciones y acrónimos aeronáuticos.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Explicación y uso de coordenadas aeronáuticas.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Vocabulario de comunicaciones de control de tráfico aéreo.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Vocabulario de ayudas de navegación.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Estructura del pasado simple: verbos regulares.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Pronunciacion de los verbos regulares en pasado simple</li>
                            <li> <i class="bi bi-airplane-fill"></i> Estructura del pasado simple: verbos irregulares.</li>
                            <li> <i class="bi bi-airplane-fill"></i> Discurso directo e indirecto (reportar).</li>
                        </ul>
                    </div>
                </div>
                <div id="operacional">
                    <h2 class="titulo-nivel">Operacional</h2>
                    <div class="row">

                        <div class="col-md-6"><h5>
                            <i class="bi bi-volume-up"></i> Pronunciación
                        </h5>
                        <p>
                            Pronunciación, estrés, ritmo y
                            entonación están influenciados por la
                            primera lengua o variación regional
                            pero solo a veces interfieren con
                            facilidad de comprensión.
                        </p></div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-diagram-2"></i> Estructura
                            </h5>
                            <p>
                                Estructuras gramaticales básicas y
                                patrones de oración se utilizan
                                creativamente y suelen estar bien
                                controlado. Pueden producirse errores,
                                particularmente en inusuales o
                                circunstancias inesperadas, pero
                                raramente interfiere con el significado.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-book"></i> Vocabulario
                            </h5>
                            <p>
                                Rango de vocabulario y precisión
                                suelen ser suficientes para
                                comunicarse eficazmente sobre
                                común, concreto y de trabajo
                                temas relacionados. Puede a menudo
                                paráfrasis con éxito cuando
                                falta de vocabulario en inusual o
                                circunstancias inesperadas.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-dots"></i> Fluidez
                            </h5>
                            <p>
                                Produce tramos de lenguaje
                                a un ritmo adecuado. Hay
                                puede ser pérdida ocasional de
                                fluidez en la transición desde
                                discurso ensayado o formulado a
                                interacción espontánea, pero esta
                                no impide eficaz
                                comunicación. Puede hacer
                                uso limitado de marcadores de discurso
                                o conectores.
                                Los rellenos no distraen.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-list-check"></i> Comprensión
                            </h5>
                            <p>
                                La comprensión es mayormente precisa
                                común, concreto y de trabajo
                                temas relacionados cuando el acento o
                                variedad utilizada es suficientemente
                                inteligible para una organización internacional
                                comunidad de usuarios. Cuando la
                                orador se enfrenta a un
                                lingüístico o situacional
                                complicación o un inesperado
                                de los acontecimientos, la comprensión puede
                                ser más lento o requerir aclaración
                                estrategias.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-left-quote"></i> Interacción
                            </h5>
                            <p>
                                Las respuestas suelen ser
                                inmediata, adecuada y
                                informativos. Iniciados y
                                mantiene los intercambios incluso cuando
                                tratar con un giro inesperado
                                eventos. Trata adecuadamente con
                                malentendidos aparentes por
                                comprobación, confirmación, o
                                clarificación.
                            </p>
                        </div>

                    </div>
                    <div class="row">
                        
                        <div class="titulo-contenido">
                            <span class="numero">8</span>
                            <span class="nombre">TECNOLOGÍA</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Fly by wire.</li>
                            <li><i class="bi bi-airplane-fill"></i> ¿Cómo identificar un AIRBUS y un BOEING?.</li>
                            <li><i class="bi bi-airplane-fill"></i> Abreviaturas de cabina.</li>
                            <li><i class="bi bi-airplane-fill"></i> Partes de la cabina.</li>
                            <li><i class="bi bi-airplane-fill"></i> Repaso de gramática tercera persona del singular presente simple.</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para expresar propósito.</li>
                            <li><i class="bi bi-airplane-fill"></i> Sistemas de control de vuelo.</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de seguridad.</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para dar instrucciones.</li>
                        </ul>
                        
                        <div class="titulo-contenido">
                            <span class="numero">9</span>
                            <span class="nombre">Animales peligrosos para la aviacion</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de medidas de seguridad</li>
                            <li><i class="bi bi-airplane-fill"></i> Proteger los aviones del impacto de aves</li>
                            <li><i class="bi bi-airplane-fill"></i> Peligro alrededor de los aeropuertos</li>
                            <li><i class="bi bi-airplane-fill"></i> ¿What is a bird strike?</li>
                            <li><i class="bi bi-airplane-fill"></i> Cómo leer una lista de verificación</li>
                            <li><i class="bi bi-airplane-fill"></i> Usos del verbo necesitar</li>
                            <li><i class="bi bi-airplane-fill"></i> NEED + INFINITIVE</li>
                            <li><i class="bi bi-airplane-fill"></i> NEED + GERUND</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones y verbos para expresar preferencias</li>
                            <li><i class="bi bi-airplane-fill"></i> Describiendo objetos o cosas desconocidas</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para decir intenciones</li>
                        </ul>

                        <div class="titulo-contenido">
                            <span class="numero">10</span>
                            <span class="nombre">Aviones y maniobras</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> ¿Qué es un avión planeador?</li>
                            <li><i class="bi bi-airplane-fill"></i> Partes de un planeador</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para explicar cómo funciona
                                un objeto determinado</li>
                            <li><i class="bi bi-airplane-fill"></i> Aviones y maniobras</li>
                            <li><i class="bi bi-airplane-fill"></i> Unidades de medida</li>
                            <li><i class="bi bi-airplane-fill"></i> Comparando y contrastando tipos de
                                aviones</li>
                            <li><i class="bi bi-airplane-fill"></i> Adjetivos comparativos y superlativos
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para expresar dificultad y ayuda
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de fallo por perdida
                                hidráulica</li>
                        </ul>
                  
                        <div class="titulo-contenido">
                            <span class="numero">11</span>
                            <span class="nombre">EMERGENCIAS MEDICAS EN EL AVION</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones y vocabulario de primeros auxilios y emergencias médicas</li>
                            <li><i class="bi bi-airplane-fill"></i> Como expresar causa y efecto
                            </li>
                       
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para salvar la vida en un vuelo</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de estrés</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para hacer sugerencias y dar
                                consejos</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para dar y pedir actualizaciones
                            </li>

                        </ul>
                     
                        <div class="titulo-contenido">
                            <span class="numero">12</span>
                            <span class="nombre">QUE SON LAS MERCANCIAS PELIGROSAS</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Frases relacionadas con fuego en el avión
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones de obligación, prohibición y
                                permiso </li>
                            <li><i class="bi bi-airplane-fill"></i> Rutinas y trabajo de un bombero paracaidista</li>
                            <li><i class="bi bi-airplane-fill"></i> Fuego en la cabina del avión</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de objetos para evitar fuego en
                                cabina del avión</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones relacionadas con vocabulario de
                                fuego</li>

                        </ul>
                      
                        <div class="titulo-contenido">
                            <span class="numero">13</span>
                            <span class="nombre">CLIMA Y AVIACION</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Microrráfagas</li>
                            <li><i class="bi bi-airplane-fill"></i> Tipos de clima y su efecto en la aviación
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de clima</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario meteorológico</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para pronosticar el tiempo</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para informar</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para enunciar advertencias</li>
                            <li><i class="bi bi-airplane-fill"></i> Interrupciones en aeropuerto</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario y expresiones de interrupciones
                                en aeropuerto</li>

                        </ul>
                    </div>
                </div>
                <div id="avanzado">
                    <h2 class="titulo-nivel">Avanzado</h2>
                    <div class="row">
                        <div class="col-md-6"><h5>
                            <i class="bi bi-volume-up"></i> Pronunciación
                        </h5>
                        <p>
                            Pronunciación, estrés, ritmo y entonación, aunque influenciado por
                            la primera lengua o regional
                            variación, rara vez interfiere con facilidad
                            de comprensión.
                        </p></div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-diagram-2"></i> Estructura
                            </h5>
                            <p>
                                Estructuras gramaticales básicas
                                y los patrones de oración son
                                bien controlado.
                                Estructuras complejas son
                                intentado pero con errores
                                que a veces interfieren con
                                significado.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-book"></i> Vocabulary
                            </h5>
                            <p>
                                Rango de vocabulario y precisión
                                son suficientes para comunicar
                                eficaz en común, concreto,
                                y temas relacionados con el trabajo.
                                Paráfrasis consistentemente y
                                con éxito.
                                Vocabulario es a veces
                                idiomático.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-dots"></i> Fluidez
                            </h5>
                            <p>
                                Capaz de hablar extensamente con
                                relativa facilidad en temas conocidos,
                                pero no puede variar el flujo del habla como un
                                estilístico. Puede hacer uso de
                                marcadores de discurso apropiados o
                                conectores.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-list-check"></i> Comprensión
                            </h5>
                            <p>
                                La comprensión es precisa sobre lo común, lo concreto y el trabajo.
                                temas relacionados y en su mayoría precisos
                                cuando el hablante se enfrenta
                                con un carácter lingüístico o situacional
                                complicación o un imprevisto
                                giro de acontecimientos. Es capaz de
                                comprender una variedad de discursos
                                variedades (dialecto y/o acento) o
                                registros.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                <i class="bi bi-chat-left-quote"></i> Interacción
                            </h5>
                            <p>
                                Las respuestas son inmediatas,
                                apropiado e informativo.
                                Gestiona al hablante/oyente
                                relación de manera efectiva.
                            </p>
                        </div>
                        
                        
                        
                        
                       
                    </div>
                    <div class="row">                      
                        <div class="titulo-contenido">
                            <span class="numero">14</span>
                            <span class="nombre">ATERRIZAJES</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Tipos de pista</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de tren de aterrizaje y
                                frenado
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario para describir imágenes</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario y expresiones para narrar
                                eventos en pasado</li>
                            <li><i class="bi bi-airplane-fill"></i> Reportes en discurso indirecto </li>
                            <li><i class="bi bi-airplane-fill"></i> Técnicas para reportar en discurso
                                indirecto
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Preposiciones para describir movimientos
                                y
                                lugares</li>
                            <li><i class="bi bi-airplane-fill"></i> Verbos de movimiento</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para resolver malos
                                entendidos
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de trenes de rodaje</li>
                        </ul>

                        <div class="titulo-contenido">
                            <span class="numero">15</span>
                            <span class="nombre">AVIACION Y CALENTAMIENTO GLOBAL</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de calentamiento global</li>
                            <li><i class="bi bi-airplane-fill"></i> Prefijos y verbos en contexto</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para hablar de soluciones y
                                problemas</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de combustible para el avión
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Glaseado de combustible</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para expresar expectativas
                            </li>
                        </ul>

                        <div class="titulo-contenido">
                            <span class="numero">16</span>
                            <span class="nombre">PRESIÓN Y DESCOMPRESIÓN</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario y expresiones de presión
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario y expresiones de
                                descompresión
                                de aeronave</li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario y expresiones de explosión
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Palabras para expresar tiempo y duración
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Verbos de acción en contexto</li>
                            <li><i class="bi bi-airplane-fill"></i> Tipos de daño en la aeronave</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones de aterrizaje de emergencia
                            </li>
                        </ul>

                        <div class="titulo-contenido">
                            <span class="numero">17</span>
                            <span class="nombre">SEGURIDAD EN LA AVIACIÓN</span>
                        </div>
                        <ul>
                            <li><i class="bi bi-airplane-fill"></i> Ventiscas, borrascas, viento y nieve
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Vocabulario de palabras de conflictos y
                                restricciones </li>
                            <li><i class="bi bi-airplane-fill"></i> Acciones en vos pasiva</li>
                            <li><i class="bi bi-airplane-fill"></i> Tipos de pasajero en el avión</li>
                            <li><i class="bi bi-airplane-fill"></i> Comportamientos sospechosos en el avión
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones para expresar probabilidad y
                                posibilidad</li>
                            <li><i class="bi bi-airplane-fill"></i> Expresiones de interferencias ilegales
                            </li>
                            <li><i class="bi bi-airplane-fill"></i> Practicas de discurso directo y discurso
                                indirecto </li>
                            <li><i class="bi bi-airplane-fill"></i> Reportes en discurso indirecto</li>
                        </ul>
                    </div>
                </div>
                <div id="experto">
                    <h2 class="titulo-nivel">Experto</h2>
                    <p>El nivel "experto", como lo llamó la OACI, a menudo se denomina nivel de "hablante nativo", pero esto no es 100% cierto. Es posible (para hablantes no nativos) alcanzar el nivel 6. Para lograrlo, no solo requiere una amplia gama de vocabulario, sino también muchos otros aspectos que te otorgarán un nivel 6, como tus habilidades gramaticales, pronunciación, etc.
                        <br>
                        Tener mucha experiencia en vuelos no es garantía de ser un experto en el "idioma inglés", pero sin duda te ayuda. Tus habilidades en el idioma inglés durante la prueba se evaluarán en las siguientes áreas:</p>
                <div class="row">
                    <div class="col-md-6">
                        <h5>
                            <i class="bi bi-volume-up"></i> Pronunciación
                        </h5>
                        <p>Casi impecable. Apenas hay diferencia entre tu pronunciación
                            y la forma en que hablan los hablantes nativos.</p>    
                    </div>
                    <div class="col-md-6"> 
                        <h5>
                            <i class="bi bi-diagram-2"></i> Estructura
                        </h5>
                        <p>Gramática básica: perfecto | Gramática compleja: (casi)
                            perfecta. No sólo la gramática básica, sino también los
                            patrones de oraciones más complejos son casi perfectos
                            cuando los usas, ¡y los usas con frecuencia!</p>
                    </div>
                    <div class="col-md-6">
                        <h5>
                            <i class="bi bi-book"></i> Vocabulary
                        </h5>
                        <p>Sigue, sigue y sigue... suena natural. Puedes hablar,
                            hablar, hablar, hablar... suena natural y sin esfuerzo.
                            Utiliza marcadores de discurso no sólo porque sabe que debe
                            hacerlo en esta prueba, sino porque ésta es su forma
                            habitual de hablar. Para enfatizar algo, incluso puedes
                            variar el flujo del habla de forma natural.</p>
                    </div>
                    <div class="col-md-6"> 
                        <h5>
                            <i class="bi bi-chat-dots"></i> Fluidez
                        </h5>
                        <p>¿Puedes hablar de algo*? Puedes hablar de casi
                            cualquier tema; ya sea familiar o desconocido. Por supuesto,
                            utilizas modismos en tu discurso cotidiano y no sólo conoces
                            una palabra para nombrar algo, sino términos diferentes y
                            matizados para una misma cosa.</p>
                    </div>
                    <div class="col-md-6">
                        <h5>
                            <i class="bi bi-list-check"></i> Comprensión
                        </h5>
                        <p>No hay nada que no entiendas. ¿Podrías escuchar un noticiero
                            en inglés, una canción de rap en inglés… o algo así? Por
                            supuesto, esto no es necesario durante la prueba, pero si
                            eres capaz de escuchar este tipo de cosas sin esfuerzo,
                            podrías tener un nivel 6. También reconoces y conoces
                            sutilezas culturales y lingüísticas.</p>
                    </div>
                    <div class="col-md-6">
                        <h5>
                            <i class="bi bi-chat-left-quote"></i> Interacción
                        </h5>
                        <p>El inglés es fácil (para ti). Interactúas con facilidad en
                            casi todas las situaciones, eres sensible a las señales
                            verbales y no verbales. Pregúntate: ¿podrías contarnos un
                            chiste de la aviación en ingles?</p>
                    </div>
                </div>
                <div class="row">                      
                    

                    <div class="titulo-contenido">
                        <span class="numero">18</span>
                        <span class="nombre">CONVERSACION</span>
                    </div>
                    <ul>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 1
                        </li>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 2</li>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 3</li>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 4</li>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 5
                        </li>
                        <li><i class="bi bi-airplane-fill"></i> Conversacion 6
                        </li>
                       
                    </ul>
                </div>
                </div>


            </div>
        </div>
    </div>
@endsection
