@extends('user.navbar')

<head>
    <link rel="stylesheet" href="{{ asset('css/user/cuestionario.css') }}">
</head>
@section('contenido')

    @if (!$respuestasAnteriores->isEmpty())
        <style>
            .form-check-input:disabled {
                opacity: 1;
            }

            .form-check-input:disabled+.form-check-label {
                color: #000;
                opacity: 1;
            }
        </style>
        <div class="container-fluid formulario" id="respuestas-anteriores">
            <h1>{{ $formulario->titulo }}</h1>

            {{-- <h5><strong>Puntaje final:</strong> {{ $puntajeObtenidoFormulario }} / {{ $formulario->puntaje_total }}</h5>           --}}
            @foreach ($respuestasAnteriores as $respuestaAnterior)
                <div class="cont-pregunta">                 
                    {!! $respuestaAnterior->pregunta->texto_pregunta !!}
                    @if ($respuestaAnterior->pregunta->tipo_pregunta == 'Corta')
                        <input type="text" class="form-control" value="{{ $respuestaAnterior->respuesta_usuario }}"
                            readonly>
                    @elseif ($respuestaAnterior->pregunta->tipo_pregunta == 'Larga')
                        <input type="text" class="form-control" value="{{ $respuestaAnterior->respuesta_usuario }}"
                            readonly>
                    @elseif ($respuestaAnterior->pregunta->tipo_pregunta == 'Verdadero/Falso')
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                                name="respuesta{{ $respuestaAnterior->pregunta->id }}"
                                id="verdadero{{ $respuestaAnterior->pregunta->id }}" value="Verdadero"
                                {{ $respuestaAnterior->respuesta_usuario == 'Verdadero' ? 'checked' : '' }} disabled>
                            <label class="form-check-label"
                                for="verdadero{{ $respuestaAnterior->pregunta->id }}">Verdadero</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                                name="respuesta{{ $respuestaAnterior->pregunta->id }}"
                                id="falso{{ $respuestaAnterior->pregunta->id }}" value="Falso"
                                {{ $respuestaAnterior->respuesta_usuario == 'Falso' ? 'checked' : '' }} disabled>
                            <label class="form-check-label" for="falso{{ $respuestaAnterior->pregunta->id }}">Falso</label>
                        </div>
                    @elseif ($respuestaAnterior->pregunta->tipo_pregunta == 'Opcion multiple')
                        <div class="form-check">
                            @foreach ($respuestaAnterior->pregunta->opciones as $opcion)
                                <input class="form-check-input" type="radio"
                                    name="respuesta{{ $respuestaAnterior->pregunta->id }}" id="opcion{{ $opcion->id }}"
                                    value="{{ $opcion->textoOpcion }}"
                                    {{ $respuestaAnterior->respuesta_usuario == $opcion->textoOpcion ? 'checked' : '' }}
                                    disabled>
                                <label class="form-check-label"
                                    for="opcion{{ $opcion->id }}">{{ $opcion->textoOpcion }}</label><br>
                            @endforeach
                            <input class="form-check-input" type="radio"
                                name="respuesta{{ $respuestaAnterior->pregunta->id }}"
                                id="respuestaCorrecta{{ $respuestaAnterior->pregunta->id }}"
                                value="{{ $respuestaAnterior->pregunta->respuestaCorrecta }}"
                                {{ $respuestaAnterior->respuesta_usuario == $respuestaAnterior->pregunta->respuestaCorrecta ? 'checked' : '' }}
                                disabled>
                            <label class="form-check-label" for="respuestaCorrecta{{ $respuestaAnterior->pregunta->id }}">
                                {{ $respuestaAnterior->pregunta->respuestaCorrecta }}
                            </label><br>
                        </div>
                    @elseif ($respuestaAnterior->pregunta->tipo_pregunta == 'Seleccion multiple')
                        <div class="form-check">
                            @foreach ($respuestaAnterior->pregunta->opciones as $opcion)
                                <input class="form-check-input" type="checkbox"
                                    name="respuesta{{ $respuestaAnterior->pregunta->id }}[]"
                                    id="opcion{{ $opcion->id }}" value="{{ $opcion->textoOpcion }}"
                                    {{ in_array($opcion->textoOpcion, explode(', ', $respuestaAnterior->respuesta_usuario)) ? 'checked' : '' }}
                                    disabled>
                                <label class="form-check-label"
                                    for="opcion{{ $opcion->id }}">{{ $opcion->textoOpcion }}</label><br>
                            @endforeach
                        </div>
                    @endif
                </div>

                <div class="container-fluid cont-respuesta @if ($respuestaAnterior->puntaje > 0)correcto @else incorrecto @endif">
                    <div class="row">
                        <div class="col d-flex flex-column align-items-left">
                            @if ($respuestaAnterior->pregunta->tipo_pregunta == 'Larga')
                                <strong>Correccion:</strong>
                                <br>
                                <p>{{ $respuestaAnterior->correccion_usuario }}</p>
                            @else
                                @if ($respuestaAnterior->puntaje > 0)
                                    <strong class="text-success respuesta">Respuesta Correcta <i
                                            class="bi bi-check-circle-fill"></i></strong>
                                @else
                                    <strong class="text-danger respuesta">Respuesta Incorrecta <i
                                            class="bi bi-x-circle-fill"></i></strong>
                                @endif

                                <p class="mb-0">
                                    <strong>La respuesta correcta es:</strong>
                                    {{ $respuestaAnterior->pregunta->respuestaCorrecta }}
                                    @foreach ($respuestaAnterior->pregunta->opciones as $opcion)
                                        @if ($opcion->esCorrecta)
                                            <span>{{ $opcion->textoOpcion }}</span>,
                                        @endif
                                    @endforeach
                                </p>
                            @endif
                        </div>

                        <div class="col d-flex justify-content-center align-items-center">Puntaje: 
                            {{ $respuestaAnterior->puntaje }}/{{ $respuestaAnterior->pregunta->puntaje }} puntos
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <form method="post" action="{{ route('guardar.respuestas', ['formulario' => $formulario->id]) }}">
            <div class="container-fluid formulario">
                <h1>{{ $formulario->titulo }}</h1>
          
                <p class="text-center"> {!!$formulario->descripcion!!}</p>
                <input type="text" id="formulario_id" value="{{ $formulario->id }}" hidden>
                @csrf

                @forelse ($formulario->preguntas as $pregunta)
                    <div class="cont-pregunta">
                        <div class="contenido d-flex justify-content-center mb-3">
                            @if ($pregunta->contenido)
                                @if ($pregunta->contenido->tipo_contenido === 'audio')
                                    <audio src="{{ $pregunta->contenido->url_archivo }}" controls controlsList="nodownload"
                                        style="width: 90%"></audio>
                                @elseif ($pregunta->contenido->tipo_contenido === 'imagen')
                                    <img src="{{ $pregunta->contenido->url_archivo }}"
                                        alt="{{ $pregunta->contenido->titulo }}" style="width:80%">
                                @elseif ($pregunta->contenido->tipo_contenido === 'video')
                                    <video width="100%" src="{{ $pregunta->contenido->url_archivo }}" controls
                                        controlsList="nodownload"></video>
                                @elseif ($pregunta->contenido->tipo_contenido === 'pdf')
                                    {{-- <a href="{{ $pregunta->contenido->url_archivo }}" class="btn btn-primary"
                                        target="_blank"><i class="bi bi-eye"></i> {{ $pregunta->contenido->titulo }}</a> --}}
                                    <iframe src="{{ $pregunta->contenido->url_archivo }}" width="800" height="600"></iframe>
                                @else
                                @endif
                            @endif
                        </div>
                        {!! $pregunta->texto_pregunta !!}
                        <p class="d-flex justify-content-between">
                          
                            <span>{{ $pregunta->puntaje }} puntos</span>
                        </p>
                  

                        @if ($pregunta->tipo_pregunta == 'Corta')
                            <input type="text" class="form-control" name="respuestaCorta{{ $pregunta->id }}">
                        @elseif ($pregunta->tipo_pregunta == 'Larga')
                            <textarea class="form-control" name="respuestaLarga{{ $pregunta->id }}" cols="100%" rows="4"></textarea>
                        @elseif ($pregunta->tipo_pregunta == 'Verdadero/Falso')
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="respuestaCorrecta{{ $pregunta->id }}"
                                    id="verdadero{{ $pregunta->id }}" value="Verdadero">
                                <label class="form-check-label" for="verdadero{{ $pregunta->id }}">Verdadero</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="respuestaCorrecta{{ $pregunta->id }}" id="falso{{ $pregunta->id }}"
                                    value="Falso">
                                <label class="form-check-label" for="falso{{ $pregunta->id }}">Falso</label>
                            </div>
                        @elseif ($pregunta->tipo_pregunta == 'Opcion multiple')
                            @php
                                $opciones = $pregunta->opciones->pluck('textoOpcion')->toArray();
                                $respuestaCorrecta = $pregunta->respuestaCorrecta;
                                $opciones[] = $respuestaCorrecta; // Agrega la respuesta correcta al array de opciones

                                // Mezcla todas las opciones solo si la respuesta correcta no está seleccionada
                                if (!in_array($respuestaCorrecta, (array) $request->input('opcion' . $pregunta->id, []))) {
                                    shuffle($opciones);
                                }
                            @endphp

                            <div class="form-check">
                                @foreach ($opciones as $opcion)
                                    <input class="form-check-input" type="radio" name="opcion{{ $pregunta->id }}"
                                        id="opcion{{ $loop->index }}" value="{{ $opcion }}">
                                    <label class="form-check-label"
                                        for="opcion{{ $loop->index }}">{{ $opcion }}</label><br>
                                @endforeach
                            </div>
                        @elseif ($pregunta->tipo_pregunta == 'Seleccion multiple')
                            <div class="form-check">
                                @foreach ($pregunta->opciones->pluck('textoOpcion')->shuffle() as $opcion)
                                    <input class="form-check-input" type="checkbox" name="opcion{{ $pregunta->id }}[]"
                                        id="opcion{{ $loop->index }}" value="{{ $opcion }}">
                                    <label class="form-check-label"
                                        for="opcion{{ $loop->index }}">{{ $opcion }}</label><br>
                                @endforeach
                            </div>
                        @endif


                    </div>
                @empty
                    <p>No hay preguntas disponibles en este formulario.</p>
                @endforelse

                <button type="submit" class="btn-enviar"><i class="bi bi-send"></i> Enviar</button>
            </div>
        </form>
    @endif


@endsection
