<head>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;700&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <link rel="stylesheet" href="{{ asset('css/user/navbar.css') }}">

</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" style="color: white">
                <img src="{{ asset('images/logo.png') }}" alt="logo" width="50px">
                Academy
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-barra" aria-current="page" href="/bienvenido">Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-barra dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Mi aprendizaje
                        </a>

                        <ul class="dropdown-menu">
                            @php
                                $modulos = $modulosAsignados();
                            @endphp
                            @forelse ($modulos as $modulo)
                                <li>
                                    <a class="dropdown-item" href="{{ route('ver.modulo', ['id' => $modulo->id]) }}">
                                        Modulo {{ $modulo->num_modulo}}
                                    </a>
                                </li>
                            @empty
                                <li><a class="dropdown-item" href="#">No tienes módulos asignados</a></li>
                            @endforelse
                        </ul>

                    </li>

                </ul>
                <div class="d-flex">
                    <div class="btn-group">
                        <button type="button" class="btn menu-usuario" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-person-circle" style="font-size: 24px"></i>
                        </button>
                        
                        <ul class="dropdown-menu dropdown-menu-lg-end">
                            <li><a class="dropdown-item" href="/perfil"><i class="bi bi-person-fill"></i> Mi perfil</a>
                            </li>
                            <li><a class="dropdown-item" href="/logout"><i class="bi bi-box-arrow-left"></i> Cerrar
                                    Sesion</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </nav>
    <main>
        @yield('contenido')
    </main>
<footer>
    <div class="container-fluid" id="piePagina">
        <div class="row">
            <div class="col-md-12 col-lg-6 text-center">
                <img src="{{ asset('images/logo.png') }}" alt="" class="mx-auto img-fluid">
                <p class="mb-0">¡Aprende inglés aeronáutico con nosotros y aprueba tu competencia lingüística!
                    <br>©2023 - EFA Academy
                </p>

            </div>
            <div class="col-md-12 col-lg-6 text-center datos">
                <p><b>Correo electrónico:</b> efaacademy2022@gmail.com</p>
                <p><b>Teléfono:</b> +591 67555735</p>
                <div class="iconos">
                    <a class="btn" href="https://www.facebook.com/EFAACADEMY1" role="button" target="_blank">
                        <i class="bi bi-facebook"></i>
                    </a>
                    <a class="btn"
                        href="https://wa.me/59167555735?text=Quisiera%20saber%20m%C3%A1s%20sobre%20EFA%20Academy"
                        role="button" target="_blank">
                        <i class="bi bi-whatsapp"></i>
                    </a>
                    <a class="btn" href="https://instagram.com/englishforaviationacademy?igshid=MzRlODBiNWFlZA=="
                        role="button" target="_blank">
                        <i class="bi bi-instagram"></i>
                    </a>
                </div>
            </div>

        </div>
    </div>
</footer>

    
    
    
</body>
