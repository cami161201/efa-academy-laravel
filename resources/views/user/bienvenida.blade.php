@extends('user.navbar')

@section('contenido')
    @auth

        <head>
            <link rel="stylesheet" href="css/user/bienvenida.css">
            <link
                href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;700&family=Roboto:wght@300;400;500&display=swap"
                rel="stylesheet">
        </head>


        <div id="carrusel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="0" class="active" aria-current="true"
                    aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="3" aria-label="Slide 4"></button>
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="4" aria-label="Slide 5"></button>
                <button type="button" data-bs-target="#carrusel" data-bs-slide-to="5" aria-label="Slide 6"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="10000"
                    style="background-image: url('images/carrusel-1.jpg')">

                    <div class="carousel-caption d-none d-md-block">
                        <h2>Bienvenido a EFA Academy</h2>
                        <p>
                            Aprende inglés aeronáutico y despega hacia tu futuro en la aviación.
                        </p>
                    </div>
                </div>
                <div class="carousel-item" data-bs-interval="2000" style="background-image: url('images/carrusel-2.jpg')">
                    {{-- <img src="images/fondo.jpg" class="d-block w-100" alt="..."> --}}
                    <div class="carousel-caption d-none d-md-block">
                        <h2>¡Vuela alto con nuestro curso de inglés aeronáutico!</h2>
                        <p> Aterrizaje seguro en el idioma de la aviación.</p>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('/images/fondo-car20.jpg')">
                    {{-- <img src="images/login.jpg" class="d-block w-100" alt="..."> --}}
                    <div class="carousel-caption d-none d-md-block">
                        <h2>¡Despega hacia el éxito!</h2>
                        <p>
                            con nuestro programa de inglés aeronáutico de primer nivel
                        </p>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('/images/carrusel-3.jpg')">
                    {{-- <img src="images/login.jpg" class="d-block w-100" alt="..."> --}}
                    <div class="carousel-caption d-none d-md-block">
                        <h2>¿Quieres volar alto?</h2>
                        <p>Comienza con el inglés aeronáutico. Únete a nuestra academia.</p>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('/images/fondo-car-16.jpg')">
                    {{-- <img src="images/login.jpg" class="d-block w-100" alt="..."> --}}
                    <div class="carousel-caption d-none d-md-block">
                        <h2>Aprueba la competencia linguistica OACI</h2>
                        <p>con excelencia. Contáctanos. </p>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('/images/torre.jpg')">
                    {{-- <img src="images/login.jpg" class="d-block w-100" alt="..."> --}}
                    <div class="carousel-caption d-none d-md-block">
                        <h2>¿Sueñas con ser piloto, controlador aéreo, o tripulante de cabina?</h2>
                        <p>Habla inglés aeronáutico como un profesional</p>
                    </div>
                </div>

            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carrusel" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carrusel" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>


        <div class="container-fluid" id="elemento">
            <h1>Niveles de Competencia de la OACI</h1>
            <p>Estos niveles son esenciales para garantizar que nuestros estudiantes y profesionales de la aviación alcancen la
                competencia necesaria en inglés aeronáutico, un estándar esencial para la comunicación segura en la industria de
                la aviación a nivel mundial.</p>

            <div class="row" id="nivel">
                <div class="col-md-4 info-nivel">

                    <a href="/niveles#pre-elemental" class="btn btn-muestra">
                        Nivel: Pre - elemental
                    </a>

                </div>
                <div class="col-md-4 info-nivel">
                    <a href="/niveles#elemental" class="btn btn-muestra">
                        Nivel: Elemental
                    </a>
                </div>
                <div class="col-md-4 info-nivel">
                    <a href="/niveles#pre-operacional" class="btn btn-muestra">
                        Nivel: Pre - operacional
                    </a>
                </div>
                <div class="col-md-4 info-nivel">                   
                    <a href="/niveles#operacional" class="btn btn-muestra">
                        Nivel: Operacional
                    </a>                    
                </div>        
                <div class="col-md-4 info-nivel">
                
                    <a href="/niveles#avanzado" class="btn btn-muestra">
                        Nivel: Avanzado
                    </a> 
                 
                </div>
                <div class="col-md-4 info-nivel">
                    <button class="btn btn-muestra" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">
                        Nivel: Experto
                    </button>

                    <div class="modal fade" id="exampleModalToggle" aria-labelledby="exampleModalToggleLabel" tabindex="-1"
                        aria-hidden="true" style="display: none">
                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalToggleLabel">
                                        Experto
                                    </h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <ul>
                                        <li>
                                            <i class="bi bi-airplane-fill"></i> Topic 1
                                        </li>
                                        <li>
                                            <i class="bi bi-airplane-fill"></i> Topic 2
                                        </li>
                                        <li>
                                            <i class="bi bi-airplane-fill"></i> Topic 3
                                        </li>
                                        <li>
                                            <i class="bi bi-airplane-fill"></i> Topic 4
                                        </li>
                                        <li>
                                            <i class="bi bi-airplane-fill"></i> Topic 5
                                        </li>
                                    </ul>
                                </div>
                                <button class="btn btn-primary n6" data-bs-target="#experto2" data-bs-toggle="modal">
                                    ¿Quieres ser experto nivel 6?
                                    <style>
                                        .n6 {
                                            width: 80%;
                                            margin: 2% 10%
                                        }
                                    </style>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="experto2" aria-labelledby="expertoLabel2" tabindex="-1"
                        aria-hidden="true" style="display: none">
                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="expertoLabel2">
                                        ¿QUIERES SER EXPERTO NIVEL 6?
                                    </h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">

                                    <p class="text-start">
                                        El nivel "experto", como lo llamó la OACI, a menudo se denomina
                                        nivel de "hablante nativo", pero esto no es 100% cierto. Es
                                        posible (para hablantes no nativos) alcanzar el nivel 6. Para
                                        lograrlo, no solo requiere una amplia gama de vocabulario, sino
                                        también muchos otros aspectos que te otorgarán un nivel 6, como
                                        tus habilidades gramaticales, pronunciación, etc.
                                    </p>

                                    <p class="text-start">
                                        Tener mucha experiencia en vuelos no es garantía de ser un experto
                                        en el "idioma inglés", pero sin duda te ayuda. Tus habilidades en
                                        el idioma inglés durante la prueba se evaluarán en las siguientes
                                        áreas:
                                    </p>
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Pronunciación
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Casi impecable. Apenas hay diferencia entre tu pronunciación
                                                    y la forma en que hablan los hablantes nativos.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                    aria-expanded="false" aria-controls="collapseTwo">
                                                    Estructura
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                    aria-expanded="false" aria-controls="collapseThree">
                                                    Fluidez
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Sigue, sigue y sigue... suena natural. Puedes hablar,
                                                    hablar, hablar, hablar... suena natural y sin esfuerzo.
                                                    Utiliza marcadores de discurso no sólo porque sabe que debe
                                                    hacerlo en esta prueba, sino porque ésta es su forma
                                                    habitual de hablar. Para enfatizar algo, incluso puedes
                                                    variar el flujo del habla de forma natural.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                                    aria-expanded="false" aria-controls="collapseFour">
                                                    Fluidez
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    ¿Puedes hablar de algo*? Puedes hablar de casi
                                                    cualquier tema; ya sea familiar o desconocido. Por supuesto,
                                                    utilizas modismos en tu discurso cotidiano y no sólo conoces
                                                    una palabra para nombrar algo, sino términos diferentes y
                                                    matizados para una misma cosa.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                                    aria-expanded="false" aria-controls="collapseFive">
                                                    Comprensión
                                                </button>
                                            </h2>
                                            <div id="collapseFive" class="accordion-collapse collapse"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    No hay nada que no entiendas. ¿Podrías escuchar un noticiero
                                                    en inglés, una canción de rap en inglés… o algo así? Por
                                                    supuesto, esto no es necesario durante la prueba, pero si
                                                    eres capaz de escuchar este tipo de cosas sin esfuerzo,
                                                    podrías tener un nivel 6. También reconoces y conoces
                                                    sutilezas culturales y lingüísticas.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                                    aria-expanded="false" aria-controls="collapseSix">
                                                    Interacción.
                                                </button>
                                            </h2>
                                            <div id="collapseSix" class="accordion-collapse collapse"
                                                data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    El inglés es fácil (para ti). Interactúas con facilidad en
                                                    casi todas las situaciones, eres sensible a las señales
                                                    verbales y no verbales. Pregúntate: ¿podrías contarnos un
                                                    chiste de la aviación en ingles?
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <p style="text-align: center">
            <strong>"Que tus sueños vuelen mas alto que tus miedos"</strong><br> Aprende ingles aeronautico con nosotros
        </p>

    @endauth
@endsection
