@extends('user.navbar')

@section('contenido')

    <head>
        <link rel="stylesheet" href="css/user/perfil.css">
    </head>


    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">

                <div class="row cont-datos" id="datos">
                    <h3>Datos</h3>
                    <hr>
                    <div class="col-6">
                        <strong><i class="bi bi-person-circle"></i>Nombre</strong><br>
                        <p>{{ auth()->user()->nombre }}</p>
                        <strong><i class="bi bi-person-vcard"></i>Primer apellido</strong><br>
                        <p>{{ auth()->user()->primer_apellido }}</p>
                        <strong><i class="bi bi-person-vcard"></i>Segundo apellido</strong><br>
                        <p>{{ auth()->user()->segundo_apellido }}</p>
                        <strong><i class="bi bi-briefcase"></i>Ocupacion</strong><br>
                        <p>{{ auth()->user()->ocupacion }}</p>
                    </div>
                    <div class="col-6">
                        <strong><i class="bi bi-mortarboard"></i>Academia</strong><br>
                        <p>{{ auth()->user()->academia }}</p>
                        <strong><i class="bi bi-calendar-check"></i>Fecha de nacimiento</strong><br>
                        <p>
                            {{ \Carbon\Carbon::parse(auth()->user()->fecha_nacimiento)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                        </p>
                        <strong><i class="bi bi-telephone"></i>Celular</strong><br>
                        <p>{{ auth()->user()->celular }}</p>
                        <strong><i class="bi bi-envelope-at"></i>Correo electronico</strong><br>
                        <p>{{ auth()->user()->correo }}</p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="cont-datos">
                    <h3>Cursos</h3>
                    <hr>
                    <strong><i class="bi bi-calendar-check"></i> Fecha de inicio de acceso</strong><br>
                    <p>
                        {{ \Carbon\Carbon::parse(auth()->user()->start_access)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                    </p>

                    <strong><i class="bi bi-calendar-x"></i> Fecha de fin de acceso</strong><br>

                    <p>
                        {{ \Carbon\Carbon::parse(auth()->user()->end_access)->locale('es')->isoFormat('D [de] MMMM [del] YYYY') }}
                    </p>

                </div>
                <div class="cont-datos">
                    <h3>Módulos Asignados:</h3>
                    <hr>
                    @if (auth()->user()->modulos->count() > 0)
                        
                        <ul>
                            @foreach (auth()->user()->modulos as $modulo)
                                <li>{{ $modulo->titulo }}</li>
                            @endforeach
                        </ul>
                    @else
                        <p>No tienes módulos asignados actualmente.</p>
                    @endif

                </div>
            </div>
        </div>


    </div>
@endsection
