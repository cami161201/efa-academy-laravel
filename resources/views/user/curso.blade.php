@extends('user.navbar')

@section('contenido')

    <head>
        <link rel="stylesheet" href="{{ asset('css/user/curso.css') }}">
    </head>


    <div class="container-fluid">
        <h1 class="titulo-modulo">{{ $modulo->titulo }}</h1>
        <h5><b>Instrucciones <br></b></h5>
        <p class="descripcion">{{ $modulo->descripcion }}</p>

        <h3 class="titulo-unidad">Unidades:</h3>
        @foreach ($modulo->unidades as $unidad)
            {{-- @if ($unidad->estaHabilitadaParaUsuario(auth()->user())) --}}
                <ul>
                    <li>
                        <a class="unidades" data-bs-toggle="collapse" href="#unidad{{ $unidad->id }}" role="button"
                            aria-expanded="false" aria-controls="collapseExample">
                            <i class="bi bi-folder-fill"></i> {{ $unidad->nombreUnidad }}
                            @php
                                $puntajeUnidad = $unidad->puntajesUnidades->where('usuario_id', auth()->user()->id)->first();
                            @endphp

                            @if ($puntajeUnidad)
                                <span class="badge bg-success float-end">{{ $puntajeUnidad->puntaje_obtenido_unidad }} /
                                    {{ $unidad->puntaje_total_unidad }}</span>
                            @else
                                <span class="badge bg-secondary float-end">No evaluado</span>
                            @endif

                        </a>

                        <div class="collapse" id="unidad{{ $unidad->id }}">
                            <div class="contenido-unidad">
                                <p class="descripcion">
                                    {{ $unidad->descripcionUnidad }}
                                </p>
                                <ul>
                                    @foreach ($unidad->formularios as $formulario)
                                        <li>
                                            <a class="dropdown-item"
                                                href="{{ route('ver.formulario', ['id' => $formulario->id]) }}">
                                                <i class="bi bi-ui-checks"></i> {{ $formulario->titulo }}
                                                @php
                                                    $puntajeAcumulado = $formulario->puntajesAcumulados->where('usuario_id', auth()->user()->id)->first();
                                                @endphp
                                                @if ($puntajeAcumulado)
                                                    <span
                                                        class="badge bg-success float-end">{{ $puntajeAcumulado->puntaje_obtenido_formulario }}
                                                        / {{ $formulario->puntaje_total }}</span>
                                                @else
                                                    <span class="badge bg-secondary float-end">No evaluado</span>
                                                @endif
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            {{-- @endif --}}
        @endforeach
    </div>
@endsection
